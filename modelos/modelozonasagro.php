<?php


class ModeloZonasagro
{

    private $intId;
	private $txtDescripcion;

    
// ------------------------------------------------------------------------------------
	
	public function db_connect()
	{
		$config = Config::singleton();

		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
  
		if (!$this->Conexion_ID) 
		{
			die('Ha fallado la conexi�n: ' . mysql_error());
			return 0;
		}
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }
		return $this->Conexion_ID;
	}
	
// ------------------------------------------------------------------------------------

	public function __construct()
	{
		$this->db_connect();
	}

// ------------------------------------------------------------------------------------

	public function getId()
	{
		return $this->intId;
	}

	public function putId($parId)
	{
		$this->intId = $parId;
	}

// ------------------------------------------------------------------------------------

	public function getDescripcion()
	{
		return $this->txtDescripcion;
	}

	public function putDescripcion($parDescripcion)
	{
		$this->txtDescripcion = $parDescripcion;
	}

// ------------------------------------------------------------------------------------

	public function traerTodos() 
    //retorna la consulta de todas las zonas agroecologicas
	{
		$query = ('SELECT * FROM zonasagroecologicas ORDER BY descripcion');
		$result_all = mysql_query($query);
		while ($varza = mysql_fetch_object($result_all))
		{
			//llenar el array 
			$arrZonas[]=array($varza->id,
		   					$varza->descripcion);
		} 
		return $arrZonas;
	}	

//============================================================================

	public function traerZonaAgro()
	//carga las variables con los valores de una zona determinada por un ID, retorna true o false 
    {
		$id = $this->getId();
		$query = ("SELECT * FROM zonasagroecologicas WHERE zonasagroecologicas.id='$id'");
		$result_all = mysql_query($query);
		if($result_all)
		{
			$this->cargarresultados($result_all);
			return(true);
		} else {
			echo "No se encontro la zona";
			return(false);
		}
	}

//============================================================================

	public function listadoTotal()  
	//retorna un listado de todas las zonas agroecologicas
	{
		$query =("SELECT * FROM zonasagroecologicas ");
		$result_all = mysql_query($query);
		while ($varza = mysql_fetch_object($result_all))
		{
			$arrZonas[]=array("id"=>$varza->id,
   								"descripcion"=>$varza->descripcion);
		} 
		return  $arrZonas;
	}	

//============================================================================

	public function borrartipodoc()
	{	
		$query=("DELETE FROM zonasagroecologicas WHERE id = '$this->intId'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}

//============================================================================

	public function modificartipodoc()
	{
		$query = ("UPDATE zonasagroecologicas SET descripcion='$this->txtDescripcion' WHERE id = '$this->intId'");
		$result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && ($num_rows > 0));
	}


//============================================================================

	public function altatipodoc()
	{
		$query = ("INSERT INTO zonasagroecologicas (descripcion) VALUES ('$this->txtDescripcion')");
		$result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}
				
//============================================================================

	public function setvariables()
	//pone a cero y vacio todas las variables de la clase
	{
        $this->putId(0);
        $this->putDescripcion("");
	}

//============================================================================

	public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
		$this->setvariables();
		while ($cons = mysql_fetch_object($resultado))
		{
			$this->putId($cons->id);
			$this->putDescripcion($cons->descripcion);
    	}
	}

}
?>