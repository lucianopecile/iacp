<?php
require_once 'modelomovimientoexterno.php';
class Modeloarchivocobro
{

    private $intIdArchivoCobro;
    private $txtNombre;
    private $txtTipo;
    private $fecFechaImportacion;
    private $intSize;
    private $intIdUsr;
    private $txtEstado;


//==============================================================================================

	 public function db_connect()
{

        $config = Config::singleton();

    $this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
// $this->Conexion_ID=mysql_connect("localhost","root","");

		if (!$this->Conexion_ID)
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID))
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }


        return $this->Conexion_ID;

	}

//==============================================================================================

	public function __construct()
	{
	  $this->db_connect();
    }

// ------------------------------------------------------------------------------------
    public function getIdArchivoCobro()
	{
	    return $this->intIdArchivoCobro;
	}

    public function putIdArchivoCobro($parIdArchivoCobro)
	{
	    $this->intIdArchivoCobro =$parIdArchivoCobro;
	}

// ------------------------------------------------------------------------------------

    public function getNombre()
	{
	    return $this->txtNombre;
	}

    public function putNombre($parNombre)
	{

	    $this->txtNombre =$parNombre;
	}

// ------------------------------------------------------------------------------------

    public function getFechaImportacion()
	{
	    return $this->fecFechaImportacion;
	}

    public function putFechaImportacion($parFechaImportacion)
	{
	    $this->fecFechaImportacion =$parFechaImportacion;
	}

// ------------------------------------------------------------------------------------

    public function getSize()
	{
	    return $this->intSize;
	}

    public function putSize($parSize)
	{
	    $this->intSize =$parSize;
	}

// ------------------------------------------------------------------------------------

    public function getTipo()
	{
	    return $this->txtTipo;
	}
    public function putTipo($parTipo)
	{
	    $this->txtTipo =$parTipo;
	}

// ------------------------------------------------------------------------------------

    public function getEstado()
	{
		return 	$this->txtEstado;
	}
    public function putEstado($parEstado)
	{
	    $this->txtEstado = $parEstado;
	}

// ------------------------------------------------------------------------------------

    public function getIdUsr()
	{
		return 	$this->intIdUsr;
	}
    public function putIdUsr($parIdUsr)
	{
	    $this->intIdUsr=$parIdUsr;
	}

// ------------------------------------------------------------------------------------

	public function listadoArchivosMovimientos($condicion)
	{
		$movexternos = new ModeloMovimientoExterno();
                $query = "SELECT archivocobro.*,sum(movimientocuotas.montosaldo+movimientocuotas.montomora+movimientocuotas.montogastos) as montototal, sum(cobrado) as cobradototal FROM archivocobro,movimientocuotas WHERE movimientocuotas.idarchivocobro=archivocobro.id ".$condicion."GROUP BY archivocobro.id ORDER BY fechaimportacion DESC,nombre";
		$result_all = mysql_query($query);
                
		//echo $query;
		if($result_all)
		{
			while($var = mysql_fetch_object($result_all))
			{
				$arr_archivos[] = array("id"=>$var->id,
									"nombre"=>$var->nombre,
									"fecha"=>fechaACadena($var->fechaimportacion),
									"montototal"=>$var->montototal,
									"cobradototal"=>$var->cobradototal,
									"estado"=>$var->estado,
                                                                        "cantidad"=> $movexternos->contarmovimientos($var->id)
									);
			}
		}
		return($arr_archivos);
	}

// ------------------------------------------------------------------------------------

	public function listadoArchivosSinMovimientos()
	{       $movexternos = new ModeloMovimientoExterno();
		$query = "SELECT * FROM archivocobro WHERE id NOT IN (SELECT idarchivocobro FROM movimientocuotas WHERE idarchivocobro<>'NULL') ORDER BY fechaimportacion DESC,nombre";
		$result_all = mysql_query($query);
		while($var = mysql_fetch_object($result_all))
		{
			$arr_archivos[] = array("id"=>$var->id,
								"nombre"=>$var->nombre,
								"fecha"=>fechaACadena($var->fechaimportacion),
								"estado"=>$var->estado,
                                                                "cantidad"=> $movexternos->contarmovimientos($var->id)
								);
		}
		return($arr_archivos);
	}

// ------------------------------------------------------------------------------------

	public function AltaArchivoCobro()
	{
		$query = ("INSERT INTO archivocobro (nombre,tipo,size,fechaimportacion,idusr)
				VALUES ('$this->txtNombre','$this->txtTipo','$this->intSize',CURRENT_DATE,'$this->intIdUsr')");
		$result_all = mysql_query($query);
		return $result_all;
	}

// ------------------------------------------------------------------------------------

	public function traerarchivo($nombre)
	//retorna los datos de un archivo particular a partir de su nombre
	{
		$query = ("SELECT * FROM archivocobro WHERE nombre='$nombre'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}

// ------------------------------------------------------------------------------------

	public function traerArchivoNombre()
	//retorna los datos de un archivo particular a partir de su nombre, no recibe parametros
	{
		$query = ("SELECT * FROM archivocobro WHERE nombre='$this->txtNombre'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		if($result_all && $num_rows > 0)
		{
			$this->cargarResultados($result_all);
			return true;
		}
		return false;
	}

// ------------------------------------------------------------------------------------

	public function registrarEstado($id_archivo)
	{
		$query = "UPDATE archivocobro SET estado='$this->txtEstado' WHERE id='$id_archivo'";
		$result_all = mysql_query($query);
		return $result_all;
	}

// ------------------------------------------------------------------------------------

	public function cargarResultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
		while ($cons = mysql_fetch_object($resultado))
		{
	        $this->putIdArchivoCobro($cons->id);
	        $this->putNombre($cons->nombre);
	        $this->putTipo($cons->tipo);
            $this->putSize($cons->size);
	        $this->putFechaImportacion(fechaACadena(($cons->fechaimportacion)));
			$this->putEstado($cons->estado);
	        $this->putIdUsr($cons->idusr);
		}
	}



}
?>