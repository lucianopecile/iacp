<?php


class ModeloMovimiento
{
   
    private $intId;
	private $intCobrado;
	private $intMontoSaldo;
	private $intMontoMora;
	private $intMontoGastos;
	private $fecFecha;
	private $intIdCuota;
	private $intIdTipomov;
	private $intIdRegmov;
	private $intTransaccion;
	private $intIdArchivo;

    
// ------------------------------------------------------------------------------------
	
	public function db_connect()
	{
		$config = Config::singleton();
		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		// $this->Conexion_ID=mysql_connect("localhost","root","");
  
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }
        return $this->Conexion_ID;
	}
	
// ------------------------------------------------------------------------------------

	public function __construct()
	{
		$this->db_connect();
	}

// ------------------------------------------------------------------------------------

    public function getId()
	{
	    return $this->intId;
	}

    public function putId($parId)
	{
	    $this->intId = $parId;
	} 

// ------------------------------------------------------------------------------------

    public function getCobrado()
	{
	    return $this->intCobrado;
	}

    public function putCobrado($parCobrado)
	{
	    $this->intCobrado = $parCobrado;
	}

// ------------------------------------------------------------------------------------

    public function getMontoSaldo()
	{
	    return $this->intMontoSaldo;
	}

    public function putMontoSaldo($parMonto)
	{
	    $this->intMontoSaldo = $parMonto;
	}

// ------------------------------------------------------------------------------------

    public function getMontoMora()
	{
	    return $this->intMontoMora;
	}

    public function putMontoMora($parMonto)
	{
	    $this->intMontoMora = $parMonto;
	}

// ------------------------------------------------------------------------------------

    public function getMontoGastos()
	{
	    return $this->intMontoGastos;
	}

    public function putMontoGastos($parMonto)
	{
	    $this->intMontoGastos = $parMonto;
	}

// ------------------------------------------------------------------------------------

    public function getTransaccion()
	{
	    return $this->intTransaccion;
	}

    public function putTransaccion($parTransaccion)
	{
	    $this->intTransaccion =$parTransaccion;
	}

// ------------------------------------------------------------------------------------

    public function getFecha()
	{
	    return $this->fecFecha;
	}  
    public function putFecha($parFecha)
	{
	    $this->fecFecha = $parFecha;
	}

// ------------------------------------------------------------------------------------

    public function getIdCuota()
	{
	    return $this->intIdCuota;
	}

    public function putIdCuota($parIdCuota)
	{
	    $this->intIdCuota = $parIdCuota;
	}

// ------------------------------------------------------------------------------------

    public function getIdTipomov()
	{
	    return $this->intIdTipomov;
	}

    public function putIdTipomov($parIdTipomov)
	{
	    $this->intIdTipomov = $parIdTipomov;
	}

// ------------------------------------------------------------------------------------

    public function getIdRegmov()
	{
	    return $this->intIdRegmov;
	}

    public function putIdRegmov($parIdRegmov)
	{
	    $this->intIdRegmov = $parIdRegmov;
	} 

// ------------------------------------------------------------------------------------

    public function getIdArchivo()
	{
	    return $this->intIdArchivo;
	}

    public function putIdArchivo($parIdArchivo)
	{
	    $this->intIdArchivo = $parIdArchivo;
	}

//----------------------------------------------------------

	public function TraerTodos()
    //retorna la consulta de todos los movimientos sobre las cuotas
	{
		$query =('SELECT * FROM movimientocuotas ORDER BY fecha');
		$result_all = mysql_query($query);
		while ($vartd = mysql_fetch_object($result_all))
		{
			//llenar el array
			$arrMov[]=array($vartd->id,
							$vartd->cobrado,
							$vartd->montosaldo,
							$vartd->montomora,
							$vartd->montogastos,
							$vartd->fecha,
							$vartd->idcuota,
							$vartd->idtipomov,
							$vartd->transaccion,
							$vartd->idregmovimiento,
							$vartd->idarchivocobro
							);
		}
		return $arrMov;
	}

//============================================================================

	public function sumaMovimientosCuota()
	{
		$query = "SELECT sum(montosaldo) as cobradosaldo, sum(montomora) as cobradomora, sum(montogastos) as cobradogastos FROM movimientocuotas WHERE idcuota='$this->intIdCuota'";
		$result_all = mysql_query($query);
		if($result_all)
		{
			$var = mysql_fetch_object($result_all);
			$arr_suma = array("cobradosaldo"=>$var->cobradosaldo,
							"cobradomora"=>$var->cobradomora,
							"cobradogastos"=>$var->cobradogastos
							);
			return $arr_suma;
		}else{
			return false;
		}
	}

//============================================================================

	public function traermovimiento()
	{ 
		$idcta = $this->getIdCuota();
		$query = ("SELECT movimientocuotas.*,tipocobro.descripcion as registro,tipomovimiento.descripcion as tipo
				FROM movimientocuotas,tipocobro,tipomovimiento
				WHERE movimientocuotas.idcuota='$idcta' && tipocobro.id=movimientocuotas.idregmovimiento && movimientocuotas.idtipomov=tipomovimiento.id ORDER BY fecha");
		$result_all = mysql_query($query);
		if($result_all)
		{
			while($vartd = mysql_fetch_object($result_all))
			{
				//llenar el array
				$arrMov[]=array("id"=>$vartd->id,
								"cobrado"=>$vartd->cobrado,
								"montosaldo"=>$vartd->montosaldo,
								"montomora"=>$vartd->montomora,
								"montogastos"=>$vartd->montogastos,
								"fecha"=>fechaACadena($vartd->fecha),
								"idcuota"=>$vartd->idcuota,
								"idtipomov"=>$vartd->idtipomov,
								"tipo"=>$vartd->tipo,
								"registro"=>$vartd->registro,
								"transaccion"=>$vartd->transaccion,
								"idregmov"=>$vartd->idregmovimiento,
							);
			}
		}
		return $arrMov;
	}

//============================================================================

	public function traerMovimientoId()
	{
		$query = ("SELECT movimientocuotas.*,tipocobro.descripcion as registro,tipomovimiento.descripcion as tipo
				FROM movimientocuotas,tipocobro,tipomovimiento
				WHERE movimientocuotas.id='$this->intId' && tipocobro.id=movimientocuotas.idregmovimiento && movimientocuotas.idtipomov=tipomovimiento.id");
		$result_all = mysql_query($query);
		if($result_all)
		{
			$this->cargarresultados($result_all);
			return true;
		}else{
			return false;
		}
	}

//============================================================================

	public function traerFechaMovAnterior()
	//devuelve la fecha del movimiento anterior al indicado con el ID, si no existe devuelve falso
	{
		$query = "SELECT fecha	FROM movimientocuotas WHERE idcuota='$this->intIdCuota' && id<>'$this->intId' ORDER BY fecha DESC LIMIT 1";
		$result_all = mysql_query($query);
		if($result_all)
		{
			while($vartd = mysql_fetch_object($result_all))
			{
				$fecha = fechaACadena($vartd->fecha);
			}
			return $fecha;
		}else{
			return false;
		}
	}

//============================================================================

	public function listadoTotal()
    //retorna la consulta de todos los movimientos de las cuotas
	{
		$query = ("SELECT * FROM movimientocuotas");
		$result_all = mysql_query($query);
		if($result_all)
		{
			while($vartd = mysql_fetch_object($result_all))
			{
				$arrMov[] = array("id"=>$vartd->id,
								"cobrado"=>$vartd->cobrado,
								"montosaldo"=>$vartd->montosaldo,
								"montomora"=>$vartd->montomora,
								"montogastos"=>$vartd->montogastos,
								"fecha"=>$vartd->fecha,
								"idcuota"=>$vartd->idcuota,
								"idtipomov"=>$vartd->idtipomov,
								"transaccion"=>$vartd->transaccion,
								"idregmov"=>$vartd->idregmovimiento
								);
			}
		}
		return  $arrMov;
	}

//============================================================================

	public function listadoMovimientosArchivo()
    //retorna la consulta de todos los movimientos de las cuotas realizados en un mismo archivo de cobro
	{
		$query = "SELECT movimientocuotas.*, cuentas.nrocuenta, cuotas.numerocuota, tipomovimiento.descripcion as tipomov FROM movimientocuotas, cuentas, cuotas, tipomovimiento WHERE idarchivocobro=".$this->intIdArchivo." && movimientocuotas.idtipomov=tipomovimiento.id && cuotas.id=movimientocuotas.idcuota && cuentas.id=cuotas.idcuenta";
		$result_all = mysql_query($query);
		if($result_all)
		{
			while($vartd = mysql_fetch_object($result_all))
			{
				$arrMov[] = array("id"=>$vartd->id,
								"cobrado"=>$vartd->cobrado,
								"montosaldo"=>$vartd->montosaldo,
								"montomora"=>$vartd->montomora,
								"montogastos"=>$vartd->montogastos,
								"fecha"=>$vartd->fecha,
								"nrocuota"=>$vartd->numerocuota,
								"nrocuenta"=>$vartd->nrocuenta,
								"tipomov"=>$vartd->tipomov,
								);
			}
		}
		return $arrMov;
	}

//============================================================================

	public function registrarmovimiento()
    {
		$query = ("INSERT INTO movimientocuotas (cobrado,montosaldo,montomora,montogastos,fecha,idcuota,idtipomov,idregmovimiento,transaccion,idarchivocobro)
				VALUES('$this->intCobrado','$this->intMontoSaldo','$this->intMontoMora','$this->intMontoGastos','$this->fecFecha','$this->intIdCuota','$this->intIdTipomov','$this->intIdRegmov','$this->intTransaccion',$this->intIdArchivo)");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}

//============================================================================

	public function borrarMovimiento()
    {
		$query = "DELETE FROM movimientocuotas WHERE id='$this->intId'";
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}

//============================================================================

	public function borrarMovimientoCuota()
    {
		$query = "DELETE FROM movimientocuotas WHERE idcuota='$this->intIdCuota'";
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}

//============================================================================

	public function setvariables()
	//pone a cero y vacio todas las variables de la clase
	{
		$this->putId(0);
		$this->putCobrado(0);
       	$this->putMontoSaldo(0);
		$this->putMontoMora(0);
		$this->putMontoGastos(0);
        $this->putFecha(0);
       	$this->putIdCuota(0);
	    $this->putIDTipomov(0);
        $this->putIdRegmov(0);   
		$this->putTransaccion(0);
		$this->putIdArchivo(0);
	}

//============================================================================

	public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
		$this->setvariables();
		while ($cons = mysql_fetch_object($resultado))
		{
			$this->putId($cons->id);
			$this->putIdCuota($cons->idcuota);
			$this->putIDTipomov($cons->idtipomov);
			$this->putIdRegmov($cons->idregmovimiento);
			$this->putCobrado($cons->cobrado);
			$this->putMontoSaldo($cons->montosaldo);
			$this->putMontoMora($cons->montomora);
			$this->putMontoGastos($cons->montogastos);
			$this->putFecha(fechaACadena($cons->fecha));
			$this->putTransaccion($cons->transaccion);
			$this->putIdArchivo($cons->idarchivocobro);
		}
	}


}

?>