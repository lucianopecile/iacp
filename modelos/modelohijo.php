<?php


class ModeloHijo
{
    private $intIdHijo;
    private $intIdPoblador;
	private $intIdTipodoc;
	private $txtDocumento;
    private $txtNombres;
    private $txtApellido;
	private $fecFechaNac;
	
    
	public function db_connect()
	{
		$config = Config::singleton();

		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		// $this->Conexion_ID=mysql_connect("localhost","root","");
  
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }

        return $this->Conexion_ID;
	}
	
	
	
	public function __construct()
	{
		$this->db_connect();
	}
	
	
// ------------------------------------------------------------------------------------
	
    public function getIdHijo()
	{
	    return $this->intIdHijo;
	} 

    public function putIdHijo($parIdHijo)
	{
	    $this->intIdHijo = $parIdHijo;
	} 

// ------------------------------------------------------------------------------------
	
    public function getIdPoblador()
	{
	    return $this->intIdPoblador;
	} 

    public function putIdPoblador($parIdPoblador)
	{
	    $this->intIdPoblador= $parIdPoblador;
	} 

// ------------------------------------------------------------------------------------

	public function getIdTipodoc()
	{
	    return $this->intIdTipodoc;
	} 

    public function putIdTipodoc($parIdTipodoc)
	{
	    $this->intIdTipodoc = $parIdTipodoc;
	}
	
// ------------------------------------------------------------------------------------

    public function getDocumento()
	{
	    return $this->intDocumento;
	} 

    public function putDocumento($parDocumento)
	{
	    $this->intDocumento = $parDocumento;
	} 


// ------------------------------------------------------------------------------------

    public function getApellido()
	{
	    return $this->txtApellido;
	} 

    public function putApellido($parApellido)
	{
	    $this->txtApellido = $parApellido;
	}

// ------------------------------------------------------------------------------------

    public function getNombres()
	{
	    return $this->txtNombres;
	} 

    public function putNombres($parNombres)
	{
	    $this->txtNombres = $parNombres;
	}

// ------------------------------------------------------------------------------------

	public function getFechaNac()
	{
		return $this->fecFechaNac;
	}
	
	public function putFechaNac($parFechaNac)
	{
		$this->fecFechaNac = $parFechaNac;
	}
	

//=======================================================================================================================	 
	
	public function listado() 
    //retorna la consulta de todos los hijos de la tabla
	{
    	$query = ('SELECT hijos.* FROM hijos ORDER BY apellido');
		$result_all= mysql_query($query);
      
		while ($varcli = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrHijos[] = array("id"=>$varcli->id,
								"nombres"=>$varcli->apellido.", ".$varcli->nombres);
		} 
		return($arrHijos);	
	}
	


//=======================================================================================================================	 
	
	public function listadonombre() 
    //retorna lista de hijos con un mismo apellido o similar
	{
		$query = ("SELECT hijos.* FROM hijos WHERE apellido LIKE '$this->txtApellido%' ORDER BY apellido");
	
		$result_all = mysql_query($query);
      
		while ($varcli = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrHijos[] = array("id"=>$varcli->idhijo,
  		    					"nombres"=>$varcli->nombres,
								"apellido"=>$varcli->apellido,
								"documento"=>$varcli->documento,
								"fechanac"=>$varcli->fechanac
								);
		} 
		return($arrHijos);	
	}
	
//=======================================================================================================================	 

	public function listadototal() 
    //retorna la consulta de todos los hijos y su documento 
	{
		$query = ('SELECT hijos.idhijo, hijos.nombres, hijos.apellido, hijos.documento, tipodocumento.descripcion FROM hijos, tipodocumento WHERE hijos.idtipodoc = tipodocumento.id  ORDER BY apellido');

		$result_all = mysql_query($query);
		
		while ($varcli = mysql_fetch_object($result_all))
		{
			//llenar el array 
			$arrHijos[] = array("id"=>$varcli->idhijo,
  		    					"nombres"=>$varcli->nombres,
								"apellido"=>$varcli->apellido,
								"documento"=>$varcli->documento,
								"fechanac"=>$varcli->fechanac
								);
		} 
		return($arrHijos);	
	}
	
//=======================================================================================================================	 
	
	public function traerhijo()
	//carga los datos de un hijo particular a partir de un id en las variables del modelo
	//devuelve verdadero o falso en caso de exito o error respectivamente
	{
		$query = ("SELECT hijos.*, tipodocumento.descripcion FROM hijos, tipodocumento WHERE hijos.idtipodoc = tipodocumento.id && hijos.idhijo = '$this->intIdHijo'");
	     
        $result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
	  
		if($result_all && $num_rows > 0)
		{
      		$this->cargarresultados($result_all);
			return(true);	            
      	} else {
	  		return(false);	
	  	}
	}

//=======================================================================================================================	 
	
	public function listarhijospoblador()
	//retorna todos los hijos de un poblador a partir de su id 
	{
		$query = ("SELECT hijos.* FROM hijos WHERE hijos.idpoblador = $this->intIdPoblador");
        $result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		
		while ($varcli = mysql_fetch_object($result_all))
		{
			//llenar el array 
			$arrHijos[] = array("id"=>$varcli->idhijo,
  		    					"nombres"=>$varcli->nombres,
								"apellido"=>$varcli->apellido,
								"idtipodoc"=>$varcli->idtipodoc,
								"documento"=>$varcli->documento,
								"fechanac"=>fechaACadena($varcli->fechanac)
								);
		} 
		return($arrHijos);		
	}
	
//=======================================================================================================================	 
	
	public function borrarhijo()
	{	
		$query=("DELETE FROM hijos WHERE idhijo = '$this->intIdHijo'");
		$result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}
	   
//=======================================================================================================================	 

	public function modificarhijo()
	{
		$query = ("UPDATE hijos SET nombres='$this->txtNombres', apellido='$this->txtApellido', documento='$this->intDocumento', 
					idtipodoc='$this->intIdTipodoc',fechanac='$this->fecFechaNac', idpoblador='$this->intIdPoblador' 
					WHERE idhijo = '$this->intIdHijo'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return($result_all );
	}

//=======================================================================================================================	 

	public function altahijo()
	{
		$query = ("INSERT INTO hijos (documento, idtipodoc, nombres, apellido, fechanac, idpoblador) 
				VALUES ('$this->intDocumento','$this->intIdTipodoc','$this->txtNombres',
				'$this->txtApellido', '$this->fecFechaNac', '$this->intIdPoblador')");
		$result_all = mysql_query($query);
	    return($result_all);
	}

//============================================================================

	public function setvariables()
	//pone a cero y vacio todas las variables de la clase
	{
		$this->putIdHijo(0);
		$this->putIdTipodoc(0);
		$this->putDocumento(0);
		$this->putNombres("");
		$this->putApellido("");
		$this->putFechaNac("");
		$this->putIdPoblador(0);
	}
  
//=======================================================================================================================	 
   
	public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase modelo para ser accedidos desde el controlador
	{
		$this->setvariables();
		
		while ($cons = mysql_fetch_object($resultado))
		{
			$this->putIdHijo($cons->idhijo);
			$this->putIdTipodoc($cons->idtipodoc);
			$this->putDocumento($cons->documento);
			$this->putNombres($cons->nombres);
			$this->putApellido($cons->apellido);
			$this->putFechaNac($cons->fechanac);
			$this->putIdPoblador($cons->idpoblador);
		}
	}
	
 // ------------------------------------------------------------------------------------	
	
	public function buscardocumento()
	//retorna el id del poblador segun su tipo y nro de documento
	{	 
		$condicion = "";
	    //$condicion .= "pobladores.idtipodoc='$this->intIdTipodoc' && ";
		$condicion .= "hijos.documento='$this->intDocumento' ";
	 
	    $query = ("SELECT pobladores.* FROM pobladores WHERE ".$condicion);
		
		$result_all = mysql_query($query);
	    $num_rows = mysql_affected_rows();
	    
        if($result_all)
        {
	         //$this->putIdHijo(0);
	         $this->cargarresultados($result_all);
             return($this->intIdHijo);	            
		} else {
			return(false);	
		}
	}


}
?>