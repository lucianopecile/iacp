<?php


class ModeloParametro
{
   
    private $intId;
    private $intInteresMora;
    private $txtCuentaCorriente;
    private $intConvenio;
    private $intInteresCuotas;
    private $intRecuperoMensura;
    
// ------------------------------------------------------------------------------------
	
	public function db_connect()
	{
		$config = Config::singleton();

		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
  
		if (!$this->Conexion_ID) 
		{
			die('Ha fallado la conexi�n: ' . mysql_error());
			return 0;
		}
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
        {
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }
            return $this->Conexion_ID;
	}
	
// ------------------------------------------------------------------------------------

	public function __construct()
	{
            $this->db_connect();
	}

// ------------------------------------------------------------------------------------

	public function getId()
	{
            return $this->intId;
	}

	public function putId($parId)
	{
            $this->intId = $parId;
	}

// ------------------------------------------------------------------------------------

	public function getCuentaCorriente()
	{
            return $this->txtCuentaCorriente;
	}

	public function putCuentaCorriente($parCuenta)
	{
            $this->txtCuentaCorriente = $parCuenta;
	}

// ------------------------------------------------------------------------------------

	public function getInteresMora()
	{
            return $this->intInteresMora;
	}

	public function putInteresMora($parInt)
	{
            $this->intInteresMora = $parInt;
	}

// ------------------------------------------------------------------------------------

	public function getInteresCuotas()
	{
            return $this->intInteresCuotas;            
	}

	public function putInteresCuotas($interes)
	{
            $this->intInteresCuotas = $interes;            
	}
                
// ------------------------------------------------------------------------------------

	public function getRecuperoMensura()
	{
            return $this->intRecuperoMensura;            
	}

	public function putRecuperoMensura($porcentaje)
	{
            $this->intRecuperoMensura = $porcentaje;            
	}
              
// ------------------------------------------------------------------------------------

	public function getConvenio()
	{
            return $this->intConvenio;
	}

	public function putConvenio($parConvenio)
	{
            $this->intConvenio = $parConvenio;
	}

//============================================================================

	public function modificarInteresMora()
	{
            //modifico siempre el primer registro de la tabla, el valor depende de la columna
            $query = ("UPDATE parametros SET interesmorachubut='$this->intInteresMora' LIMIT 1");
            $result_all = mysql_query($query);
            return $result_all;
	}

//============================================================================

	public function traerInteresMora()
	{
            //devuelvo siempre el primer registro de la tabla, el valor depende de la columna
            $query = ("SELECT interesmorachubut FROM parametros LIMIT 1");
            $result_all = mysql_query($query);
            if($result_all)
            {
                $cons = mysql_fetch_object($result_all);
                $this->intInteresMora = $cons->interesmorachubut;
                return true;
            }else{
                return false;
            }
	}
        
        public function traerIntereses()
	{
            //devuelvo siempre el primer registro de la tabla, el valor depende de la columna
            $query = ("SELECT interesmorachubut, interescuotas, recuperomensura FROM parametros LIMIT 1");
            $result_all = mysql_query($query);
            if($result_all)
            {                
                $cons = mysql_fetch_object($result_all);
                $this->intInteresMora = $cons->interesmorachubut;
                $this->intInteresCuotas = $cons->interescuotas;
                $this->intRecuperoMensura = $cons->recuperomensura;
                return true;
            }else{
                return false;
            }
	}

        public function modificarIntereses()
	{
            //modifico siempre el primer registro de la tabla, el valor depende de la columna
            $query = ("UPDATE parametros SET interesmorachubut='$this->intInteresMora' LIMIT 1");
            $result_all = mysql_query($query);
            $query = ("UPDATE parametros SET interescuotas='$this->intInteresCuotas' LIMIT 1");
            $result_all = mysql_query($query);
            $query = ("UPDATE parametros SET recuperomensura='$this->intRecuperoMensura' LIMIT 1");
            $result_all = mysql_query($query);
            return $result_all;
	}
        
//============================================================================

	public function modificarConvenioCuenta()
	{
		$query = ("UPDATE parametros SET convenio='$this->intConvenio',cuentacorriente='$this->txtCuentaCorriente' LIMIT 1");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}

//============================================================================

	public function traerConvenioCuenta()
	{
            $query = ("SELECT convenio,cuentacorriente FROM parametros LIMIT 1");
            $result_all = mysql_query($query);
            if($result_all)
            {
                $cons = mysql_fetch_object($result_all);
                $this->txtCuentaCorriente = $cons->cuentacorriente;
                $this->intConvenio = $cons->convenio;
                return true;
            }else{
                return false;
            }
	}

//============================================================================

	public function traerParametro()
	{	
            //$query = ("SELECT * FROM parametros WHERE id='$this->intId'");
            $query = ("SELECT * FROM parametros WHERE id='1'");
            $result_all = mysql_query($query);
            if($result_all)
            {
                $this->cargarresultados($result_all);
                return(true);	            
            }else{
                return false;
            }
	}

//==============================================================================================
  
	public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
	    while ($cons = mysql_fetch_object($resultado))
            {
	        $this->putId($cons->id);
	        $this->putInteresMora($cons->interesmorachubut);
	        $this->putConvenio($cons->convenio);
                $this->putCuentaCorriente($cons->cuentacorriente);
                $this->putInteresCuotas($cons->interescuotas);
                $this->putRecuperoMensura($cons->recuperomensura);
            }
	}	

//============================================================================

        public function modificarInteresCuotas()
        {
            //modifico siempre el primer registro de la tabla, el valor depende de la columna
            $query = ("UPDATE parametros SET interescuotas='$this->IntInteresCuotas' LIMIT 1");
            $result_all = mysql_query($query);
            return $result_all;
        }

//============================================================================

        public function traerInteresCuotas()
        {
            //devuelvo siempre el primer registro de la tabla, el valor depende de la columna
            $query = ("SELECT interescuotas FROM parametros LIMIT 1");
            $result_all = mysql_query($query);
            if($result_all)
            {
                $cons = mysql_fetch_object($result_all);
                $this->intInteresCuotas = $cons->interescuotas;
                return true;
            }else{
                return false;
            }
        }
}
?>