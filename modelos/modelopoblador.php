<?php


class ModeloPoblador 
{
    private $intIdPoblador;
	private $intIdTipodoc;
	private $txtDocumento;
    private $txtNombres;
    private $txtApellido;
    private $txtDomicilioPostal;
	private $txtDomicilioLegal;
	private $txtNacionalidad;
	private $txtLugarNac;
	private $fecFechaNac;
    private $txtNombrePadre;
    private $txtNombreMadre;
	private $intVivePadre;
	private $intViveMadre;
	private $txtNacionalidadPadre;
	private $txtNacionalidadMadre;
	private $intIdConyuge;
	private $intIdLocalidad;
	private $txtTelefono;
	private $txtCaracTel;
	private $intIdEstadoCivil;
	
    
	public function db_connect()
	{
		$config = Config::singleton();

		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		// $this->Conexion_ID=mysql_connect("localhost","root","");
  
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }

        return $this->Conexion_ID;
	}
	
	
	
	public function __construct()
	{
		$this->db_connect();
	}
	
	
// ------------------------------------------------------------------------------------
	
    public function getIdPoblador()
	{
	    return $this->intIdPoblador;
	} 

    public function putIdPoblador($parIdPoblador)
	{
	    $this->intIdPoblador = $parIdPoblador;
	} 

// ------------------------------------------------------------------------------------

    public function getIdTipodoc()
	{
	    return $this->intIdTipodoc;
	} 

    public function putIdTipodoc($parIdTipodoc)
	{
	    $this->intIdTipodoc = $parIdTipodoc;
	}
	
// ------------------------------------------------------------------------------------

    public function getDocumento()
	{
	    return $this->txtDocumento;
	}	

    public function putDocumento($parDocumento)
	{
	    $this->txtDocumento = $parDocumento;
	} 


// ------------------------------------------------------------------------------------

    public function getApellido()
	{
	    return $this->txtApellido;
	} 

    public function putApellido($parApellido)
	{
	    $this->txtApellido = $parApellido;
	}

// ------------------------------------------------------------------------------------

    public function getNombres()
	{
	    return $this->txtNombres;
	} 

    public function putNombres($parNombres)
	{
	    $this->txtNombres = $parNombres;
	}

// ------------------------------------------------------------------------------------

    public function getDomicilioPostal()
	{
	    return $this->txtDomicilioPostal;
	} 

    public function putDomicilioPostal($parDomicilioPostal)
	{
	    $this->txtDomicilioPostal = $parDomicilioPostal;
	} 

// ------------------------------------------------------------------------------------

    public function getDomicilioLegal()
	{
	    return $this->txtDomicilioLegal;
	} 

    public function putDomicilioLegal($parDomicilioLegal)
	{
	    $this->txtDomicilioLegal = $parDomicilioLegal;
	} 

// ------------------------------------------------------------------------------------

    public function getNacionalidad()
	{
	    return $this->txtNacionalidad;
	} 

    public function putNacionalidad($parNacionalidad)
	{
	    $this->txtNacionalidad = $parNacionalidad;
	} 

// ------------------------------------------------------------------------------------

	public function getFechaNac()
	{
		return $this->fecFechaNac;
	}
	
	public function putFechaNac($parFechaNac)
	{
		$this->fecFechaNac = $parFechaNac;
	}
	
// ------------------------------------------------------------------------------------

    public function getLugarNac()
	{
	    return $this->txtLugarNac;
	} 

    public function putLugarNac($parLugarNac)
	{
	    $this->txtLugarNac = $parLugarNac;
	} 
	
// ------------------------------------------------------------------------------------

    public function getNombrePadre()
	{
	    return $this->txtNombrePadre;
	} 

    public function putNombrePadre($parNombrePadre)
	{
	    $this->txtNombrePadre = $parNombrePadre;
	}
	
// ------------------------------------------------------------------------------------

    public function getNombreMadre()
	{
	    return $this->txtNombreMadre;
	} 

    public function putNombreMadre($parNombreMadre)
	{
	    $this->txtNombreMadre = $parNombreMadre;
	}

// ------------------------------------------------------------------------------------

    public function getVivePadre()
	{
	    return $this->intVivePadre;
	} 

    public function putVivePadre($parVivePadre)
	{
	    $this->intVivePadre = $parVivePadre;
	}

// ------------------------------------------------------------------------------------

    public function getViveMadre()
	{
	    return $this->intViveMadre;
	} 

    public function putViveMadre($parViveMadre)
	{
	    $this->intViveMadre = $parViveMadre;
	}
// ------------------------------------------------------------------------------------

    public function getNacionalidadPadre()
	{
	    return $this->txtNacionalidadPadre;
	} 

    public function putNacionalidadPadre($parNacionalidadPadre)
	{
	    $this->txtNacionalidadPadre = $parNacionalidadPadre;
	}

// ------------------------------------------------------------------------------------

    public function getNacionalidadMadre()
	{
	    return $this->txtNacionalidadMadre;
	} 

    public function putNacionalidadMadre($parNacionalidadMadre)
	{
	    $this->txtNacionalidadMadre = $parNacionalidadMadre;
	}
	
// ------------------------------------------------------------------------------------

    public function getIdConyuge()
	{
	    return $this->intIdConyuge;
	} 

    public function putIdConyuge($parIdConyuge)
	{
	    $this->intIdConyuge = $parIdConyuge;
	}

// ------------------------------------------------------------------------------------

    public function getIdLocalidad()
	{
	    return $this->intIdLocalidad;
	} 

    public function putIdLocalidad($parIdLocalidad)
	{
	    $this->intIdLocalidad = $parIdLocalidad;
	} 
	
// ------------------------------------------------------------------------------------

    public function getTelefono()
	{
	    return $this->txtTelefono;
	} 

    public function putTelefono($parTelefono)
	{
	    $this->txtTelefono = $parTelefono;
	} 

// ------------------------------------------------------------------------------------

    public function getCaracTel()
	{
	    return $this->txtCaracTel;
	} 

    public function putCaracTel($parCaracTel)
	{
	    $this->txtCaracTel = $parCaracTel;
	} 

// ------------------------------------------------------------------------------------

    public function getIdEstadoCivil()
	{
	    return $this->intIdEstadoCivil;
	} 

    public function putIdEstadoCivil($parIdEstadoCivil)
	{
	    $this->intIdEstadoCivil = $parIdEstadoCivil;
	}


//=======================================================================================================================	 
	
	public function listado() 
    //retorna la consulta de todos los pobladores
	{
    	$query = ('SELECT id, nombres, apellido FROM pobladores ORDER BY apellido');
	
    	$result_all= mysql_query($query);
      
		while ($varcli = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrPobladores[] = array("id"=>$varcli->id,
									"nombres"=>$varcli->apellido.", ".$varcli->nombres
							 
							 	);
		} 
		return($arrPobladores);	
	}
	


//=======================================================================================================================	 
	
	public function listadonombre() 
    //retorna lista de pobladores con un mismo apellido y muestra el documento
	{
		$query = ("SELECT pobladores.id,pobladores.nombres,pobladores.apellido,pobladores.documento,pobladores.idtipodoc,tipodocumento.descripcion as tipodoc,localidades.descripcion FROM pobladores,localidades,tipodocumento WHERE pobladores.idlocalidad=localidades.id && pobladores.idtipodoc=tipodocumento.id && apellido LIKE '$this->txtApellido%' ORDER BY apellido");
	
		$result_all = mysql_query($query);
      
		while ($varcli = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrPobladores[] = array("id"=>$varcli->id,
  		    					"nombres"=>$varcli->nombres,
								"apellido"=>$varcli->apellido,
								"localidad"=>$varcli->descripcion,
								"documento"=>$varcli->documento,
								"tipodoc"=>$varcli->tipodoc
								);
		} 
		return($arrPobladores);	
	}

//=======================================================================================================================	 
	
	public function listadonombreconyuges() 
    //retorna lista de pobladores con un mismo apellido y muestra el documento
	{
		$query = ("SELECT solteros.id, solteros.nombres ,solteros.apellido, solteros.documento, tipodocumento.descripcion FROM (SELECT *  FROM pobladores WHERE idconyuge IS NULL) as solteros, tipodocumento WHERE tipodocumento.id = solteros.idtipodoc && apellido LIKE '$this->txtApellido%' ORDER BY apellido");
		$result_all = mysql_query($query);
      
		while ($varcli = mysql_fetch_object($result_all))
		{
	 		//llenar el array 
			$arrPobladores[] = array("id"=>$varcli->id,
  		    					"nombres"=>$varcli->nombres,
								"apellido"=>$varcli->apellido,
								"descripcion"=>$varcli->descripcion,
								"documento"=>$varcli->documento
								);
		} 
		return($arrPobladores);	
	}
	
//=======================================================================================================================	 
	
	public function listadototal() 
    //retorna la consulta de todos los pobladores y su localidad 
	{
		$query = ('SELECT pobladores.id, pobladores.nombres, pobladores.apellido, pobladores.documento, localidades.descripcion, tipodocumento.descripcion as tipodoc FROM pobladores, localidades, tipodocumento WHERE pobladores.idlocalidad = localidades.id && pobladores.idtipodoc=tipodocumento.id  ORDER BY apellido');
	  
		$result_all = mysql_query($query);
      
		while ($varcli = mysql_fetch_object($result_all))
		{
			//llenar el array 
			$arrPobladores[] = array("id"=>$varcli->id,
  		    					"nombres"=>$varcli->nombres,
								"apellido"=>$varcli->apellido,
								"documento"=>$varcli->documento,
								"localidad"=>$varcli->descripcion,
								"tipodoc"=>$varcli->tipodoc,
								);
		} 
		return($arrPobladores);	
	}
	
//=======================================================================================================================	 
	
	public function traerpoblador()
	//retorna los datos de un poblador particular a partir de un id 
	{
		$query = ("SELECT pobladores.*, tipodocumento.descripcion FROM pobladores, tipodocumento WHERE tipodocumento.id = pobladores.idtipodoc && pobladores.id = '$this->intIdPoblador'");
	     
        $result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
	  
		if($result_all && $num_rows > 0)
		{
      		$this->cargarresultados($result_all);
			return(true);	            
      	} else {
	  		return(false);	
	  	}
	}

//=======================================================================================================================	 
	
	public function traerconyugepoblador()
	//retorna los datos de un poblador particular a partir de un id 
	{
		$query = ("SELECT pobladores.*, tipodocumento.descripcion FROM pobladores, tipodocumento WHERE tipodocumento.id = pobladores.idtipodoc && pobladores.idconyuge = '$this->intIdPoblador'");
	     
        $result_all = mysql_query($query);
		
		if($result_all)
		{
      		$this->cargarresultados($result_all);
			return(true);	            
      	} else {
	  		return(false);	
	  	}
	}
	
//=======================================================================================================================	 
	
	public function borrarpoblador()
	{	
		$query=("DELETE FROM pobladores WHERE id = '$this->intIdPoblador'");
		$result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && $num_rows > 0);
	}
     	
//=======================================================================================================================	 

	public function modificarpoblador()
	{
		$query = ("UPDATE pobladores SET nombres='$this->txtNombres', apellido='$this->txtApellido', idtipodoc='$this->intIdTipodoc', 
					documento='$this->txtDocumento', telefono='$this->txtTelefono', caracteristica='$this->txtCaracTel', 
					domiciliopostal='$this->txtDomicilioPostal', domiciliolegal='$this->txtDomicilioLegal', idlocalidad='$this->intIdLocalidad', 
					nombrepadre='$this->txtNombrePadre', nombremadre='$this->txtNombreMadre', vivepadre='$this->intVivePadre', 
					vivemadre='$this->intViveMadre', nacionalidadpadre='$this->txtNacionalidadPadre', nacionalidadmadre='$this->txtNacionalidadMadre',
					nacionalidad='$this->txtNacionalidad', lugarnacimiento='$this->txtLugarNac', fechanac='$this->fecFechaNac',
					idestadocivil='$this->intIdEstadoCivil' WHERE id = '$this->intIdPoblador'");
		$result_all = mysql_query($query);
		return($result_all );
	}

//=======================================================================================================================	 

	public function altapoblador()
	{
		$query = ("INSERT INTO pobladores(idtipodoc, documento, nombres, apellido, domiciliolegal, domiciliopostal, nombrepadre, nombremadre,
				vivepadre, vivemadre, nacionalidadpadre, nacionalidadmadre, idconyuge, telefono, caracteristica, nacionalidad, lugarnacimiento,
				fechanac, idlocalidad, idestadocivil) VALUES ('$this->intIdTipodoc', '$this->txtDocumento','$this->txtNombres', '$this->txtApellido',
				'$this->txtDomicilioLegal', '$this->txtDomicilioPostal', '$this->txtNombrePadre', '$this->txtNombreMadre', '$this->intVivePadre',
				'$this->intViveMadre', '$this->txtNacionalidadPadre', '$this->txtNacionalidadMadre', $this->intIdConyuge, '$this->txtTelefono',
				'$this->txtCaracTel', '$this->txtNacionalidad', '$this->txtLugarNac', '$this->fecFechaNac', '$this->intIdLocalidad', '$this->intIdEstadoCivil')");
		$result_all = mysql_query($query);
		if ($result_all)
		{
			define('IDPOB', mysql_insert_id());
			return IDPOB;
		} else {
			return 0;
		}
	}

//=======================================================================================================================

	public function altaconyugepoblador()
	{
		$query = ("INSERT INTO pobladores(idtipodoc, documento, nombres, apellido, domiciliolegal, domiciliopostal, nombrepadre, nombremadre,
				vivepadre, vivemadre, nacionalidadpadre, nacionalidadmadre, idconyuge, telefono, caracteristica, nacionalidad, lugarnacimiento,
				fechanac, idlocalidad, idestadocivil) VALUES ('$this->intIdTipodoc', '$this->txtDocumento','$this->txtNombres', '$this->txtApellido',
				'$this->txtDomicilioLegal', '$this->txtDomicilioPostal', '$this->txtNombrePadre', '$this->txtNombreMadre', '$this->intVivePadre',
				'$this->intViveMadre', '$this->txtNacionalidadPadre', '$this->txtNacionalidadMadre', $this->intIdConyuge, '$this->txtTelefono',
				'$this->txtCaracTel', '$this->txtNacionalidad', '$this->txtLugarNac', '$this->fecFechaNac', '$this->intIdLocalidad', '$this->intIdEstadoCivil')");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		if ($result_all && ($num_rows > 0))
		{
			define('idPoblador',mysql_insert_id()); /* obtengo el id del ultimo insert en la DB */
			$this->putIdPoblador(idPoblador);
			$query = ("UPDATE pobladores SET idconyuge='$this->intIdPoblador' WHERE id='$this->intIdConyuge'");
			$result_all = mysql_query($query);
			$num_rows = mysql_affected_rows();
			if ($result_all && ($num_rows > 0))
			{
				return ($result_all);
			} else {
				$query = ("DELETE * FROM pobladores WHERE idconyuge='$this->intIdPoblador'");
				$result_all = mysql_query($query);
				$num_rows = mysql_affected_rows();
				return false;
			}
		} else {
			return false;
		}
	}
	
//=======================================================================================================================	 
	
	public function borrarconyugepoblador()
	{
		$query = ("UPDATE pobladores SET idconyuge=NULL WHERE id='$this->intIdConyuge'");
		$result_all = mysql_query($query);
		
		if ($result_all)
		{
			$query=("DELETE FROM pobladores WHERE id = '$this->intIdPoblador'");
			$result_all=mysql_query($query);
			$num_rows = mysql_affected_rows();
			return ($result_all && $num_rows > 0);
		} else {
			return ($result_all && $num_rows > 0);
		}
	}

//=======================================================================================================================	 

	public function desvincularconyugepoblador()
	// quita los apuntadores entre conyuge y poblador desde ambas partes. si falla una consulta deshace la operacion
	{
		$query = ("UPDATE pobladores SET idconyuge = NULL WHERE id = '$this->intIdPoblador'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		if ($result_all && ($num_rows > 0))
		{
			$query = ("UPDATE pobladores SET idconyuge = NULL WHERE id = '$this->intIdConyuge'");
			$result_all = mysql_query($query);
			$num_rows = mysql_affected_rows();
			if ($result_all && ($num_rows > 0))
			{
				return($result_all );
			} else {
				$query = ("UPDATE pobladores SET idconyuge = '$this->intIdConyuge' WHERE id = '$this->intIdPoblador'");
				$result_all = mysql_query($query);
				$num_rows = mysql_affected_rows();
				return false;
			}
		} else {
			return ($result_all);
		}
	}

//=======================================================================================================================	 

	public function vincularConyugePoblador()
	// vincula las claves del poblador y del conyuge
	{
		$query = ("UPDATE pobladores SET idconyuge='$this->intIdConyuge' WHERE id='$this->intIdPoblador'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		if ($result_all && ($num_rows > 0))
		{
			$query = ("UPDATE pobladores SET idconyuge='$this->intIdPoblador' WHERE id='$this->intIdConyuge'");
			$result_all = mysql_query($query);
			$num_rows = mysql_affected_rows();
			if ($result_all && ($num_rows > 0))
			{
				return($result_all );
			} else {
				$query = ("UPDATE pobladores SET idconyuge=NULL WHERE id='$this->intIdPoblador'");
				$result_all = mysql_query($query);
				$num_rows = mysql_affected_rows();
				return false;
			}
		} else {
			return ($result_all);
		}
	}
	
//============================================================================

	public function setvariables()
	//pone a cero y vacio todas las variables de la clase
	{
		$this->putIdPoblador(0);
		$this->putIdTipodoc(0);
		$this->putDocumento(0);
		$this->putNombres("");
		$this->putApellido("");
		$this->putNacionalidad("");
		$this->putLugarNac("");
		$this->putFechaNac("");
		$this->putDomicilioLegal("");
		$this->putDomicilioPostal("");
		$this->putNombrePadre("");
		$this->putNombreMadre("");
		$this->putVivePadre(0);
		$this->putViveMadre(0);
		$this->putNacionalidadPadre("");
		$this->putNacionalidadMadre("");
		$this->putIdConyuge(0);
		$this->putTelefono("");
		$this->putCaracTel("");
		$this->putIdLocalidad(0);
		$this->putIdEstadoCivil(0);
	}
  
//=======================================================================================================================	 
   
	public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
		$this->setvariables();
		
		while ($cons = mysql_fetch_object($resultado))
		{
			$this->putIdPoblador($cons->id);
			$this->putIdTipodoc($cons->idtipodoc);
			$this->putDocumento($cons->documento);
			$this->putNombres($cons->nombres);
			$this->putApellido($cons->apellido);
			$this->putNacionalidad($cons->nacionalidad);
			$this->putLugarNac($cons->lugarnacimiento);
			$this->putFechaNac($cons->fechanac);
			$this->putDomicilioLegal($cons->domiciliolegal);
			$this->putDomicilioPostal($cons->domiciliopostal);
			$this->putNombrePadre($cons->nombrepadre);
			$this->putNombreMadre($cons->nombremadre);
			$this->putVivePadre($cons->vivepadre);
			$this->putViveMadre($cons->vivemadre);
			$this->putNacionalidadPadre($cons->nacionalidadpadre);
			$this->putNacionalidadMadre($cons->nacionalidadmadre);
			$this->putIdConyuge($cons->idconyuge);
			$this->putTelefono($cons->telefono);
			$this->putCaracTel($cons->caracteristica);
			$this->putIdLocalidad($cons->idlocalidad);
			$this->putIdEstadoCivil($cons->idestadocivil);
		}
	}
	
 // ------------------------------------------------------------------------------------	
	
	public function buscardocumento()
	//retorna el id del poblador segun su tipo y nro de documento
	{	 
		$condicion = "pobladores.documento='$this->txtDocumento' ";
	 
	    $query = ("SELECT pobladores.* FROM pobladores WHERE ".$condicion);
		$result_all = mysql_query($query);
		
	    
        if($result_all)
        {
	         $this->putIdPoblador(0);
	         $this->cargarresultados($result_all);
             return($this->intIdPoblador);	            
		} else {
			return(false);	
		}
	}

// ------------------------------------------------------------------------------------	
	
	public function existedocumento()
	//retorna si ya existe un poblador con el nro de documento indicado
	{
	    $query = ("SELECT * FROM pobladores WHERE documento='$this->txtDocumento' && id <> '$this->intIdPoblador'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();

		if($result_all && ($num_rows > 0))
			return true;
		else
			return(false);	
	}
	

}
?>