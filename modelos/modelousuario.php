<?PHP
class modelousuario 

{
    private $intIdUsuario;
    private $txtUsuario;
    private $txtClave;	
    private $txtNombreCompleto;
    private $intAcceso;
    private $intIdDepartamento;
    private $intIdEmpleado;
    private $arrPerfil;

//============================================================================
	
  public function db_connect()
{
    $config = Config::singleton();

    $this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
  
        if (!$this->Conexion_ID) 
        {
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }
 
        return $this->Conexion_ID;
   
}
	
	
//============================================================================
	
    public function __construct()
    {
	
        $this->db_connect();
        $this->view = new View();
    }
 

// ------------------------------------------------------------------------------------
    public function getIdUsuario()
    {
        return $this->intIdUsuario;
    } 
    public function putIdUsuario($parIdUsuario)
    {
        $this->intIdUsuario =$parIdUsuario;
    } 
// ------------------------------------------------------------------------------------
     public function getUsuario()
    {
        return $this->txtUsuario;
    } 
    public function putUsuario($parUsuario)
    {
        $this->txtUsuario =$parUsuario;
    } 
// ------------------------------------------------------------------------------------
    public function getClave()
    {
        return $this->txtClave;
    } 
    public function putClave($parClave)
    {
        $this->txtClave =$parClave;
    } 
	
// ------------------------------------------------------------------------------------
    public function getNombreCompleto()
    {
        return $this->txtNombreCompleto;
    } 
    public function putNombreCompleto($parNombreCompleto)
    {
        $this->txtNombreCompleto =$parNombreCompleto;
    } 
// ------------------------------------------------------------------------------------
    public function getAcceso()
    {
        return $this->intAcceso;
    } 
    public function putAcceso($parAcceso)
    {
        $this->intAcceso =$parAcceso;
    } 
// ------------------------------------------------------------------------------------
    public function getIdDepartamento()
    {
        return $this->intIdDepartamento;
    } 
    public function putIdDepartamento($parIdDepartamento)
    {
        $this->intIdDepartamento =$parIdDepartamento;
    } 			
// ------------------------------------------------------------------------------------
    public function getIdEmpleado()
    {
        return $this->intIdEmpleado;
    } 
    public function putIdEmpleado($parIdEmpleado)
    {
        $this->intIdEmpleado =$parIdEmpleado;
    } 			
	
// ------------------------------------------------------------------------------------
    public function getPerfil()
    {
            return 	$this->arrPerfil;
    } 
    public function putPerfil($parPerfil=array())
    {
        $this->arrPerfil=$parPerfil;
    } 
	
//============================================================================
    public function TraerTodos() 
    //retorna la consulta de todos los empleados con tipoemp=templeado
    {  
	 
        $query =("SELECT usuarios.usuario,usuarios.nombrecompleto FROM usuarios ");
	
        $result_all=mysql_query($query);
      
        while ($varusr = mysql_fetch_object($result_all)) {
            //llenar el array 
            $arrUsuarios[]=array("usuario"=>$varusr->usuario,
                                "nombrecompleto"=>$varusr->nombrecompleto
				);					                   
        } 
		
	return($arrUsuarios);	
    }
					
//============================================================================
    public function traerusuario()	 	 
    { 
	
        $username=$this->getUsuario();
        $query = ("SELECT usuarios.id, usuarios.nombrecompleto,  usuarios.clave, usuarios.usuario FROM usuarios WHERE usuarios.usuario='$username'   ");

        $result_all=mysql_query($query);
 
        if($result_all)
        {      
            $todo =mysql_fetch_object($result_all);    	
	    return($todo);
        }
        else
        {
            echo "No se encontro usuario";
            return(false);
        }
   }	 
//============================================================================
    public function traerdatosusuario()	 	 
    { 	
	$userid=$this->getIdUsuario();
        $query = ("SELECT usuarios.id, usuarios.nombrecompleto, usuarios.clave, usuarios.usuario, usuarios.iddepartamento,  usuarios.idempleado FROM usuarios WHERE usuarios.id='$userid'   ");
	$result_all=mysql_query($query);
 
        if($result_all)
        {      
            $this->cargarresultados($result_all);
	     return(true);
	}
	else
        {
            echo "No se encontro usuario";
            return(false);
	}
   }	 
	 	 	 
//============================================================================
	public function listadoTotal()  
	{
   	
	 $query =("SELECT usuarios.id,usuarios.nombrecompleto,usuarios.usuario,usuarios.clave,usuarios.idempleado FROM usuarios    ORDER BY nombrecompleto");
     $result_all=mysql_query($query);
      while ($vartd = mysql_fetch_object($result_all)) {
					$arrUsuarios[]=array( 
										"id"=>$vartd->id,
   										"nombrecompleto"=>$vartd->nombrecompleto,
										"usuario"=>$vartd->usuario
										);
					
			 
	               
      } 
	return  $arrUsuarios;
	
}	

//============================================================================
	
     public function borrarusuario()
     {	
     
	  $usuarioactivo=$_SESSION["s_idusr"];
      $query=("DELETE FROM usuarios WHERE id = '$this->intIdUsuario' && !(id='$usuarioactivo')  ");
      $result_all=mysql_query($query);
	  $num_rows = mysql_affected_rows();
	  
       if($result_all  ){
	      if($num_rows>0){
            return(true);}
		  else{
		    echo "No se puede dar de baja el usuario activo";
			return(true);
		  }	
	   }
	   else{
	      return(false);
	   }	  

	  }
	   
     	
//============================================================================

     public function modificarusuario()
     {
	  
	 
       $usuarioactivo=$_SESSION["s_idusr"];
       $query = ("UPDATE usuarios SET nombrecompleto= '$this->txtNombreCompleto', iddepartamento='$this->intIdDepartamento', usuario='$this->txtUsuario',clave='$this->txtClave',idempleado='$this->intIdEmpleado'  WHERE id = '$this->intIdUsuario'");
	  
      $result_all=mysql_query($query);
	  $num_rows = mysql_affected_rows();

      if($result_all  ){
	     
	    	 	   $this->ActualizarPerfil();
				   
		 		   
         return(true);
      }
	  else{
	   return(false);
	  }
}
//============================================================================

     public function modificarclave()
     {
	  
	 
       $usuarioactivo=$_SESSION["s_idusr"];
       $query = ("UPDATE usuarios SET clave='$this->txtClave' WHERE id = '$this->intIdUsuario'");
	  
      $result_all=mysql_query($query);

      if($result_all  ){
	     
         return(true);
      }
	  else{
	   return(false);
	  }
}

//============================================================================


     public function altausuario()
     {

      
     $query = ("INSERT INTO usuarios (nombrecompleto,  usuario, clave,iddepartamento,idempleado) VALUES ('$this->txtNombreCompleto', '$this->txtUsuario','$this->txtClave',$this->intIdDepartamento,'$this->intIdEmpleado')");
	 $result_all=mysql_query($query);
     if($result_all){
	      define('IDUsuario',mysql_insert_id());
          $this->putIdUsuario(IDUsuario);
          $altaperfil=$this->ActualizarPerfil();
          if(altaperfil){
              $this->view->show1("bridge.html","");	

		    return(true);
		  }
          else{
	         return(false);
	      }
       
      }
	  else{
	   return(false);
	  }

  }
  

//============================================================================

	public function setvariables()
	//pone a cero y vacio todas las variables de la clase
	{
        $this->putIdUsuario(0);
        $this->putNombreCompleto("");
        $this->putUsuario("");
	    $this->putClave("");
		$this->putAcceso(0);
		$this->putidsucursal(0);
		$this->putIdEmpleado(0);
	}  
//============================================================================
 
  
  public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
	   
	    
	    while ($cons = mysql_fetch_object($resultado)) {
	 		    
        $this->putIdUsuario($cons->id);
        $this->putNombreCompleto($cons->nombrecompleto);
        $this->putUsuario($cons->usuario);
	    $this->putClave($cons->clave);
		$this->putIdDepartamento($cons->iddepartamento);
		$this->putIdEmpleado($cons->idempleado);
		
		}
       
		
       
		
	}
	
  
 	
//============================================================================
	public function recuperarPerfil()
	{
        $arrPerfil='';

		$query="select perfiles1.iditems,menu.descripcion as descmenu,perfiles1.descripcion as descperf, perfiles1.idusuario from menu , (select perfiles.*, menu.descripcion, menu.idparent from perfiles,menu where menu.id=perfiles.iditems && perfiles.idusuario='$this->intIdUsuario') as perfiles1  where menu.idparent=0 && perfiles1.idparent=menu.id order by menu.id,perfiles1.iditems";
		
        $result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
		if ($result_all) 
		{ 
		  
		   if($num_rows>0) {
           while ($vartd = mysql_fetch_object($result_all)) {
       			$arrPerfil[]=array( 
										$vartd->iditems,
										$vartd->descmenu."->".$vartd->descperf,
   										$vartd->idusuario
										);
				
                   
      
	
			}
		}	
      	
		}
		return 	$arrPerfil;
		}
		
//============================================================================
	public function traerPerfil()
	{
        $arrPerfil='';
		$query="select perfiles.*, menu.callcontrolador, menu.callaccion from perfiles,menu where menu.id=perfiles.iditems && perfiles.idusuario='$this->intIdUsuario' ";
		
		
        $result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
		if ($result_all) 
		{ 
		   if($num_rows>0) {

           while ($vartd = mysql_fetch_object($result_all)) {
		   
       			$arrPerfil[]=array( 
										"id"=>$vartd->iditems,
										"controlador"=>$vartd->callcontrolador,
										"accion"=>$vartd->callaccion,
   										"usr"=>$vartd->idusuario
										);
				
                   
               
	
			}
		}	
      	
		}
		return 	$arrPerfil;
		}		
		
//============================================================================
	public function TraerDetalle() 
    //retorna la consulta de todas las empresas
	{
    $arrPerfil=$this->recuperarPerfil();
	
	return $arrPerfil;
	
	
	
}	

//============================================================================
private function ActualizarPerfil()
{

		$query="delete from perfiles WHERE idusuario='$this->intIdUsuario'" ;
		$result_all=mysql_query($query); 
        $parArray[1]="0";
		$parArray=$this->getPerfil();
		$ItemMenu=0;
	    $perfil= new modeloperfil();
		if (is_array($parArray)) 
		{

		    foreach($parArray as $ItemMenu)
		    { 
			  $IdUsr=$this->getIdUsuario();
			
			  $pudoagregar=$perfil->AgregarPadre($ItemMenu,$IdUsr);
			  
			  if($pudoagregar){
			    $perfil->putIdUsuario($IdUsr);
  			    $perfil->putIdItems($ItemMenu);
			    $pudoagregar=$perfil->altaperfil();
				
				if(!$pudoagregar){

			       return(false);
			    } 
			  }
			   else{

			    return(false);
			   }
  			
			
            } 
		}
		 $this->putPerfil($this->TraerDetalle());
}		
 
  	 
 
	

}


?>

