<?php

class ModeloSolicitud 
{
    private $intIdSolicitud;
	private $intIdPoblador;
	private $intIdUsrCreador;
	private $intIdUsrMod;
	private $fecUltMod;
	private $fecFechaSolicitud;
	private $intIdLocalidad;
	private $txtObservacion;
	private $intIdEstadoSolicitud;
	private $intIdGradoTenencia;
	private $intAnioExpediente;
	private $txtLetraExpediente;
	private $intNroExpediente;
	private $booResidente;
	private $fecResideDesde;
	private $booEmpleadoPublico;
	private $txtReparticion;
	private $intIdEmpleado;
    private $txtNombres;
    private $txtApellido;
	private $intTipoSolicitud;
	private $intSuperficie;
	private $intIdUnidad;

//------------------------------------------------------------------------------------

	public function db_connect()
	{
		$config = Config::singleton();
		$this->Conexion_ID=mysql_connect($config->get('dbhost'),$config->get('dbuser'), $config->get('dbpass'));
		// $this->Conexion_ID=mysql_connect("localhost","root","");
		if (!$this->Conexion_ID) 
		{
            die('Ha fallado la conexi�n: ' . mysql_error());
            return 0;
        }
        //seleccionamos la base de datos
        if (!@mysql_select_db($config->get('dbname'),$this->Conexion_ID)) 
		{
            echo "Imposible abrir " . $config->get('dbname') ;
            return 0;
        }
        return $this->Conexion_ID;
	}
	
	public function __construct()
	{
	  $this->db_connect();
	}
	
// ------------------------------------------------------------------------------------

    public function getIdSolicitud()
	{
	    return $this->intIdSolicitud;
	} 

    public function putIdSolicitud($parIdSolicitud)
	{
	    $this->intIdSolicitud = $parIdSolicitud;
	} 

// ------------------------------------------------------------------------------------

    public function getIdPoblador()
	{
	    return $this->intIdPoblador;
	} 

    public function putIdPoblador($parIdPoblador)
	{
	    $this->intIdPoblador = $parIdPoblador;
	} 
// ------------------------------------------------------------------------------------

    public function getApellido()
	{
	    return $this->txtApellido;
	} 

    public function putApellido($parApellido)
	{
	    $this->txtApellido = $parApellido;
	}

// ------------------------------------------------------------------------------------

    public function getNombres()
	{
	    return $this->txtNombres;
	} 

    public function putNombres($parNombres)
	{
	    $this->txtNombres = $parNombres;
	}	
// ------------------------------------------------------------------------------------

    public function getIdUsrCreador()
	{
	    return $this->intIdUsrCreador;
	} 

    public function putIdUsrCreador($parIdUsrCreador)
	{
	    $this->intIdUsrCreador = $parIdUsrCreador;
	} 

// ------------------------------------------------------------------------------------

    public function getIdUsrMod()
	{
	    return $this->intIdUsrMod;
	} 

    public function putIdUsrMod($parIdUsrMod)
	{
	    $this->intIdUsrMod = $parIdUsrMod;
	} 
// ------------------------------------------------------------------------------------

    public function getUsrMod()
	{
	    return $this->intUsrMod;
	} 

    public function putUsrMod($parUsrMod)
	{
	    $this->intUsrMod = $parUsrMod;
	} 

// ------------------------------------------------------------------------------------

    public function getUltMod()
	{
	    return $this->fecUltMod;
	} 

    public function putUltMod($parUltMod)
	{
	    $this->fecUltMod = $parUltMod;
	} 
	
// ------------------------------------------------------------------------------------

    public function getFechaSolicitud()
	{
	    return $this->fecFechaSolicitud;
	} 

    public function putFechaSolicitud($parFechaSolicitud)
	{
	    $this->fecFechaSolicitud = $parFechaSolicitud;
	}

// ------------------------------------------------------------------------------------

    public function getIdLocalidad()
	{
	    return $this->intIdLocalidad;
	} 

    public function putIdLocalidad($parIdLocalidad)
	{
	    $this->intIdLocalidad = $parIdLocalidad;
	} 

// ------------------------------------------------------------------------------------
	
    public function getObservacion()
	{
		return 	$this->txtObservacion;
	} 
    
	public function putObservacion($parObservacion)
	{
	    $this->txtObservacion = $parObservacion;
	} 

// ------------------------------------------------------------------------------------
	
	public function getIdEstadoSolicitud()
	{
		return 	$this->intIdEstadoSolicitud;
	} 
    
	public function putIdEstadoSolicitud($parIdEstadoSolicitud)
	{
	    $this->intIdEstadoSolicitud = $parIdEstadoSolicitud;
	} 
	
// ------------------------------------------------------------------------------------

	public function getIdGradoTenencia()
	{
		return $this->intIdGradoTenencia;
	}
	
	public function putIdGradoTenencia($parIdGradoTenencia)
	{
		$this->intIdGradoTenencia = $parIdGradoTenencia;
	}

// ------------------------------------------------------------------------------------

	public function getAnioExpediente()
	{
		return $this->intAnioExpediente;
	}
	
	public function putAnioExpediente($parAnioExpediente)
	{
		$this->intAnioExpediente = $parAnioExpediente;
	}
	
// ------------------------------------------------------------------------------------

	public function getNroExpediente()
	{
		return $this->intNroExpediente;
	}
	
	public function putNroExpediente($parNroExpediente)
	{
		$this->intNroExpediente = $parNroExpediente;
	}
	
// ------------------------------------------------------------------------------------

	public function getLetraExpediente()
	{
		return $this->txtLetraExpediente;
	}
	
	public function putLetraExpediente($parLetraExpediente)
	{
		$this->txtLetraExpediente = $parLetraExpediente;
	}
	
// ------------------------------------------------------------------------------------

	public function getResidente()
	{
		return $this->booResidente;
	}
	
	public function putResidente($parResidente)
	{
		$this->booResidente = $parResidente;
	}
	
// ------------------------------------------------------------------------------------

	public function getFechaResideDesde()
	{
		return $this->fecResideDesde;
	}
	
	public function putFechaResideDesde($parResideDesde)
	{
		$this->fecResideDesde = $parResideDesde;
	}
	
// ------------------------------------------------------------------------------------

	public function getEmpleadoPublico()
	{
		return $this->booEmpleadoPublico;
	}
	
	public function putEmpleadoPublico($parEmpleadoPublico)
	{
		$this->booEmpleadoPublico = $parEmpleadoPublico;
	}

// ------------------------------------------------------------------------------------

	public function getReparticion()
	{
		return $this->txtReparticion;
	}
	
	public function putReparticion($parReparticion)
	{
		$this->txtReparticion = $parReparticion;
	}
	
// ------------------------------------------------------------------------------------

	public function getIdEmpleado()
	{
		return $this->intIdEmpleado;
	}
	
	public function putIdEmpleado($parIdEmpleado)
	{
		$this->intIdEmpleado = $parIdEmpleado;
	}

// ------------------------------------------------------------------------------------

    public function getTipoSolicitud()
	{
	    return $this->intTipoSolicitud;
	} 

    public function putTipoSolicitud($parTipoSolicitud)
	{
	    $this->intTipoSolicitud = $parTipoSolicitud;
	} 

// ------------------------------------------------------------------------------------

	public function getIdUnidad()
	{
		return $this->intIdUnidad;
	}
	
	public function putIdUnidad($parIdUnidad)
	{
		$this->intIdUnidad = $parIdUnidad;
	}
// ------------------------------------------------------------------------------------

	public function getSuperficie()
	{
		return $this->intSuperficie;
	}
	
	public function putSuperficie($parSuperficie)
	{
		$this->intSuperficie = $parSuperficie;
	}
	
// ------------------------------------------------------------------------------------

	public function listadoTotal() 
	//retorna la consulta de todas los solicitudes
	{		
		$query = ('SELECT solicitudes.*, pobladores.apellido, pobladores.nombres, estadosolicitud.descripcion as estado, gradotenencia.descripcion as grado, localidades.descripcion as localidad FROM solicitudes,pobladores,estadosolicitud,gradotenencia,localidades WHERE solicitudes.idpoblador=pobladores.id && solicitudes.idestadosolicitud=estadosolicitud.id && solicitudes.idgradotenencia=gradotenencia.id && solicitudes.idlocalidad=localidades.id');
		$result_all=mysql_query($query);
		if($result_all)
		{
			while ($varsol = mysql_fetch_object($result_all))
			{
			   if($varsol->tiposolicitud==RURAL)
			       $tiposolicitud="RURAL";
			   else
			       $tiposolicitud="URBANA";
				   	   
	 			//llenar el array 
				$arrSolicitudes[]=array("id"=>$varsol->id,
                        	"nroexpediente"=>$varsol->nroexpediente,
							"anioexpediente"=>$varsol->anioexpediente,
							"letraexpediente"=>$varsol->letraexpediente,
							"idpoblador"=>$varsol->idpoblador,
							"apellido"=>$varsol->apellido,
							"nombres"=>$varsol->nombres,
							"fechasolicitud"=>fechaACadena($varsol->fechasolicitud),
							"idestadosolicitud"=>$varsol->idestadosolicitud,
							"estado"=>$varsol->estado, 
							"tiposolicitud"=>$tiposolicitud, 
							"grado"=>$varsol->grado,
							"localidad"=>$varsol->localidad);
			} 
		}
		return($arrSolicitudes);	
	}
// ------------------------------------------------------------------------------------

	public function listadoTotalRurales() 
	//retorna la consulta de todas los solicitudes
	{
		require_once 'modelocuenta.php';
		$query = ("SELECT solicitudes.*, pobladores.apellido, pobladores.nombres, estadosolicitud.descripcion as estado, gradotenencia.descripcion as grado, localidades.descripcion as localidad FROM solicitudes, pobladores, estadosolicitud, gradotenencia, localidades WHERE tiposolicitud=".RURAL."  &&  solicitudes.idpoblador=pobladores.id && solicitudes.idestadosolicitud=estadosolicitud.id && solicitudes.idgradotenencia=gradotenencia.id && solicitudes.idlocalidad=localidades.id");
		$result_all = mysql_query($query);
		if($result_all)
		{
			while ($varsol = mysql_fetch_object($result_all))
			{
				//busco el numero de cuenta correspondiente a la solicitud
				$cuenta = new ModeloCuenta();
				$cuenta->putIdSolicitud($varsol->id);
				$c_ok = $cuenta->traerCuentaSolicitud();
				//si tiene cuenta le asigno el nro, sino "no tiene"
				$nrocuenta = $c_ok?$cuenta->getNroCuenta():"No tiene";
	 			//llenar el array 
				$arrSolicitudes[]=array("id"=>$varsol->id,
                        	"nroexpediente"=>$varsol->nroexpediente,
							"anioexpediente"=>$varsol->anioexpediente,
							"letraexpediente"=>$varsol->letraexpediente,
							"idpoblador"=>$varsol->idpoblador,
							"apellido"=>$varsol->apellido,
							"nombres"=>$varsol->nombres,
							"fechasolicitud"=>fechaACadena($varsol->fechasolicitud),
							"idestadosolicitud"=>$varsol->idestadosolicitud,
							"estado"=>$varsol->estado, 
							"grado"=>$varsol->grado,
							"superficie"=>$varsol->superficie,
							"nrocuenta"=>$nrocuenta,
							"localidad"=>$varsol->localidad,
						);
			} 
		}
		return($arrSolicitudes);	
	}
// ------------------------------------------------------------------------------------

	public function listadoTotalUrbanas() 
	//retorna la consulta de todas los solicitudes
	{
		require_once 'modelocuenta.php';
		$query = ("SELECT solicitudes.*, pobladores.apellido, pobladores.nombres, estadosolicitud.descripcion as estado, gradotenencia.descripcion as grado, localidades.descripcion as localidad FROM solicitudes, pobladores, estadosolicitud, gradotenencia, localidades WHERE solicitudes.tiposolicitud=".URBANA." && solicitudes.idpoblador=pobladores.id && solicitudes.idestadosolicitud=estadosolicitud.id && solicitudes.idgradotenencia=gradotenencia.id && solicitudes.idlocalidad=localidades.id");
		$result_all = mysql_query($query);
		if($result_all)
		{
			while ($varsol = mysql_fetch_object($result_all))
			{
				//busco el numero de cuenta correspondiente a la solicitud
				$cuenta = new ModeloCuenta();
				$cuenta->putIdSolicitud($varsol->id);
				$c_ok = $cuenta->traerCuentaSolicitud();
				//si tiene cuenta le asigno el nro, sino "no tiene"
				$nrocuenta = $c_ok?$cuenta->getNroCuenta():"No tiene";
	 			//llenar el array 
				$arrSolicitudes[]=array("id"=>$varsol->id,
                        	"nroexpediente"=>$varsol->nroexpediente,
							"anioexpediente"=>$varsol->anioexpediente,
							"letraexpediente"=>$varsol->letraexpediente,
							"idpoblador"=>$varsol->idpoblador,
							"apellido"=>$varsol->apellido,
							"nombres"=>$varsol->nombres,
							"fechasolicitud"=>fechaACadena($varsol->fechasolicitud),
							"idestadosolicitud"=>$varsol->idestadosolicitud,
							"estado"=>$varsol->estado, 
							"grado"=>$varsol->grado,
							"superficie"=>$varsol->superficie,
							"nrocuenta"=>$nrocuenta,
							"localidad"=>$varsol->localidad,
						);
			}
		}
		return($arrSolicitudes);	
	}

// ------------------------------------------------------------------------------------	

	public function traersolicitud()
	//retorna los datos de un solicitud particular a partir de un expediente
	
	{
	   
	   
		$query = ("SELECT solicitudes.*, estadosolicitud.descripcion as estado,pobladores.nombres,pobladores.apellido FROM solicitudes, estadosolicitud,pobladores  WHERE solicitudes.id='$this->intIdSolicitud' && solicitudes.idestadosolicitud=estadosolicitud.id  && pobladores.id=solicitudes.idpoblador");
	     
        $result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
	  
       if($result_all && $num_rows>0 ){
      
	    $this->cargarresultados($result_all);
       return(true);	            
      } 
	  	
	  else{
	  return(false);	
	  }
		
       
		
	}
	

// ------------------------------------------------------------------------------------	
	public function traersolicitudporid()
	//retorna los datos de un solicitud particular a partir de un id
	{
	   
	   
		$query = ("SELECT solicitudes.* FROM solicitudes  WHERE solicitudes.id='$this->intIdSolicitud'" );
	
        $result_all=mysql_query($query);
		$num_rows = mysql_affected_rows();
	  
       if($result_all  ){
      
	    $this->cargarresultados($result_all);
       return(true);	            
      } 
	  	
	  else{
	  return(false);	
	  }
		
       
		
	}
	
// ------------------------------------------------------------------------------------	
     public function borrarsolicitud()
     {	
     
	  
      $query=("DELETE FROM solicitudes WHERE id = '$this->intIdSolicitud'");
      $result_all=mysql_query($query);
	  $num_rows = mysql_affected_rows();
	  
       if($result_all && $num_rows>0 ){
	      
          return(true);
	   }
	   else{
	      return(false);
	   }	  

	  }
	   

     	
// ------------------------------------------------------------------------------------
     public function modificarsolicitud()
     {
	  
	 
    
       $query = ("UPDATE solicitudes SET idpoblador='$this->intIdPoblador',fechasolicitud='$this->fecFechaSolicitud',idusrmod='$this->intIdUsrMod',fechaultmod=CURRENT_DATE,idlocalidad=$this->intIdLocalidad, observacion='$this->txtObservacion', idestadosolicitud='$this->intIdEstadoSolicitud',idgradotenencia='$this->intIdGradoTenencia', anioexpediente='$this->intAnioExpediente',nroexpediente='$this->intNroExpediente', letraexpediente='$this->txtLetraExpediente',residente='$this->booResidente',fecharesidedesde='$this->fecResideDesde',  empleadopublico='$this->booEmpleadoPublico', reparticion='$this->txtReparticion', idempleado=$this->intIdEmpleado,superficie='$this->intSuperficie',idunidad=$this->intIdUnidad     WHERE id = '$this->intIdSolicitud' ");
	  
      $result_all=mysql_query($query);
	  $num_rows = mysql_affected_rows();
          if($result_all){
	               return(true);
           }
	       else{
	             return(false);
	       }
      
}



// ------------------------------------------------------------------------------------
     public function altasolicitud()
     {

     
     $query = ("INSERT INTO solicitudes (idpoblador,fechasolicitud,idusrcreador,idusrmod,fechaultmod,idlocalidad, observacion, idestadosolicitud,idgradotenencia, anioexpediente,nroexpediente, letraexpediente,residente,fecharesidedesde,  empleadopublico, reparticion, idempleado,tiposolicitud,superficie,idunidad ) VALUES ('$this->intIdPoblador','$this->fecFechaSolicitud','$this->intIdUsrCreador','$this->intIdUsrMod',CURRENT_DATE,$this->intIdLocalidad, '$this->txtObservacion','$this->intIdEstadoSolicitud','$this->intIdGradoTenencia', '$this->intAnioExpediente','$this->intNroExpediente', '$this->txtLetraExpediente','$this->booResidente','$this->fecResideDesde', '$this->booEmpleadoPublico', '$this->txtReparticion', $this->intIdEmpleado,'$this->intTipoSolicitud','$this->intSuperficie',$this->intIdUnidad )");
	 
    $result_all=mysql_query($query);
    define('IDSOLICITUD',mysql_insert_id());
  	$this->putIdSolicitud(IDSOLICITUD);
     if($result_all){
	
       return(true);
      }
	  else{
	  
	   return(false);
	  }

  }
  

//------------------------------------------------------------------------------------------------ 
  public function cargarresultados($resultado)
	//coloca los datos del query en las variables de la clase
	{
	  
	    while ($cons = mysql_fetch_object($resultado)) {
	 		    
        $this->putIdSolicitud($cons->id);
        $this->putIdPoblador($cons->idpoblador);
        $this->putIdUsrCreador($cons->idusrcreador);
    	$this->putIdUsrMod($cons->idusrmod);
        $this->putUltMod($cons->fechaultmod);
        $this->putFechaSolicitud($cons->fechasolicitud);
        $this->putIdLocalidad($cons->idlocalidad);
		$this->putIdEstadoSolicitud($cons->idestadosolicitud);
		$this->putIdGradoTenencia($cons->idgradotenencia);
		$this->putAnioExpediente($cons->anioexpediente);
		$this->putNroExpediente($cons->nroexpediente);
		$this->putLetraExpediente($cons->letraexpediente);
		$this->putResidente($cons->residente);
		$this->putFechaResideDesde($cons->fecharesidedesde);
		$this->putEmpleadoPublico($cons->empleadopublico);
		$this->putReparticion($cons->reparticion);
		$this->putIdEmpleado($cons->idempleado);
        $this->putNombres($cons->nombres);
		$this->putApellido($cons->apellido);
		$this->putObservacion($cons->observacion);
		$this->putTipoSolicitud($cons->tiposolicitud);
		$this->putSuperficie($cons->superficie);
		$this->putIdUnidad($cons->idunidad);
		}
	}
	
// ------------------------------------------------------------------------------------

	public function listadoRuralesExpedienteConInspeccion() 
	//retorna la consulta de todas los solicitudes
	{
		$condicion = "";
		if($this->getIdSolicitud() > 0)
			$condicion.="solicitudesrurales.id='$this->intIdSolicitud' && ";

		if ($this->getNroExpediente() > 0)
			$condicion.="solicitudes.nroexpediente LIKE '$this->intNroExpediente%' && ";
		      
		if (!$this->getApellido() == "")
			$condicion.=" pobladores.apellido LIKE '$this->txtApellido%' && ";

		$query = ("SELECT solicitudes.id, solicitudes.fechasolicitud, solicitudes.nroexpediente, solicitudes.anioexpediente, solicitudesrurales.id as idrural, pobladores.apellido, pobladores.nombres FROM solicitudes, solicitudesrurales, pobladores WHERE solicitudes.idpoblador=pobladores.id && solicitudes.id=solicitudesrurales.idsolicitud && ".$condicion."    tiposolicitud=".RURAL." && solicitudesrurales.id IN (SELECT idsolicitudrural FROM inspeccion WHERE idsolicitudrural=solicitudesrurales.id)");
		$result_all = mysql_query($query);
		if($result_all)
		{
			while ($varsol = mysql_fetch_object($result_all))
			{
 				//llenar el array 
				$arrSolicitudes[]=array("id"=>$varsol->id,
							"idrural"=>$varsol->idrural,
                       		"nroexpediente"=>$varsol->nroexpediente,
							"anioexpediente"=>$varsol->anioexpediente,
							"apellido"=>$varsol->apellido,
							"nombres"=>$varsol->nombres,
							"fechasolicitud"=>fechaACadena($varsol->fechasolicitud));
			}
		}
		return($arrSolicitudes);	
	}

// ------------------------------------------------------------------------------------

	public function listadoRuralesExpedienteSinInspeccion() 
	//retorna la consulta de todas los solicitudes
	{
		 $condicion = "";
		if ($this->getNroExpediente() > 0)
		      $condicion.="solicitudes.nroexpediente LIKE '$this->intNroExpediente%' && ";
		      
		if (!$this->getApellido() == "")
		      $condicion.=" pobladores.apellido LIKE '$this->txtApellido%' && ";

		$query = ("SELECT solicitudes.id, solicitudes.fechasolicitud, solicitudes.nroexpediente, solicitudes.anioexpediente, solicitudesrurales.id as idrural, pobladores.apellido, pobladores.nombres FROM solicitudes, solicitudesrurales, pobladores WHERE solicitudes.idpoblador=pobladores.id && solicitudes.id=solicitudesrurales.idsolicitud && ".$condicion."    tiposolicitud=".RURAL." && solicitudesrurales.id NOT IN (SELECT idsolicitudrural FROM inspeccion WHERE idsolicitudrural=solicitudesrurales.id)");
		$result_all = mysql_query($query);
		if($result_all)
		{
			while ($varsol = mysql_fetch_object($result_all))
			{
 				//llenar el array 
				$arrSolicitudes[]=array("id"=>$varsol->id,
							"idrural"=>$varsol->idrural,
                       		"nroexpediente"=>$varsol->nroexpediente,
							"anioexpediente"=>$varsol->anioexpediente,
							"apellido"=>$varsol->apellido,
							"nombres"=>$varsol->nombres,
							"fechasolicitud"=>fechaACadena($varsol->fechasolicitud));
			}
		}
		return($arrSolicitudes);	
	}

// ------------------------------------------------------------------------------------

	public function listadoExpedienteNombre() 
	//retorna la consulta de todas los solicitudes segun un  nro de expediente o un solicitante
	{
		$condicion = "";
		if ($this->getNroExpediente() > 0)
			$condicion.="solicitudes.nroexpediente LIKE '$this->intNroExpediente%' && ";
		      
		if (!$this->getApellido() == "")
			$condicion.=" pobladores.apellido LIKE '$this->txtApellido%' && ";
		
		if ($this->getAnioExpediente() > 0)
			$condicion.="solicitudes.anioexpediente LIKE '$this->intAnioExpediente%' && ";
		
		$query = ("SELECT solicitudes.*, pobladores.apellido, pobladores.nombres, estadosolicitud.descripcion as estado, gradotenencia.descripcion as grado FROM solicitudes, pobladores, estadosolicitud, gradotenencia WHERE solicitudes.idpoblador=pobladores.id && solicitudes.idestadosolicitud=estadosolicitud.id && ".$condicion." solicitudes.idgradotenencia=gradotenencia.id");
		$result_all=mysql_query($query);
		if($result_all)
		{
			while ($varsol = mysql_fetch_object($result_all))
			{
			   if($varsol->tiposolicitud==RURAL)
			       $tiposolicitud="RURAL";
			   else
			       $tiposolicitud="URBANA";
				   	   
	 			//llenar el array 
				$arrSolicitudes[]=array("id"=>$varsol->id,
                        	"nroexpediente"=>$varsol->nroexpediente,
							"anioexpediente"=>$varsol->anioexpediente,
							"letraexpediente"=>$varsol->letraexpediente,
							"idpoblador"=>$varsol->idpoblador,
							"apellido"=>$varsol->apellido,
							"nombres"=>$varsol->nombres,
							"fechasolicitud"=>fechaACadena($varsol->fechasolicitud),
							"idestadosolicitud"=>$varsol->idestadosolicitud,
							"estado"=>$varsol->estado, 
							"tiposolicitud"=>$tiposolicitud, 
							"grado"=>$varsol->grado);
			} 
		}
		return($arrSolicitudes);	
	}

// ------------------------------------------------------------------------------------

	public function listadoExpedienteNombreSinCuenta() 
	//retorna la consulta de todas los solicitudes segun un  nro de expediente o un solicitante
	{
        require_once 'modelos/modelosolicitudrural.php';
        require_once 'modelos/modeloinspeccion.php';
        require_once 'modelos/modelosolicitudurbana.php';
        $s_rural = new ModeloSolicitudRural();
        $inspeccion = new ModeloInspeccion();
        $s_urbana = new ModeloSolicitudUrbana();
		$condicion = "";
		if ($this->getNroExpediente() > 0)
			$condicion.="solicitudes.nroexpediente LIKE '$this->intNroExpediente%' && ";
		      
		if (!$this->getApellido() == "")
			$condicion.=" pobladores.apellido LIKE '$this->txtApellido%' && ";
		
		if ($this->getAnioExpediente() > 0)
			$condicion.="solicitudes.anioexpediente LIKE '$this->intAnioExpediente%' && ";
		
        $query =("SELECT solicitudes.id,anioexpediente,nroexpediente,letraexpediente,fechasolicitud,tiposolicitud,superficie,pobladores.apellido,pobladores.nombres, localidades.descripcion as localidad
				FROM solicitudes,pobladores,localidades
				WHERE solicitudes.idpoblador=pobladores.id && solicitudes.idlocalidad=localidades.id && ".$condicion." solicitudes.id NOT IN (SELECT idsolicitud FROM cuentas WHERE idsolicitud=solicitudes.id)");
		$result_all = mysql_query($query);
		if($result_all)
		{
			while ($varsol = mysql_fetch_object($result_all))
			{
                $valor = 0;
                $destino = true;
                $s_urbana->putIdDestinoTierra(0);
                if($varsol->tiposolicitud==RURAL)
                {
                    $tiposolicitud="RURAL";
					$unidad = "ha";
                    //si es RURAL traigo tambien el valor de la hectarea
                    $s_rural->putIdSolicitud($varsol->id);
                    $s_rural->traersolicitudruralasociada();
                    $inspeccion->putIdSolicitud($s_rural->getIdSolicitudRural());
                    $i_ok = $inspeccion->traerInspeccionAsociada();
                    if($i_ok)
                        $valor = $inspeccion->getValorHectarea();
                }else{
                    $tiposolicitud="URBANA";
					$unidad = "m2";
                    //si no tiene destino de la tierra asignado me salteo el registro
                    $s_urbana->putIdSolicitud($varsol->id);
                    $s_urbana->traersolicitudurbanaasociada();
                    if($s_urbana->getIdDestinoTierra() == 0)
                        $destino = false;   //no tiene destino tierra cargado
				}
                if(($valor > 0) || ($destino && $tiposolicitud=="URBANA"))
                {
                    //llenar el array
                    $arrSolicitudes[]=array("id"=>$varsol->id,
                                "nroexpediente"=>$varsol->nroexpediente,
                                "anioexpediente"=>$varsol->anioexpediente,
                                "letraexpediente"=>$varsol->letraexpediente,
                                "apellido"=>$varsol->apellido,
                                "nombres"=>$varsol->nombres,
                                "fechasolicitud"=>fechaACadena($varsol->fechasolicitud),
                                "tiposolicitud"=>$tiposolicitud,
								"superficie"=>$varsol->superficie,
								"unidad"=>$unidad,
								"localidad"=>$varsol->localidad,
                                );
                }
			}
		}
		return($arrSolicitudes);	
	}

// ------------------------------------------------------------------------------------

	public function existeNroExpediente()
	//verifica que el nro de expdte ingresado no existe para otra solicitud
	{
		$query = ("SELECT id,nroexpediente FROM solicitudes WHERE nroexpediente='$this->intNroExpediente' && id <> '$this->intIdSolicitud'");
		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && ($num_rows > 0));
	}

// ------------------------------------------------------------------------------------

    public function tieneCuentaAsociada()
    //determina si la solicitud ya tiene una cuenta asociada
    {
        $query = ("SELECT id FROM solicitudes WHERE id='$this->intIdSolicitud' IN (SELECT solicitudes.id FROM solicitudes,cuentas WHERE cuentas.idsolicitud=solicitudes.id)");
   		$result_all = mysql_query($query);
		$num_rows = mysql_affected_rows();
		return ($result_all && ($num_rows > 0));
    }


}
?>