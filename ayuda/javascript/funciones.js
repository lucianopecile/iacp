/*-----------------------FUNCIONES DE DISE�O---------------------------------------*/
var celda_ant="";
/* Sistema de amortizacion*/
var DIRECTAFRANCES=3;
var FRANCES=1;
var ALEMAN=2;
/* Tipo de gracia */
var NOPLENA=1;
var PLENA=2;
var NOTIENE=3;
/* Periodos de Tiempo*/
var NOTIENEPERIODO=1;

function marcacelda(celda)
{

	
if (celda_ant==""){
	celda_ant=celda;
}
else{
	//alert(celda_ant.style.backgroundColor);
}

celda_ant.style.backgroundColor="#FFFFFF";
celda.style.backgroundColor="#CFCFCF";
celda_ant=celda;
 

}


function colorear(celda)
{
	
   if(celda.style.backgroundColor=="rgb(207, 207, 207)" || (celda.style.backgroundColor=="#cfcfcf")){
     celda.style.backgroundColor="#CFCFCF";
   }
   else{	

      celda.style.backgroundColor="#C7E0C7";
   }
 
 
}



function soloblanco(celda)
{
  if(celda.style.backgroundColor=="rgb(207, 207, 207)" || (celda.style.backgroundColor=="#cfcfcf")){
     celda.style.backgroundColor="#CFCFCF";
	
  }
  else{	
   
	 celda.style.backgroundColor="#FFFFFF";
  }
  
}	

//=============================================================================================================
function selecionarItems(combo,value)
{   
    var i=0;
	for (i = 0; i <combo.length; i++) {
		combo.options[i].selected=false;
    }
}

function selecionarItem(combo,clave)
{   var j=0;
	for (j = 0; j <combo.length; j++) {
        if (combo.options[j].value==clave) combo.options[j].selected=true;
    }
}


 function makeDateFormat(nDay, nMonth, nYear){ 
    var sRes; 
    sRes =  padNmb(nYear, 4, "0") + "/" + padNmb(nMonth, 2, "0") + "/" +padNmb(nDay, 2, "0"); 
    return sRes; 
   } 
  
  
 function diadehoy(){
	 
	 
	 var d, s = "";
  d = new Date();
 var nMonth= (d.getMonth() + 1) ;
  var nDay= d.getDate() ;
  if(d.getYear()<1000){
    var    nYear= d.getYear()+1900;
  }
  else{
       nYear= d.getYear();
  }
     var sRes; 
    sRes =  padNmb(nDay, 2, "0")+ "/" +padNmb(nMonth, 2, "0") + "/" +padNmb(nYear, 4, "0")  ; 
    return sRes; 


	 
	 } 
  
function fechasiguales(fechadesde,fechahasta)

{
	
   var nDia1 = Number(fechadesde.substr(0, 2)); 
   var nMes1 = Number(fechadesde.substr(3, 2)); 
   var nAnio1 = Number(fechadesde.substr(6, 4));
   
   var nDia2 = Number(fechahasta.substr(0, 2)); 
   var nMes2 = Number(fechahasta.substr(3, 2)); 
   var nAnio2 = Number(fechahasta.substr(6, 4));
   
   if((nAnio1==nAnio2) && (nMes1==nMes2)&&(nDia1==nDia2)){
      
       return true;
   }else{
	   return false;
		}
 	
} 
 
function compararfechas(fechadesde,fechahasta)

{
	
   var nDia1 = Number(fechadesde.substr(0, 2)); 
   var nMes1 = Number(fechadesde.substr(3, 2)); 
   var nAnio1 = Number(fechadesde.substr(6, 4));
   
   var nDia2 = Number(fechahasta.substr(0, 2)); 
   var nMes2 = Number(fechahasta.substr(3, 2)); 
   var nAnio2 = Number(fechahasta.substr(6, 4));
   
   if(nAnio1>nAnio2)
      return false;
   if(nAnio1<nAnio2)
      return true;
   if(nAnio1==nAnio2){
        if(nMes1>nMes2)
          return false;	 
		if(nMes1<nMes2)
          return true;	  
		if(nMes1==nMes2){
          if(nDia1>=nDia2)
            return false;	
	      if(nDia1<nDia2)
            return true;	
	
		}
   }	
	
	
return false;	
	
}
  
function esFecha(miFecha) {
 
	if (miFecha=='00/00/0000') return true;
    var Fecha= new String(reemplazar(miFecha, "-","/"));
    Fecha=reemplazar(Fecha, " ","");
    var RealFecha= new Date();
    var Ano= new String(Fecha.substring(Fecha.lastIndexOf("/")+1,Fecha.length));   
    var Mes= new String(Fecha.substring(Fecha.indexOf("/")+1,Fecha.lastIndexOf("/")));   
    var Dia= new String(Fecha.substring(0,Fecha.indexOf("/")));   
  

    var AnoActual= RealFecha.getYear();   
     
	
	 
    if (isNaN(Ano) || Ano.length<4 || Ano.length>4 ||parseFloat(Ano)<1900 || parseFloat(Ano)>2100   ) { 
	
	        return false   
    }   
    
    if (isNaN(Mes) || parseFloat(Mes)<1 || Mes.length<2 || Mes.length>2 || parseFloat(Mes)>12){

        return false   
    }   

    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){   
        return false   
    }   

    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {   
        if ((Mes==2 && Dia == 29 && !bisiesto(Ano))||(Mes==2 && Dia == 30) || Dia>30) { 
	         return false    	
        }   
    }   
  return true
}   
function esNumero( numstr ) {
	if ( numstr+"" == "undefined" || numstr+"" == "null" || numstr+"" == "" ) 
		return false;

	var isValid = true;
	var decCount = 0; 

	numstr += ""; 

	for (i = 0; i < numstr.length; i++) {
		if (numstr.charAt(i) == ".")
			decCount++;
			if (!((numstr.charAt(i) >= "0") && (numstr.charAt(i) <= "9") || 
			   (numstr.charAt(i) == "$") || (numstr.charAt(i) == " ") || (numstr.charAt(i) == "."))) {
					isValid = false;
					break;
			} else if ((numstr.charAt(i) == "$" && i != 0) ||
				(numstr.charAt(i) == "." && numstr.length == 1) ||
				(numstr.charAt(i) == "." && decCount > 1)) {
				isValid = false;
			break;
		} 
	} 
	return isValid;
} 

function reemplazar (miCadena,este,poreste) {

	var viejaCadena= new String(miCadena)
	var nuevaCadena= new String()
	
	for (i = 0; i < viejaCadena.length; i++) {
		if (viejaCadena.charAt(i) == este) {
			nuevaCadena += poreste
		} else {
			nuevaCadena += viejaCadena.charAt(i)
		}
	}
	return nuevaCadena
}

function gotoLink(url){
location.href = url;
}


function ImprirProyecto(proyecto){
	alert("Falta implementar el Imprimir");
	return false;
 } 
 function esDigitoN(digito){
	return  !(digito < 45 || digito > 57);
/*	return !(digito>=48 && digito<=57)*/
}
function ignoreSpaces(string) {
var temp = "";
string = '' + string;
splitstring = string.split(" ");
for(i = 0; i < splitstring.length; i++)
temp += splitstring[i];
return temp;
}

function esvacio(valor)
{
	
	var q=valor;
	
	if ((q.length < 1)) {
		
	    return (true); 
    } 
   else{
	 for ( i = 0; i < q.length; i++ ) {   
                if ( q.charAt(i) != " " ) {   
                        return (false);   
                  }   
      }
	   return (true); 
   }
	
}



function cantdias(mes,anio)
{
	
dias=[0,31,29,31,30,31,30,31,31,30,31,30,31]	

ultimo=0;
mes=mes*1;
if (mes==2){
	fecha=new Date(anio,1,29)
	vermes=fecha.getMonth();
	if((vermes+1)!=mes){ultimo=28}
}
if(ultimo==0){ultimo=dias[mes]}
return(ultimo);
}

function bisiesto(anio){
	
	if((anio%4==0)&&((anio%100!=0) || (anio%400==0)))
        {return(true);}
	else
	    {return(false);}

	}


function sumardias(fecha1,diassumar){
	
	milisegundos=parseInt(diassumar*24*60*60*1000);
	fecha=new Date(fecha1);
	
	dia=fecha.getDate();
	mes=fecha.getMonth();
	anio=fecha.getYear();
	tiempo=fecha.getTime();
	total=fecha.setTime(parseInt(tiempo+milisegundos));
	dia=fecha.getDate();
	mes=fecha.getMonth();
	anio=fecha.getYear();
	return(fecha);
	}
	
	
function solonumero(e){
	
    tecla_codigo = (document.all) ? e.keyCode : e.which;
    if(tecla_codigo==8)return true; //si se activa acepta que borre en firefox
    if(e.keyCode==9)return true; //si se activa acepta tab

    patron =/[0-9]/;
    tecla_valor = String.fromCharCode(tecla_codigo);
    return patron.test(tecla_valor);        
}
function solonumerocoma(e){
	
tecla_codigo = (document.all) ? e.keyCode : e.which;
if(tecla_codigo==8)return true; //si se activa acepta que borre en firefox
if(e.keyCode==9)return true; //si se activa acepta tab

patron =/[0-9.]/;
tecla_valor = String.fromCharCode(tecla_codigo);
return patron.test(tecla_valor);

}
function solotel(e){
	
tecla_codigo = (document.all) ? e.keyCode : e.which;
if(tecla_codigo==8)return true; //si se activa acepta que borre en firefox

if(e.keyCode==9)return true; //si se activa acepta tab

patron =/[0-9()-]/;
tecla_valor = String.fromCharCode(tecla_codigo);
return patron.test(tecla_valor);

}
function solonumerobarra(e){
	
tecla_codigo = (document.all) ? e.keyCode : e.which;
if(tecla_codigo==8)return true; //si se activa acepta que borre en firefox
if(e.keyCode==9)return true; //si se activa acepta tab

patron =/[0-9/]/;
tecla_valor = String.fromCharCode(tecla_codigo);
return patron.test(tecla_valor);

}


function sinespacios(e){
	
tecla_codigo = (document.all) ? e.keyCode : e.which;
if(tecla_codigo==8)return true; //si se activa acepta que borre en firefox

if(e.keyCode==9)return true;//si se activa acepta tab

	 patron =/[0-9a-z]/;  
	 tecla_valor = String.fromCharCode(tecla_codigo);
     return patron.test(tecla_valor);


}


function nombremes(parfecha){
	
   var nDia1 = Number(parfecha.substr(0, 2)); 
   var nMes1 = Number(parfecha.substr(3, 2)); 
   var nAnio1 = Number(parfecha.substr(6, 4));

	switch (nMes1){
	case 1 :
	       return("enero");
		   break;
	
	case 2 :
	       return("febrero");
		   break;
	case 3 :
	       return("marzo");
		   break;
	
	case 4 :
	       return("abril");
		   break;
    case 5 :
	       return("mayo");
		   break;		   
    case 6 :
	       return("junio");
		   break;			   

    case 7 :
	       return("julio");
		   break;	
	case 8 :
	       return("agosto");
		   break;	
	case 9 :
	       return("septiembre");
		   break;	
	case 10 :
	       return("octubre");
		   break;	
	case 11 :
	       return("noviembre");
		   break;	
	default :
	       return("diciembre");
	}	
	 return (""); 
}

function sumarmes(sFec0){
	
   var nDia = parseInt(sFec0.substr(0, 2), 10); 
   var nMes = parseInt(sFec0.substr(3, 2), 10); 
   var nAno = parseInt(sFec0.substr(6, 4), 10); 
   nMes += 1; 
   if (nMes == 13){ 
     nMes = 1; 
     nAno += 1; 
    } 
   return makeFrenchFormat(nDia, nMes, nAno); 
} 
  