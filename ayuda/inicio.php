<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script language="JavaScript" type="text/javascript" src="./javascript/jquery-1.2.2.pack.js"></script>
<script language="JavaScript" type="text/javascript" src="./javascript/ddaccordion.js"></script>
<script language="JavaScript" type="text/javascript" src="./javascript/menudyn.js"></script>


<link rel="stylesheet" type="text/css" href="./css/default.css" media="all"/>


<?php
	require 'librerias/config.php'; //de configuracion
	require 'librerias/view.php'; //Mini motor de plantillas
	require 'config.php'; //Archivo con configuraciones.	 
?>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Ayuda a Sistema IAC</title>
</head>

<body>

<div id="masthead"></div>

<div id="content">

	<div id="sidebar">
		  
	<div id="temas"><h2>Temas de ayuda</h2></div>
	
<div class="glossymenu">

<a class="menuitem submenuheader" href="?controlador=&&accion=ver_pagina&&pagina=&&sector=">Principal<br/></a>
<div class="submenu">
	<ul type=none >
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaprincipal.php&&sector=#ingreso">C&oacute;mo ingresar al sistema</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaprincipal.php&&sector=#salir">C&oacute;mo salir del sistema</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaprincipal.php&&sector=#proceso">C&oacute;mo dar de alta una nueva cuenta</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaprincipal.php&&sector=#reliquidar">C&oacute;mo reliquidar/modificar una cuenta</a></li>
	</ul>
</div>

<a class="menuitem submenuheader" href="?controlador=&&accion=ver_pagina&&pagina=&&sector=">Pobladores<br/></a>
<div class="submenu">
	<ul type=none >
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudapobladores.php&&sector=#alta">C&oacute;mo cargar un nuevo poblador</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudapobladores.php&&sector=#mod">C&oacute;mo modificar un poblador</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudapobladores.php&&sector=#cony">C&oacute;mo cargar el c&oacute;nyuge de un poblador</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudapobladores.php&&sector=#hijos">C&oacute;mo cargar los hijos de un poblador</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudapobladores.php&&sector=#baja">C&oacute;mo eliminar un poblador o un c&oacute;nyuge</a></li>
	</ul>
</div>

<a class="menuitem submenuheader" href="?controlador=&&accion=ver_pagina&&pagina=&&sector=">Solicitudes rurales<br/></a>
<div class="submenu">
	<ul type=none >
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudarurales.php&&sector=#alta">C&oacute;mo cargar una nueva solicitud rural</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudarurales.php&&sector=#tierra">Datos de la tierra rural</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudarurales.php&&sector=#mod">C&oacute;mo modificar una solicitud rural</a></li>
	</ul>
</div>

<a class="menuitem submenuheader" href="?controlador=&&accion=ver_pagina&&pagina=&&sector=">Solicitudes urbanas<br/></a>
<div class="submenu">
	<ul type=none >
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaurbanas.php&&sector=#alta">C&oacute;mo cargar una nueva solicitud urbana</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaurbanas.php&&sector=#tierra">Datos de la tierra urbana</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaurbanas.php&&sector=#mod">C&oacute;mo modificar una solicitud urbana</a></li>
	</ul>
</div>

<a class="menuitem submenuheader" href="?controlador=&&accion=ver_pagina&&pagina=&&sector=">Informes de inspecci&oacute;n rural<br/></a>
<div class="submenu">
	<ul type=none >
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudainspeccion.php&&sector=#alta">C&oacute;mo cargar un informe de inspecci&oacute;n</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudainspeccion.php&&sector=#mod">C&oacute;mo modificar un informe</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudainspeccion.php&&sector=#precio">C&oacute;mo definir el precio por hect&aacute;rea</a></li>
	</ul>
</div>

<a class="menuitem submenuheader" href="?controlador=&&accion=ver_pagina&&pagina=&&sector=">Cuentas<br/></a>
<div class="submenu">
	<ul type=none >
	<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacuentas.php&&sector=#alta">C&oacute;mo cargar una nueva cuenta</a></li>
	<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacuentas.php&&sector=#reliquidar">C&oacute;mo reliquidar/modificar una cuenta</a></li>
	<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacuentas.php&&sector=#precio">C&oacute;mo definir el valor total de la tierra</a></li>
	</ul>
</div>

<a class="menuitem submenuheader" href="?controlador=&&accion=ver_pagina&&pagina=&&sector=">Cuotas<br/></a>
<div class="submenu">
	<ul type=none >
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacuotas.php&&sector=#editar">C&oacute;mo editar una cuota</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacuotas.php&&sector=#impr">Imprimir una boleta de pago a t&eacute;rmino</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacuotas.php&&sector=#pago">Reimprimir una boleta de pago vencida</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacuotas.php&&sector=#pagoparcial">Imprimir una boleta de pago parcial</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacuotas.php&&sector=#gastos">Modificar los gastos fijos de una cuenta</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacuotas.php&&sector=#diferir">Diferir los vencimientos de las cuotas</a></li>
	</ul>
</div>

<a class="menuitem submenuheader" href="?controlador=&&accion=ver_pagina&&pagina=&&sector=">Cobros<br/></a>
<div class="submenu">
	<ul type=none >
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacobros.php&&sector=#importar">C&oacute;mo importar un archivo de cobros</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacobros.php&&sector#cobro">C&oacute;mo cobrar manualmente una cuota</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacobros.php&&sector#descuento">C&oacute;mo descontar el cobro de una cuota</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudacobros.php&&sector#deshacer">C&oacute;mo deshacer &uacute;ltimo movimiento</a></li>

	</ul>
</div>

<a class="menuitem submenuheader" href="?controlador=&&accion=ver_pagina&&pagina=&&sector=">Consultas<br/></a>
<div class="submenu">
	<ul type=none >
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaconsultas.php&&sector=#estado">Estado de cuenta</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaconsultas.php&&sector=#deudores">Listado de deudores a una fecha</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaconsultas.php&&sector=#localidad">Listado de cuentas por localidad</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaconsultas.php&&sector=#saldos">Listado de saldos totales</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaconsultas.php&&sector=#ingresos">Listado de ingresos</a></li>
	</ul>
</div>

<a class="menuitem submenuheader" href="?controlador=&&accion=ver_pagina&&pagina=&&sector=">Par&aacute;metros varios<br/></a>
<div class="submenu">
	<ul type=none >
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaparametros.php&&sector=#interes">C&oacute;mo modificar el inter&eacute;s del BCH</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaparametros.php&&sector=#destino">Destinos de tierras urbanas</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudaparametros.php&&sector=#valores">Valores de tierras rurales</a></li>
	</ul>
</div>

<a class="menuitem submenuheader" href="?controlador=&&accion=ver_pagina&&pagina=&&sector=">Usuarios<br/></a>
<div class="submenu">
	<ul type=none >
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudausuarios.php&&sector=#administrar">C&oacute;mo administrar usuarios</a></li>
		<li><a href="inicio.php?controlador=ayuda&&accion=ver_pagina&&pagina=ayudausuarios.php&&sector=#clave">C&oacute;mo cambiar la contrase&ntilde;a</a></li>
	</ul>
</div>

</div> 

</div>
</div>

    
<div id="main">
	<!-- <div id="headermain"></div> -->
	
	<div id="bodymain" class="ver_pagina"> 
           
    <?php
    	//Incluimos el FrontController, aca se llama a las acciones segun se elige en el menu
	    require 'librerias/frontcontroller.php';

    	//Lo iniciamos con su m�todo est�tico main.
		FrontController::main();
	?>
    
	</div>
    
	<div id="footermain"></div>

</div>

</body>

</html>
