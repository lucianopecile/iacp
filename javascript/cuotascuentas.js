function cancelarEnvio()
{
   document.location = "./index.php?controlador=cuota&&accion=mostrarcuenta";
   return(true);
}

function editarCuota(idcuota)
{
	var saldo = document.getElementById("saldo").value;
	var saldoInt = parseInt(saldo.split(',', 1));
	if( idcuota > 0){
		if(saldoInt < 1){
			alert("No puede editar las cuotas de una cuenta cancelada");
			return false;
		}
		document.location = "./index.php?controlador=cuota&&accion=editarcuota&&idcuota="+idcuota+"&&operacion="+"2";
	}else{
		alert("Debe primero seleccionar la cuota");
		return false;
	}
	return true;
}
