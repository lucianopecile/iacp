function enviarDatos(form)
{
	moverConceptos();
	if (validarDatos(form)){
		form.submit();
	}
}
	
function validarDatos()
{
	return true; 
}

function cancelarEnvio()
{
	nada();
}

function validarCheck(este,valormodif)
{
 
	if (este.checked){
		
	 valormodif.value=1;
	 
    }
	else{
	
	valormodif.value=0;
  }
			
}
function formatomiles(objeto,prefix){
	

var num=objeto.value*1;
prefix = prefix || '';
num += '';
var splitStr = num.split('.');
var splitLeft = splitStr[0];
var splitRight = splitStr.length > 1 ? ',' + splitStr[1] : '';
var regx = /(\d+)(\d{3})/;
while (regx.test(splitLeft)) {
splitLeft = splitLeft.replace(regx, '$1' + '.' + '$2');
}
objeto.value=prefix + '  '+splitLeft + splitRight;

}

function elegiritem(idparaagregar, casilla)
{
	campo = "valor"+idparaagregar;
	valor = document.getElementById(campo).value;
}

function calculoinicial()
{
    comprobarFactores();
   	var precio = document.getElementById('precio').value*1;
    var total = document.getElementById('superficie').value*1;
    document.getElementById('total').value = total*precio;
	var liquidacion = document.getElementById('total').value*1;
/*	var grillae = document.getElementById("grillaelegidos");
	var filae = grillae.getElementsByTagName("tr");
	var cellse;
	for (var i=0; i<filae.length; i++){
		idname = filae[i].id;
		cellse = filae[i].getElementsByTagName("input");
		for (var j=0; j<cellse.length; j++){
			var val='valor'+idname;
			
			if((cellse[j].id == val && idname=='5')){ // si es Mensura=id 5 suma al total de la liquidacion
				valor=cellse[j].value;
				liquidacion=liquidacion+(valor*1);
			}
		}
	}*/
	document.getElementById('total').value = liquidacion.toFixed(2);
	document.getElementById('totalmostrar').value= document.getElementById('total').value;
	formatomiles(document.getElementById('totalmostrar'),'');

}

function moverConceptos()
{
	var idsolicitud = document.getElementById("idsolicitud").value;      
	var grillae = document.getElementById("grillaelegidos");
	var filae = grillae.getElementsByTagName("tr");
	var nuevoarray2 = new Array() ;
	var cellse;
	a = 0;
	for(i=0; i<filae.length; i++){
		idname = filae[i].id;
		var nuevoarraye = new Array() ;
		cellse = filae[i].getElementsByTagName("input");
		var des = 'descripcion'+idname;
		var val = 'valor'+idname;
		for (j=0; j<cellse.length; j++){
			if((cellse[j].id==des))
				descrip=cellse[j].value;
			if((cellse[j].id==val))
				valor=cellse[j].value;
			if((cellse[j].id=='elegir')){
				if(cellse[j].checked){
					nuevoarraye[0]=idsolicitud;
					nuevoarraye[1]=idname;
					nuevoarraye[2]=valor;
					nuevoarray2[a++]=nuevoarraye;
				}
			}
		}
	}
	var grilla = document.getElementById("grilladisponibles");
	var fila = grilla.getElementsByTagName("tr");
	var cells;
	a = nuevoarray2.length;
	for(i=0; i<fila.length; i++){
		idname = fila[i].id;
		var nuevoarray = new Array() ;
		cells = fila[i].getElementsByTagName("input");
		var des = 'descripcion'+idname;
		var val = 'valor'+idname;
		for (j=0; j<cells.length; j++){
			if((cells[j].id == des)){
				descrip = cells[j].value;
			}
			if((cells[j].id == val)){
				valor = cells[j].value;
			}
			if((cells[j].id == 'elegir')){
				if(cells[j].checked){
					nuevoarray[0]=idsolicitud;
					nuevoarray[1]=idname;
					nuevoarray[2]=valor;
					nuevoarray2[a++]=nuevoarray;
				}
			}
		}
	}
	document.getElementById('conceptoselegidos').value = nuevoarray2;
	return(true);
}

function comprobarFactores()
{
	var form = document.getElementById("formulario");
	var precio = document.getElementById("precio").value*1;
	var preciofinal = precio;
	var titulo = "<h3>Factores del medio</h3>";
	var tabla = "<table>";
	if(form.espejosagua.value == 1){
		tabla = tabla+"<tr><td>Cercan&iacute;a a espejos de agua</td><td>%100</td></tr>";
		preciofinal += precio;	//aumento un 100%
	}
	if(form.rioarroyo.value == 1){
		tabla = tabla+"<tr><td>Existencia de rios o arroyos</td><td>%100</td></tr>";
		preciofinal += precio * 1;	//aumento un 100%
	}
	if(form.altascumbres.value == 1){
		tabla = tabla+"<tr><td>Cercan&iacute;a a altas cumbres</td><td>%50</td></tr>";
		preciofinal += precio * 0.5;	//aumento un 50%
	}
	if(form.centrosurbanos.value == 1){
		tabla = tabla+"<tr><td>Cercan&iacute;a a centros urbanos</td><td>%20</td></tr>";
		preciofinal += precio * 0.2;	//aumento un 20%
	}
	if(form.accesos.value == 1){
		tabla = tabla+"<tr><td>Caminos accesibles</td><td>%80</td></tr>";
		preciofinal += precio * 0.8;	//aumento un 80%
	}
	if((form.veranada.value == form.superficie.value) || (form.invernada.value == form.superficie.value)){
		preciofinal *= 0.5;
	}
	tabla = tabla+"</table>";
	document.getElementById("precio").value = preciofinal.toFixed(2);
	document.getElementById("factores").innerHTML = titulo + tabla;
	return true;	
}

function comprobarSuperficie()
{
	var sup = document.getElementById("superficie").value;
	document.getElementById("sup").innerHTML = comprobar(sup);
	var ver = document.getElementById("veranada").value;
	document.getElementById("ver").innerHTML = comprobar(ver);
	var inv = document.getElementById("invernada").value;
	document.getElementById("inv").innerHTML = comprobar(inv);
	return true;
}

function comprobar(valor)
{
	if(valor == 0){
		return 0;
	}else{
		var arr = valor.split('.');
		var nuevo_valor = arr[0];
		nuevo_valor = nuevo_valor+'-'+arr[1].substring(0,2)+'-'+arr[1].substring(2,4)+'-'+arr[1].substring(4);
		return nuevo_valor;
	}
}

function EnviarLiquidacion()
{
	var liquidacion = document.getElementById('total').value*1;
    var valorhectarea = document.getElementById("precio").value*1;
	if(window.opener != null)
	{		
		window.opener.cargarLiquidacion(liquidacion, valorhectarea);
	}
    setTimeout("nada()",300);
}

function nada()
{
	self.close();
}
