function moverClaves(form,tipo)
{
	var comboOrigen;
	var comboDestino;
	switch(tipo){
		case "seleccionar":
			comboOrigen = form.vwMenuDisponible;
            comboDestino = form.vwMenuElegido;
		    break;
	    case "deseleccionar":
            comboOrigen = form.vwMenuElegido;
            comboDestino = form.vwMenuDisponible;
		    break;
	}
    var cantidad = comboOrigen.length;

	for(var i = 0; i < cantidad; i++){
		if(comboOrigen.options[i].selected == true){
			// Agregar a Destino
			var mvalor= comboOrigen.options[i].value;
            if (!existeClave(comboDestino,mvalor)){
			     var mtexto= comboOrigen.options[i].text;				
                 var nuevoItem = new Option(mtexto,mvalor);
                 comboDestino.options[comboDestino.length] =nuevoItem;			
                 comboOrigen.options[i] = null;
			     cantidad = cantidad -1;
			     i = i -1;
			}
		}
	}
}

function existeClave(combo,clave)
{
	for(var j=0; j < combo.length; j++){
		if(combo.options[j].value == clave)
			return true;
    }
	return false;
}

function ponernombre(form)
{
	var cantidad = form.ListaEmpleados.length;
	for(var i=0; i < cantidad; i++){
		if(form.ListaEmpleados.options[i].selected == true){
			form.nombrecompleto.value=form.ListaEmpleados.options[i].text;
		}
	}
}

function enviarDatos(form)
{
	var cantidad = form.vwMenuElegido.length;
	if(validarDatos(form)){
		for(var i=0; i < cantidad; i++){
			form.vwMenuElegido.options[i].selected = true;
		}
		form.submit();
	}
}
	
function validarDatos(form)
{
	if(validarEmpleado(form)){
		if (validarUsuario(form)){
			if (validarNombre(form)){
				if (validarClave(form)){
					if(validarDepartamento(form)){
						return true;
					}
				}
			}
		}
	}
	return false;
}

function validarUsuario(form)
{
   var cantidad = form.ListaUsuarios.length;
      
	  if(!esvacio(form.usuario.value)){
        if(form.accion.value=="altausuario"){
	       for (var i = 1; i < cantidad; i++) {
			  
              if(form.usuario.value==form.ListaUsuarios[i].value){
				  alert("El usuario ya existe");
				  form.usuario.focus();
				  return(false);
				  }
	       }
		}
		}
		else{
			alert("Debe ingresar un nombre de usuario");
				  form.usuario.focus();
				  return(false);
			
			
			}
		
   
	
       return (true);   
                  
  
}

function validarClave(form)
{
   var longitud = form.clave.value.length;
              if(longitud<=0){
				  alert("Debe restaurar la clave");
				  form.clave.focus();
				  return(false);
				 
	          }
			  else{
				   
				 return(true);  
				  }
	
 
}

function validarDepartamento(form)
{
	var dpto = form.iddepartamento.value;
	if(dpto<=0){
		alert("Debe seleccionar el departamento de trabajo del usuario");
		form.ListaDepartamento.focus();
		return false;
	}
	return true;
}

function compararclaves(form)
{
   var longitud = form.clave.value.length;
   var clave1=form.clave.value;
   var clave2=form.clave2.value;
              if(longitud<8){
				  alert("La clave debe ser de 8 caracteres");
				  form.clave.focus();
				  return(false);
				 
	          }
			  else{
				  if(clave1==clave2){
				
				    return(true)
				  }
	              else{
				  	  alert("Las claves no coinciden");
					  form.clave.value="";
					  form.clave2.value="";
				      form.clave.focus();
				      return(false);
					  }
		
			  }
	
                  
  
}


function noclaves(){
	
		  document.getElementById('clave').disabled='disabled';
		  document.getElementById('clave2').disabled='disabled';

	
	}
	
	
	
function validarNombre(form)
{
	var q = form.nombrecompleto.value;
	if ((form.nombrecompleto.value.length < 1)) {
		alert("Debe ingresar el nombre del usuario ");
		form.nombrecompleto.focus();
        return (false); 
    }else{
		for (var i = 0; i < q.length; i++ ) {
                if ( q.charAt(i) != " " ) {   
                        return (true);   
                }   
        }   
  
  // Agregar las validaciones.-
    alert("No se acepta el nombre"); 
	form.nombrecompleto.focus();
	return(false);
	
   }
            
}



function validarEmpleado(form)
{
	
	var empleado=form.idempleado.value;
 	if (empleado<=0){
 	   alert("Debe ingresar el empleado relacionado con el usuario "); 
 		form.ListaEmpleados.focus();
         return (false); 
		
	 	}
 
	return(true);
   
}

function validarpas(form)
{
  if (form.checkcambia.checked){
	 form.clave.value="";
	 form.clave2.value="";
	 form.clave.disabled=false;
	 form.clave2.disabled=false;
	 form.checkcambia.disabled=true ;
	 
    
	
  }
			
	
   return(true);
	
   
}




function noenter(field,evento) { 
 
  var keyCode = evento.keyCode ? evento.keyCode : evento.which ? evento.which : evento.charCode;
 
if (keyCode == 13) {
    return false;
} 
else{
     return true;
} 

}
function cancelarEnvio(form)
{
	
   document.location = "./index.php?controlador=usuario&&accion=mostrarusuario";
   return(true);
}



