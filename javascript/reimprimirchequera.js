function enviarDatos(form)
{
	if (validarDatos(form)){
		form.submit();
	}
}
	
function validarDatos(form)
{
	return (true);
}

function cancelarEnvio()
{
   document.location = "./index.php?controlador=cuota&&accion=reimprimirchequera";
   return(true);
}

function buscarprestamo(form)
{    
     var expanio=form.expanio.value;
     var idorganismoexp=form.idorganismoexp.value;
     var nroexpediente=form.nroexpediente.value;
     
url = "base.php?controlador=prestamo&&accion=elegirprestamocta&&expanio="+expanio+" && idorganismoexp="+idorganismoexp+" && nroexpediente="+nroexpediente;  
	 open(url, "listaprestamo", "width=600, height=350, toolbar=no, top=200, left=200 ");
   
	return true;
}

function imprimirchequera()
{  
	 var idpmo=window.document.formcta.idprestamo.value;
	 var cuotadesde=window.document.formreimprimir.cuotadesde.value;
	 var cuotahasta=window.document.formreimprimir.cuotahasta.value;

	 if(idpmo>0 && cuotadesde>0 && cuotahasta>0){
 	
    url = "./reports/reimprimirchequera.php?idpmo="+idpmo+"&&cuotadesde="+cuotadesde+"&&cuotahasta="+cuotahasta;  
	open(url, "impcheq", "width=650, height=250, toolbar=no, top=200, left=200 ");
	 }
      else{
	   alert("Debe seleccionar el rango de cuotas para reimprimir la chequera");
	   return(false);
	 }

	return(true);
}

function buscarCuenta(form)
{
    var anio = form.anioexpediente.value;
    var nro = form.nroexpediente.value;
    var nrocuenta = form.nrocuenta.value;
    var url = "base.php?controlador=cuenta&&accion=elegircuenta&&anioexpediente="+anio+"&&nroexpediente="+nro+"&&nrocuenta="+nrocuenta;
    open(url, "listacuentas", "width=600,height=350,toolbar=no,top=200,left=200");
    return true;
}

function mostrarDatos(idcuenta)
{
    document.location = "index.php?controlador=cuota&&accion=reimprimirchequera&&idcuenta="+idcuenta+"&&operacion=4";
}

function conceptosCuenta(form)
{
	var idsolicitud = form.idsolicitud.value;
	var url = "base.php?controlador=liquidacion&&accion=conceptoscuenta&&idsolicitud="+idsolicitud;
	open(url, "listaconceptoscuentas", "width=620,height=550,toolbar=no,top=200,left=200");
	return true;
}
