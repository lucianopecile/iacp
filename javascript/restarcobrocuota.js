function enviarDatos(form)
{
    if(validarDatos(form)){
        form.submit();
        return true;
    }
    return false;
}

function validarDatos(form)
{
	if(!validarCobrado(form))
		return false;

	if(form.obs_ok.value == 0){
		alert("Debe detallar el motivo del descuento al cobro de la cuota para su registro");
		form.observacion.focus();
		return false;
	}
    return true;
}

function cancelarEnvio(idcuenta)
{
    document.location = "./index.php?controlador=cobro&&cuentarestarcobro&&idcuenta="+idcuenta;
    return true;
}

function validarCobrado(form)
{
	var monto = form.montoresta.value*1;
	var cobros_anteriores = form.cobradototal_sf.value*1;
	if(monto > cobros_anteriores || monto == 0){
		alert("El descuento no puede superar el total ya cobrado ni ser cero");
		form.montoresta.focus();
		return false;
	}
	return true;
}
