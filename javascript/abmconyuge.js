function enviarDatos(form)
{
	if (validarDatos(form))
	{
		form.submit();
	}
}
	
function validarDatos(form)
{
	if(!validarDoc(form))
		return(false);
	if(!validarPoblador(form))
		return(false);
	if (!validarApellido(form))
		return(false);
	if (!validarNombre(form))
		return(false);
	return(true); 
}

function validarApellido(form)
{
	var i;
	q=form.apellido.value;
	
	if ((form.apellido.value.length < 1)) { 
	
		alert("Debe ingresar un apellido para el poblador"); 
		form.apellido.focus();
        return (false); 
    } 
   else{
	   
	 for (i = 0; i < q.length; i++ ) {   
                if ( q.charAt(i) != " " ) {   
                        return (true);   
                }   
        }   
  
  // Agregar las validaciones.-
    alert("No se acepta el apellido"); 
	form.apellido.focus();
	return(false);
	
   }
}

function validarNombre(form)
{
	var i;
	q=form.nombres.value;
	
	if ((form.apellido.value.length < 1)) { 
	
		alert("Debe ingresar nombre para el poblador"); 
		form.apellido.focus();
        return (false); 
    } 
   else{
	   
	 for ( i = 0; i < q.length; i++ ) {   
                if ( q.charAt(i) != " " ) {   
                        return (true);   
                }   
        }   
  
  // Agregar las validaciones.-
    alert("No se acepta el nombre"); 
	form.nombres.focus();
	return(false);
	
   }
}
/*
function verRta(rta)
{
//Esta funcion trae una respuesta del html encontro exp. si la respuesta es 0 el expediente no existe, 
//si la respuesta es mayor a 0 quiere decir que el expediente YA existe y llama a la solicitud asociada

if(rta>0){	
	
    //document.location = "index.php?controlador=emprendedor&&accion=veremprendedor&&operacion="+2+"&&idemp="+rta;
	alert("Ya existe un poblador con ese tipo y n�mero de documento");
	document.getElementById('documento').value=0;
	
    document.getElementById('guardar').disabled=true; //enable button
	document.getElementById('documento').focus();
	return(false);
	
  }
  else{
    document.getElementById('guardar').disabled=false; //enable button
	document.getElementById('telefono').focus();
	return(true); 
  }
}
*/

function verificarPoblador(form)
{
	var tipodoc=form.idtipodoc.value;
	var documento=form.documento.value;
	document.getElementById("guardar").disabled=true; //disable button

	if(tipodoc>0 && documento>0){
		url = "base.php?controlador=poblador&&accion=verificardocumento&&tipodoc="+tipodoc+" && documento="+documento;  
		open(url, "existedoc", "width=450, height=25, toolbar=no, top=200, left=200 ");
	}
	return true;
}
	
function validarDoc(form)
{ 
     var tipodoc=form.idtipodoc.value;
     var documento=form.documento.value;

	 if(tipodoc > 0 && documento > 0){
		 return(true);
     }
	 else{
		 alert("Debe ingresar el n�mero de documento"); 
		 form.documento.focus();
		 return(false);
	 }
}
	

function validarPoblador(form)
{
	if ((form.idtipodoc.value < 1))
	{ 
		alert("Debe ingresar el tipo de documento");
		form.idtipodoc.focus();
		return(false);
	}
	if ((form.idlocalidad.value < 1))
	{ 
		alert("Debe seleccionar la localidad de residencia");
		form.idlocalidad.focus();
		return(false);
	}
	if ((form.idestadocivil.value < 1))
	{
		alert("Debe seleccionar el estado civil de la persona");
		form.idestadocivil.focus();
		return(false);
	}
	return(true);
}


function validarDocumento(e)
{
	//Valida digito aceptando puntos  y guion
	tecla_codigo = (document.all) ? e.keyCode : e.which;
	//if(tecla_codigo==8)return true; si se activa acepta espacios en blanco
	patron =/[0-9.-]/;
	tecla_valor = String.fromCharCode(tecla_codigo);
	return patron.test(tecla_valor);
}


function cancelarEnvio(form)
{
	document.location = "./index.php?controlador=index&&accion=nada";
	return(true);
}

/*
function cargarConyuge()
{
	document.location = "./index.php?controlador=conyuge&&accion=verconyuge";
	return(true);
}

function elegirConyuge()
{
	document.location = "./index.php?controlador=conyuge&&accion=elegirconyuge";
	return(true);
}

function cargarHijo()
{
	document.location = "./index.php?controlador=hijo&&accion=verhijo";
	return(true);
}

function elegirHijo()
{
	document.location = "./index.php?controlador=hijo&&accion=elegirhijo";
	return(true);
}
*/


// -- FUNCIONES AJAX --

/* ajax.Request */  
function ajaxRequest(url, data) {  
	var aj = new Ajax.Request(  
	url, {  
		method:'get',   
		parameters: data,   
		onComplete: getResponse  
		}  
	);  
}  
/* ajax.Response */  
function getResponse(oReq) {  
	$('result').innerHTML = oReq.responseText;  
}  