function enviarDatos(form)
{
   if (validarDatos(form)){
     form.submit();
   }
}
	
function validarDatos(form)
{
	if(esvacio(form.descripcion.value)){
		alert("Debe ingresar una descripci�n para el destino de la tierra");
		form.descripcion.focus();
		return false;
	}
	if(!validarPrecio(form))
		return false;
	if(form.idlocalidad.value <= 0){
		alert("Debe seleccionar una localidad para el destino de la tierra");
		form.ListaLocalidad.focus();
		return false;
	}
	return true;
}

function cancelarEnvio()
{
	document.location = "./index.php?controlador=destinotierra&&accion=mostrardestinotierra";
	return(true);
}

function validarPrecio(form)
{
    var valor = form.precio.value.replace(",",".");
    valor *= 1;
    if(valor <= 0){
		alert("Debe ingresar un precio v�lido");
		form.precio.focus();
        return false;
    }
    form.precio.value = valor;
    return true;
}