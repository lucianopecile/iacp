function enviarDatos(form)
{
    if(validarDatos(form)){
        form.submit();
        return true;
    }
    return false;
}

function validarDatos(form)
{
    if(form.descripcion.value == ""){
        alert("Debe ingresar una descripci�n del parámetro");
        form.descripcion.focus();
        return false;
    }
    if(form.descripcion.value <= 0){
        alert("Debe ingresar un valor para el parámetro");
        form.valor.focus();
        return false;
    }
    return true;
}

function cancelarEnvio()
{
    document.location = "index.php?controlador=parametro&&accion=mostrarparametros";
    return true;
}