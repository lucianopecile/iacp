function enviarDatos(form)
{
	validarDatos(form);
	form.submit();
}

function validarDatos(form)
{
	var cuotadesde = form.cuotadesde.value;
	var fechadiferir = form.fechadiferir.value;

	if(!esFecha(fechadiferir)){
		alert("Debe ingresar una fecha v�lida para diferir los vencimientos");
		form.cuotadesde.focus();
		return false;
	}
	if(esvacio(cuotadesde)){
	   alert("Debe ingresar la cuota para diferir los vencimientos");
	   form.cuotadesde.focus();
	   return(false);
	}
	return true;
}

function cancelarEnvio()
{
	document.location = "./index.php?controlador=cobro&&accion=mostrarcuenta";
	return true;
}


function diferir(idcuenta, formdiferir)
{   
	var cuotadesde = formdiferir.cuotadesde.value;
	var fechadiferir = formdiferir.fechadiferir.value;
	var observacionant = formdiferir.observacionant.value;
	var observacion = formdiferir.observacion.value;

	if(!esFecha(fechadiferir)){
		alert("Debe ingresar una fecha v�lida para diferir los vencimientos");
		formdiferir.cuotadesde.focus();
		return false;
	}
	if(esvacio(cuotadesde)){
	   alert("Debe ingresar la cuota para diferir los vencimientos");
	   formdiferir.cuotadesde.focus();
	   return(false);
	}
	document.location = "./index.php?controlador=cobro&&accion=diferirvencimientos&&idcuenta="+idcuenta+"&&cuotadesde="+cuotadesde+"&&fechadiferir="+fechadiferir+"&&observacionant="+observacionant+"&&observacion="+observacion;
	return true;
}
