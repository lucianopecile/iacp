function enviarDatos(form)
{
    if(validarDatos(form)){
        form.submit();
        return true;
    }
    return false;
}

function validarDatos(form)
{
    if(form.valor.value <= 0){
        alert("Debe ingresar un valor para el interés");
        form.valor.focus();
        return false;
    }
    return true;
}

function cancelarEnvio()
{
    document.location = "index.php?controlador=index&&accion=nada";
    return true;
}
