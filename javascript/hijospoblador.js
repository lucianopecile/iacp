function nuevoHijo()
{
	var titulo = "<br/><h4>Datos del nuevo hijo</4><br/>"; 

	var tabla =	"<table><tr><td width=\"100\"><input id=\"apellido\" name=\"apellido\" type=\"text\" value=\"Apellido\" /></td>" +
				"<td width=\"100\"><input id=\"nombres\" name=\"nombres\" type=\"text\" value=\"Nombres\" /></td>" +
				"<td width=\"100\"><select id=\"lTipodoc\" name=\"lTipodoc\" onchange=\"formhijo.idtipodoc.value=this.value; \">" +
				"<option value=\"0\">Seleccionar...</option>" +
					"<option value='1'>DNI</option>" +
					"<option value='2'>LC</option>" +
					"<option value='3'>LE</option>" +
					"<option value='4'>CUIL</option>" +
					"<option value='5'>CUIT</option>" +
				"</select></td>" +
				"<td width=\"100\"><input id=\"documento\" name=\"documento\" type=\"text\" value=\"Nro documento\" />" +
				"</td><td width=\"100\"><input id=\"fechanac\" name=\"fechanac\" type=\"text\" value=\"Fecha de nacimiento\" onkeypress=\"return solonumerobarra(event)\" size=\"20\" maxlength=\"10\" /></td>" +
				"</tr></table><input id=\"idtipodoc\" name=\"idtipodoc\" type=\"hidden\"/>";
	
	var botones = "<input type=\"button\" class=\"button_text\" onclick=\"guardarHijo(this.form);\" value=\"Guardar\"/>";
	document.getElementById('nuevohijo').innerHTML = titulo + tabla + botones;
	return true;
}

function modificarHijo(idhijo)
{
	var tabla = 'tablahijo'+idhijo;
	var ap='apellido'+idhijo;
	var nom='nombres'+idhijo;
	var tdoc='listaTipoDoc'+idhijo;
	var doc='documento'+idhijo;
	var nac='fechanac'+idhijo;
	var botmod='botonmod'+idhijo;
	var botelim='botonelim'+idhijo;
	
	// desabilito todas las filas de la tabla excepto la seleccionada
	var grilla = document.getElementById("grilla");
	var tablas = grilla.getElementsByTagName("table");
	var cells;
	
	for (var i = 0; i < tablas.length; i++)
	{
		cells = tablas[i].getElementsByTagName("input");
		lista = tablas[i].getElementsByTagName("select");
		for (var j = 1; j < cells.length; j++)
		{
			cells[j].setAttribute("disabled", true);
			lista[0].setAttribute("disabled", true);
		}
		if (tablas[i].getAttribute("id") == tabla)
		{
			document.getElementById(ap).disabled = false;
			document.getElementById(nom).disabled = false;
			document.getElementById(tdoc).disabled = false;
			document.getElementById(doc).disabled = false;
			document.getElementById(nac).disabled = false;
			document.getElementById(botmod).disabled = false;
			document.getElementById(botelim).disabled = false;
		}
	}
	document.getElementById(ap).focus();
}

function guardarCambios(form)
{
	var idhijo = form.idhijo.value;
	var ap='apellido'+idhijo;
	var nom='nombres'+idhijo;
	var tdoc='listaTipoDoc'+idhijo;
	var doc='documento'+idhijo;
	var nac='fechanac'+idhijo;

	form.apellido.value = document.getElementById(ap).value;
	form.nombres.value = document.getElementById(nom).value;
	form.idtipodoc.value = document.getElementById(tdoc).value;
	form.documento.value = document.getElementById(doc).value;
	form.fechanac.value = document.getElementById(nac).value;
	
	form.botonmodificar.style.visibility = 'hidden';
	
	if (validarDatosHijo(form))
		form.submit();
}

function eliminarHijo(form)
{
	if (confirm("�Est� seguro de eliminar el hijo?"))
			form.submit();
}

function guardarHijo(form)
{
	if (validarDatosHijo(form))
		form.submit();
}



function validarDatosHijo(form)
{
	if (!validarApellidoHijo(form))
		return(false);
	if (!validarNombreHijo(form))
		return(false);
	if(!validarTipoDocHijo(form))
		return(false);
	if (!validarFechasHijo(form))
		return(false);
	return(true); 
}

function validarApellidoHijo(form)
{
	var i;
	q=form.apellido.value;
	
	if ((form.apellido.value.length < 1)) { 
	
		alert("Debe ingresar un apellido para el hijo"); 
		form.apellido.focus();
        return (false); 
    }
	if ((q == "Apellido")) { 
		alert("Debe ingresar un apellido para el hijo"); 
		form.apellido.focus();
        return (false); 
    }
   else{
	   
	 for (i = 0; i < q.length; i++ ) {   
                if ( q.charAt(i) != " " ) {   
                        return (true);   
                }   
        }   
    alert("No se acepta el apellido"); 
	form.apellido.focus();
	return(false);
	
   }
}

function validarNombreHijo(form)
{
	var i;
	q=form.nombres.value;
	
	if ((form.nombres.value.length < 1)) { 
		alert("Debe ingresar un nombre para el hijo"); 
		form.apellido.focus();
        return (false); 
    }
	if ((q == "Nombres")) { 
		alert("Debe ingresar un nombre para el hijo"); 
		form.nombres.focus();
        return (false); 
    }
   else{
	   for ( i = 0; i < q.length; i++ ) {   
		   if ( q.charAt(i) != " " ) {   
			   return (true);   
          }   
	   	}   
		alert("No se acepta el nombre"); 
		form.nombres.focus();
		return(false);
   }
}

function validarTipoDocHijo(form)
{ 
     var tipodoc=form.idtipodoc.value;

	 if(tipodoc > 0 )
		 return(true);
	 else {
		 form.idtipodoc.value = 1; /*le asigno por defecto valor 1 (DNI) para mantener la consistencia en la DB */
		 return true;
	 }
}
	

function validarFechasHijo(form)
{
	fechanac = form.fechanac.value;
	if ((fechanac == "Fecha de nacimiento") || (fechanac == ""))
		return true;
	if(esFecha(fechanac))
	    return(true);
	else{
	      alert("La fecha es incorrecta");
	      form.fechanac.focus();
          return(false);
	}
}
