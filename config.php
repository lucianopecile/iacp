<?php
$config = Config::singleton();
 
$config->set('controllersFolder', 'controladores/');
$config->set('modelsFolder', 'modelos/');
$config->set('docsFolder', 'documentos/');
$config->set('viewsFolder', 'vista/');
$config->set('fpdFolder', 'fpdf153/');
 
$config->set('dbhost', 'localhost');
$config->set('dbname', 'iacp_dbo');
$config->set('dbuser', 'root');
$config->set('dbpass', '');


?>