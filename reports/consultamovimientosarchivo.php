<?php
define('FPDF_FONTPATH','../fpd153/font/');
require_once '../fpd153/fpdf.php' ;
require_once '../fpd153/diseniomovimientos.php' ;
require_once '../librerias/config.php';
require_once '../librerias/funcionesphp.php';
require_once '../modelos/modelomovimiento.php';
require_once '../config.php'; //Archivo con configuraciones.

$mov = new ModeloMovimiento();
$mov->putIdArchivo($_POST['idarchivo']);
$lista = $mov->listadoMovimientosArchivo();
if(count($lista) <= 0)
{
	$mensaje = htmlentities("No hay movimientos.");
	$data['mensaje'] = $mensaje;
	printf("%s", $mensaje);
	return false;
}

if($lista)
{
	$pdf = new APDF();
	$pdf->AddPage();
	$pdf->Setmargins(20,20,10);
	$pdf->SetLineWidth(0.1);
	$pdf->SetFillColor(192, 192, 192);
	$pdf->Setfont('times','',8);
	$fila=30;
	$columna=30;
	$pdf->SetFont('Times','B');

	// Encabezado Fila
	$pdf->SetFont('Times','B',8);
	$fila=$fila+21;
	$pdf->SetXY($columna,$fila);
	$pdf->Cell(110,5,'',1,1,'C',1);
	$pdf->SetXY($columna,$fila);
	$pdf->drawTextBox("Fecha", 20, 5,'C','M', 1);
	$pdf->SetXY($columna+20,$fila);
	$pdf->drawTextBox("Monto transferido", 50, 5,'C','M', 1);
	$pdf->SetXY($columna+70,$fila);
	$pdf->drawTextBox("Nro. de cuenta", 20, 5,'C','M', 1);
	$pdf->SetXY($columna+90,$fila);
	$pdf->drawTextBox("Nro. de cuota", 20, 5,'C','M', 1);
	$pdf->SetLineWidth(0.1);
	$fila=$fila+5;
	//Fin Encabezado de Fila

	$pdf->Setfont('times','',8);
	$i=0;
	foreach($lista as $m)
	{
		$pdf->SetXY($columna,$fila);
		$pdf->Cell(110,5,'',1,1,'C');
		//columna fecha
		$pdf->SetXY($columna,$fila);
		$pdf->drawTextBox($m['fecha'], 20, 5,'C','M', 1);
		//columna monto
		$pdf->SetXY($columna+20,$fila);
		$valor = "$ ".number_format($m['cobrado'],2,",",".")." ";
		$pdf->drawTextBox($valor, 50, 5,'R','M', 1);
		//columna nro cuenta
		$pdf->SetXY($columna+70,$fila);
		$pdf->drawTextBox($m['nrocuenta'], 20, 5,'C','M', 1);
		//columna nro cuota
		$pdf->SetXY($columna+90,$fila);
		$pdf->drawTextBox($m['nrocuota'], 20, 5,'C','M', 1);
		$fila=$fila+5;
		$i++;

		// si hay salto de pagina
		if($fila>=300)
		{
			$pdf->Addpage('P', "Legal");
			$pdf->Setmargins(20,20,10);
			$pdf->SetLineWidth(0.1);
			$pdf->SetFillColor(192, 192, 192);
			$fila=30;
			$columna=30;

			// Encabezado Fila nueva pagina
			$pdf->SetFont('Times','B',8);
			$fila=$fila+21;
			$pdf->SetXY($columna,$fila);
			$pdf->Cell(120,5,'',1,1,'C',1);
			$pdf->SetXY($columna,$fila);
			$pdf->drawTextBox("Fecha", 20, 5,'C','M', 1);
			$pdf->SetXY($columna+20,$fila);
			$pdf->drawTextBox("Nombre", 50, 5,'C','M', 1);
			$pdf->SetXY($columna+70,$fila);
			$pdf->drawTextBox("Monto", 30, 5,'C','M', 1);
			$pdf->SetXY($columna+100,$fila);
			$pdf->drawTextBox("Estado", 20, 5,'C','M', 1);
			$pdf->SetLineWidth(0.1);
			$fila=$fila+5;
			//Fin Encabezado de Fila nueva pagina
			$pdf->SetFont('Times','',8);
		}
	}
	$pdf->Output();
}

?>