<?php
define('FPDF_FONTPATH','../fpd153/font/');
require_once '../fpd153/fpdf.php' ;
require_once '../librerias/config.php';
require_once '../modelos/modelocuota.php';
//require_once '../modelos/modelopagocredito.php';
require_once '../config.php'; //Archivo con configuraciones.
$cuotas= new modelocuota();
$cuotas->putIdCuota($_POST['idcuota']);
$cuotas->putInteresMora($_POST['intmora']);
$cuotas->putIdUsrMod($_POST["idusr"]);
$cuotas->modificarmora();
$traecuotas=$cuotas->traercuota();
if($traecuotas){
    $pdf=new FPDF();
    $pdf->AddFont('barras','','barras.php');
    $pdf->AddPage();
    $fila=5;
    $columna=5;
    $valorcuota=$_POST['deudamora'];
    $nrocuota=$_POST['nrocuota'];
    $stringcta=trim(sprintf("%s",$nrocuota));
    $cuenta=$_POST['idcuenta'];
    $stringcuenta=trim(sprintf("%s",$cuenta));
    $nrocuenta=str_pad($stringcuenta,7, "0", STR_PAD_LEFT);
    $nrocta=str_pad($stringcta,3, "0", STR_PAD_LEFT);
    $oper="2";//operacion cobro cuota en mora
    $cuentacorriente=$_POST['cuentacorriente'];
    $productor=$_POST['solicitante']." - ".strtoupper($_POST['titulares']);
    $fecha1=$vto1=$_POST['fechamora'];
    $fechaemision="Fecha de Emision : ".date('d/m/Y');
    include('talon.php');
    $pdf->Output();
    return true;
}else{
    echo "No se pudieron recuperar las cuotas";
    return(false);
}
?>