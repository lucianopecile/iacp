<?php
define('FPDF_FONTPATH','../fpd153/font/');
require_once '../fpd153/fpdf.php' ;
require_once '../fpd153/diseniodeudores.php' ;
require_once '../librerias/config.php';
require_once '../modelos/modelocuota.php';
require_once '../modelos/modelocuenta.php';
require_once '../modelos/modeloparametro.php';
require_once '../librerias/funcionesphp.php';
require_once '../config.php'; //Archivo con configuraciones.

$cuota = new modelocuota();
$cuenta = new modelocuenta();
$cant_cuotas = count($listado)-1;
$desde = $_POST['fechadesde'];
$hasta = $_POST['fechahasta'];
$condicion = "&& fechavencimiento>'".cadenaAFecha($desde)."' && fechavencimiento<'".cadenaAFecha($hasta)."'";

$lista_cuentas = $cuenta->listadoTotal();
if(count($lista_cuentas) <= 0)
{
	$mensaje = htmlentities("No hay cuentas para listar.");
	$data['mensaje'] = $mensaje;
	printf("%s", $mensaje);
	return false;
}
foreach ($lista_cuentas as $varc)
{
	$idcuenta = $varc['id'];
	$cuota->putIdCuenta($idcuenta);
	//obtengo las cuotas adeudadas y vencidas de la cuenta
	$lista_cuotas = $cuota->listadoCuotasCuentaDeuda($condicion);
	if(count($lista_cuotas) > 0)
	{
		$saldo_total=$cobrado_total=$int_mora_total=$int_mora_fecha=0;
		foreach ($lista_cuotas as $c)
		{
			$cuota->putIdCuota($c['id']);
			$cuota->traerCuota();
			$saldo_total += $cuota->getSaldo();
			$cobrado_total += $cuota->getCobrado();
			$int_mora_total += $cuota->getInteresMora();
		}
		//obtengo los datos de la proxima cuota a pagar
		$prox_cuota = $cuota->proximaCuotaVencer();
		$nro_prox_cuota = $prox_cuota['nrocuota'];
		$venc_prox_cuota = $prox_cuota['fechavenc'];
		//obtengo la mora total a la fecha indicada
		$_GET['fechamora'] = $hasta;
		$arr_mora = $cuota->calcularMoraRango($lista_cuotas[0]['nrocuota'], $lista_cuotas[count($lista_cuotas)-1]['nrocuota'], $idcuenta);
		if(!$arr_mora)
		{
			$mensaje = htmlentities("Error en el c�lculo de intereses, corrobore los datos");
			$data['mensaje'] = $mensaje;
			printf("%s", $mensaje);
			return false;
		}
		foreach ($arr_mora as $c)
		{
			$int_mora_fecha += ($c['intmora'] + $c['moraanterior']);
		}
		if(($saldo_total>0) || ($int_mora_total>0))
		{
			//genero el arreglo con todos los datos
			$listado[$i]['saldototal'] = $saldo_total;
			$listado[$i]['cobradototal'] = $cobrado_total;
			$listado[$i]['moratotal'] = $int_mora_total;
			$listado[$i]['morafecha'] = $int_mora_fecha;
			$listado[$i]['nroproxcuota'] = $nro_prox_cuota;
			$listado[$i]['vencproxcuota'] = $venc_prox_cuota;
			$listado[$i]['deudaproxcuota'] = $deuda_prox_cuota;
			$listado[$i]['nrocuenta'] = $varc['nrocuenta'];
			$listado[$i]['valorliquidacion'] = $varc['valorliquidacion'];
			$listado[$i]['titular'] = $varc['solicitante'];
			$listado[$i]['tipo'] = $varc['tipo'];
			$i++;
		}
	}
}
if($listado)
{
	$pdf = new APDF();
	$pdf->AliasNbPages();
	$pdf->AddPage('P', "Legal");
	$pdf->Setmargins(20,20,10);
	$pdf->SetLineWidth(0.1);
	$pdf->SetFillColor(192, 192, 192);
	$pdf->Setfont('times','',8);
	$fila=50;
	$columna=10;
	$pdf->SetFont('Times','B');

	//Encabezado
	$pdf->SetFont('Times','B',10);
	$pdf->SetXY($columna,$fila);
	$pdf->Write(4, "Fecha: ".date('d/m/Y'));
	$pdf->SetXY($columna,$fila+5);
	$pdf->Write(4, "Deudores con vencimientos entre el ".$desde." y el ".$hasta);
	//Fin

	// Encabezado Fila
	$pdf->SetFont('Times','B',8);
	$fila=$fila+21;
	$pdf->SetXY($columna,$fila);
	$pdf->Cell(185,10,'',1,1,'C',1);
	$pdf->SetXY($columna,$fila);
	$pdf->drawTextBox("Cuenta", 12, 10,'C','M', 1);
	$pdf->SetXY($columna+12,$fila);
	$pdf->drawTextBox("Titular", 28, 10,'C','M', 1);
	$pdf->SetXY($columna+40,$fila);
	$pdf->drawTextBox("Tipo", 15, 10,'C','M', 1);
	$pdf->SetXY($columna+55,$fila);
	$pdf->drawTextBox("Monto liquidado", 18, 10,'C','M', 1);
	$pdf->SetXY($columna+73,$fila);
	$pdf->drawTextBox("Deuda total al ".$_POST['fechahasta'], 18, 10,'C','M', 1);
	$pdf->SetXY($columna+91,$fila);
	$pdf->drawTextBox("Saldo total", 18, 10,'C','M', 1);
	$pdf->SetXY($columna+109,$fila);
	$pdf->drawTextBox("Cobrado total", 18, 10,'C','M', 1);
	$pdf->SetXY($columna+127,$fila);
	$pdf->drawTextBox("Inter�s por mora calculado", 16, 10,'C','M', 1);
	$pdf->SetXY($columna+143,$fila);
	$pdf->drawTextBox("Inter�s por mora al ".$_POST['fechahasta'], 16, 10,'C','M', 1);
	$pdf->SetXY($columna+159,$fila);
	$pdf->drawTextBox("Pr�ximo vto", 16, 10,'C','M', 1);
	$pdf->SetXY($columna+175,$fila);
	$pdf->drawTextBox("N� cuota", 10, 10,'C','M', 1);
	$pdf->SetLineWidth(0.1);
	$fila=$fila+10;
	//Fin Encabezado de Fila

	$pdf->Setfont('times','',8);
	$i=0;
	foreach($listado as $c)
	{
		$pdf->SetXY($columna,$fila);
		$pdf->Cell(185,10,'',1,1,'C');
		//columna cuenta
		$pdf->SetXY($columna,$fila);
		$pdf->drawTextBox($c['nrocuenta'], 12, 10,'C','M', 1);
		//columna titular
		$pdf->SetXY($columna+12,$fila);
		$pdf->drawTextBox($c['titular'], 28, 10,'L','M', 1);
		//columna tipo
		$pdf->SetXY($columna+40,$fila);
		$pdf->drawTextBox($c['tipo'], 15, 10,'C','M', 1);
		//columna monto liquidado
		$pdf->SetXY($columna+55,$fila);
		$valor = "$ ".number_format($c['valorliquidacion'],2,",",".")." ";
		$pdf->drawTextBox($valor, 18, 10,'R','M', 1);
		//columna deuda total a la fecha
		$pdf->SetXY($columna+73,$fila);
		$valor = "$ ".number_format($c['saldototal']+$c['morafecha'],2,",",".")." ";
		$pdf->drawTextBox($valor, 18, 10,'R','M', 1);
		//columna saldo total
		$pdf->SetXY($columna+91,$fila);
		$valor = "$ ".number_format($c['saldototal'],2,",",".")." ";
		$pdf->drawTextBox($valor, 18, 10,'R','M', 1);
		//columna cobrado total
		$pdf->SetXY($columna+109,$fila);
		$valor = "$ ".number_format($c['cobradototal'],2,",",".")." ";
		$pdf->drawTextBox($valor, 18, 10,'R','M', 1);
		//columna interes mora calculado
		$pdf->SetXY($columna+127,$fila);
		$valor = "$ ".number_format($c['moratotal'],2,",",".")." ";
		$pdf->drawTextBox($valor, 16, 10,'R','M', 1);
		//columna interes por mora a la fecha
		$pdf->SetXY($columna+143,$fila);
		$valor = "$ ".number_format($c['morafecha'],2,",",".")." ";
		$pdf->drawTextBox($valor, 16, 10,'R','M', 1);
		//columna proximo vencimiento
		$pdf->SetXY($columna+159,$fila);
		$pdf->drawTextBox($c['vencproxcuota'], 16, 10,'C','M', 1);
		//columna nro cuota vto
		$pdf->SetXY($columna+175,$fila);
		$pdf->drawTextBox($c['nroproxcuota'], 10, 10,'C','M', 1);
		$fila=$fila+10;
		$i++;

		// si hay salto de pagina
		if($fila>=300)
		{
			$pdf->Addpage('P', "Legal");
			$pdf->Setmargins(20,20,10);
			$pdf->SetLineWidth(0.1);
			$pdf->SetFillColor(192, 192, 192);
			$fila=50;
			$columna=10;
			//Encabezado nuva pagina
			$pdf->SetFont('Times','B',10);
			$pdf->SetXY($columna,$fila);
			$pdf->Write(4, "Fecha: ".date('d/m/Y'));
			$pdf->SetXY($columna,$fila+5);
			$pdf->Write(4, "Deudores entre el ".$desde." y el ".$hasta);
			//Fin encabedazo

			// Encabezado Fila nueva pagina
			$pdf->SetFont('Times','B',8);
			$fila=$fila+21;
			$pdf->SetXY($columna,$fila);
			$pdf->Cell(185,10,'',1,1,'C',1);
			$pdf->SetXY($columna,$fila);
			$pdf->drawTextBox("Cuenta", 12, 10,'C','M', 1);
			$pdf->SetXY($columna+12,$fila);
			$pdf->drawTextBox("Titular", 28, 10,'C','M', 1);
			$pdf->SetXY($columna+40,$fila);
			$pdf->drawTextBox("Tipo", 15, 10,'C','M', 1);
			$pdf->SetXY($columna+55,$fila);
			$pdf->drawTextBox("Monto liquidado", 18, 10,'C','M', 1);
			$pdf->SetXY($columna+73,$fila);
			$pdf->drawTextBox("Deuda total al ".$_POST['fechahasta'], 18, 10,'C','M', 1);
			$pdf->SetXY($columna+91,$fila);
			$pdf->drawTextBox("Saldo total", 18, 10,'C','M', 1);
			$pdf->SetXY($columna+109,$fila);
			$pdf->drawTextBox("Cobrado total", 18, 10,'C','M', 1);
			$pdf->SetXY($columna+127,$fila);
			$pdf->drawTextBox("Inter�s por mora calculado", 16, 10,'C','M', 1);
			$pdf->SetXY($columna+143,$fila);
			$pdf->drawTextBox("Inter�s por mora al ".$_POST['fechahasta'], 16, 10,'C','M', 1);
			$pdf->SetXY($columna+159,$fila);
			$pdf->drawTextBox("Pr�ximo vto", 16, 10,'C','M', 1);
			$pdf->SetXY($columna+175,$fila);
			$pdf->drawTextBox("N� cuota", 10, 10,'C','M', 1);
			$pdf->SetLineWidth(0.1);
			$fila=$fila+10;
			//Fin Encabezado de Fila nueva pagina
			$pdf->SetFont('Times','',8);
		}
	}
	$pdf->Output();
}

?>