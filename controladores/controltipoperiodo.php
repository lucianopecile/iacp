<?php

require_once 'modelos/modelotipoperiodo.php';




class controltipoperiodo
{
 
 
 	function __construct()
	{

	    $this->view = new View();
	}
 
	/*Muestra un listado con las tipoperiodoes exitentes en la base de datos*/ 
  
  
/*-------------------------------------------------------------------------------------*/
  
    public function mostrartipoperiodo()
    {
        $tipoperiodo = new modelotipoperiodo();
        $liztado = $tipoperiodo->listadoTotal();
        $data['liztado'] = $liztado;
        $this->view->show1("tipoperiodo.html", $data);
		
 	}

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

	
	public function vertipoperiodo()
	{

    $tipoperiodo= new modelotipoperiodo();
   
    if (isset($_GET['id'])) { 
       $tipoperiodo->putId($_GET['id']);
	 
	   $locent=$tipoperiodo->traertipoperiodo();
       if (!$locent){
	       echo "En este momento no se puede realizar la operacion para ver tipoperiodo, intentelo mas tarde";
  	       return;
       }
	}   
	$data=$this->cargarPlantillaModificar($tipoperiodo);
	$this->view->show("abmtipoperiodo.html", $data);
	}

/*-------------------------------------------------------------------------------------*/

	public function altatipoperiodo()
	{
	   $alta= new modelotipoperiodo();
	   
	    
       $this->cargavariables($alta,ALTA);
	   
	   $altaok=$alta->altatipoperiodo();
	   if (!$altaok){
	     echo "En este momento no se puede realizar la operacion, intentelo mas tarde";
        }
	  
		 
	}
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

	public function modificartipoperiodo()
	{
		   
       $modifica= new modelotipoperiodo();
	   
	   $this->cargavariables($modifica,MODIFICAR);
		
	    $modificado=$modifica->modificartipoperiodo();
        
	   if (!$modificado){
	     echo "En este momento no se puede realizar la operacion, intentelo mas tarde";
        }
	    $this->mostrartipoperiodo();
			
	}
	
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

	
	public function borrartipoperiodo()
	{
	 
       $borra= new modelotipoperiodo();
	   $borra->putId($_POST['id']);
	   $borrado=$borra->borrartipoperiodo();
       if (!$borrado){
	     echo "En este momento no se puede realizar la operacion, intentelo mas tarde";
        }
	    $this->mostrartipoperiodo();
		 
	}

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

    //*Esta funcion carga los valores en la vista*/
    public function cargarPlantillaModificar($partipoperiodo) 
    {  
    /*En esta instancia se cargan toods los valores que son generales para todo  tipo de accion*/
	
       if(isset($_GET['operacion'])){
	    $quehacer=$_GET['operacion'];
	}else{
		$quehacer=ALTA;
	}
    
	switch($quehacer)
	{
      case ALTA:
      
        $nombreboton="Guardar";
	    $nombreaccion="altatipoperiodo";
	 
      break;	 
      case MODIFICAR:
        $nombreboton="Guardar";
	    $nombreaccion="modificartipoperiodo";
	  break;
	  case BAJA:
         $nombreboton="Eliminar";
         $nombreaccion="borrartipoperiodo";  
      break;
      default:  
		     $nombreboton="";
             $nombreaccion="";  
		  
   }
		  
  
	  switch ($quehacer)
       {

       	case MODIFICAR:

	      $parametros = array(
                    "TITULO" =>  "Editando Tipo Periodo",
                    "ID" => $partipoperiodo->getId(),
					"DESCRIPCION" => $partipoperiodo->getDescripcion(),
					"CANTMESES"=> $partipoperiodo->getCantMeses(),
					"DISA_MODI" =>"readonly='readonly'",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
                    );
					
	    break;
		case BAJA:
		  $parametros = array(
                    "TITULO" =>  "Eliminando Tipo Periodo",

                   "ID" => $partipoperiodo->getId(),
					"DESCRIPCION" => $partipoperiodo->getDescripcion(),
					"CANTMESES"=> $partipoperiodo->getCantMeses(),
					
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
                    );
	    break;
		case ALTA:
	     $parametros = array(
		 
					"TITULO" =>  "Alta de Tipo Periodo",
                    "ID" => 0,
					"DESCRIPCION" => "",
					"CANTMESES"=> 0,
					
                 	"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
					
                    );
	    break;
		default :

		 $parametros = array(
  
                    "ID" => $partipoperiodo->getId(),
					"DESCRIPCION" => $partipoperiodo->getDescripcion(),
					"CANTMESES"=> $partipoperiodo->getCantMeses(),
					
					 
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"style='visibility:hidden'",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'"
                    );
	  }				

        return $parametros;
  }
 

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/


   public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putId($_POST["id"]);
		}
        $clasecarga->putDescripcion($_POST["descripcion"]);
		$clasecarga->putCantMeses($_POST["cantmeses"]);
     
   }

}

?>