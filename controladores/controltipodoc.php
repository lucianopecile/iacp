<?php

require_once 'modelos/modelotipodoc.php';




class controltipodoc
{
 
 
 	function __construct()
	{

	    $this->view = new View();
	}
 
	/*Muestra un listado con las tipodoces exitentes en la base de datos*/ 
  
  
/*-------------------------------------------------------------------------------------*/
  
    public function mostrartipodoc()
    {
        $tipodoc = new modelotipodoc();
        $liztado = $tipodoc->listadoTotal();
        $data['liztado'] = $liztado;
        $this->view->show1("tipodoc.html", $data);
		
 	}

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

	
	public function vertipodoc()
	{

    $tipodoc= new modelotipodoc();
   
    if (isset($_GET['id'])) { 
       $tipodoc->putId($_GET['id']);
	 
	   $locent=$tipodoc->traertipodoc();
       if (!$locent){
	          $mensaje= "No se encontro el tipo de documento";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
       }
	}   
	$data=$this->cargarPlantillaModificar($tipodoc);
	$this->view->show("abmtipodoc.html", $data);
	}

/*-------------------------------------------------------------------------------------*/

	public function altatipodoc()
	{
	   $alta= new modelotipodoc();
	   
	    
       $this->cargavariables($alta,ALTA);
	   
	   $altaok=$alta->altatipodoc();
	   if (!$altaok){
	          $mensaje= "No se pudo dar de alta el documento ";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
	  
		 
	}
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

	public function modificartipodoc()
	{
		   
       $modifica= new modelotipodoc();
	   
	   $this->cargavariables($modifica,MODIFICAR);
		
	    $modificado=$modifica->modificartipodoc();
        
	   if (!$modificado){
	          $mensaje= "No se pudo modificar el tipo de documento";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
	    $this->mostrartipodoc();
			
	}
	
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

	
	public function borrartipodoc()
	{
	 
       $borra= new modelotipodoc();
	   $borra->putId($_POST['id']);
	   $borrado=$borra->borrartipodoc();
       if (!$borrado){
	          $mensaje= "No se puede borrar el tipo de documento";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
	    $this->mostrartipodoc();
		 
	}

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

    //*Esta funcion carga los valores en la vista*/
    public function cargarPlantillaModificar($partipodoc) 
    {  
    /*En esta instancia se cargan toods los valores que son generales para todo  tipo de accion*/
	
       if(isset($_GET['operacion'])){
	    $quehacer=$_GET['operacion'];
	}else{
		$quehacer=ALTA;
	}
    
	switch($quehacer)
	{
      case ALTA:
      
        $nombreboton="Guardar";
	    $nombreaccion="altatipodoc";
	 
      break;	 
      case MODIFICAR:
        $nombreboton="Guardar";
	    $nombreaccion="modificartipodoc";
	  break;
	  case BAJA:
         $nombreboton="Eliminar";
         $nombreaccion="borrartipodoc";  
      break;
      default:  
		     $nombreboton="";
             $nombreaccion="";  
		  
   }
		  
  
	  switch ($quehacer)
       {

       	case MODIFICAR:

	      $parametros = array(
                    "TITULO" =>  "Editando Tipo Documento",
                    "ID" => $partipodoc->getId(),
					"DESCRIPCION" => $partipodoc->getDescripcion(),
					
					"DISA_MODI" =>"readonly='readonly'",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
                    );
					
	    break;
		case BAJA:
		  $parametros = array(
                    "TITULO" =>  "Eliminando Tipo Documento",

                   "ID" => $partipodoc->getId(),
					"DESCRIPCION" => $partipodoc->getDescripcion(),
					
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
                    );
	    break;
		case ALTA:
	     $parametros = array(
		 
					"TITULO" =>  "Alta de Tipo Documento",
                    "ID" => 0,
					"DESCRIPCION" => "",
					
                 	"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
					
                    );
	    break;
		default :

		 $parametros = array(
  
                    "ID" => $partipodoc->getId(),
					"DESCRIPCION" => $partipodoc->getDescripcion(),
					
					 
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"style='visibility:hidden'",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'"
                    );
	  }				

        return $parametros;
  }
 

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/


   public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putId($_POST["id"]);
		}
        $clasecarga->putDescripcion($_POST["descripcion"]);
		
     
   }

}

?>