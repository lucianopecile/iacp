<?php
require_once 'modelos/modeloinspeccion.php';
require_once 'modelos/modelozonasagro.php';
require_once 'modelos/modelounidad.php';
require_once 'modelos/modelopuntoembarque.php';
require_once 'modelos/modelosolicitud.php';
require_once 'modelos/modelosolicitudrural.php';
require_once 'modelos/modelopoblador.php';


class ControlInspeccionBasico
{
 
 	function __construct()
	{
	    $this->view = new View();
	}
 
//---------------------------------------------------------------------------------
	 
	public function mostrarinspeccion()
	// muestra todas las inspecciones en un html con una tabla
	{
		$inspecciones = new modeloinspeccion();
		$liztado = $inspecciones->listadoTotal();
		$data['liztado'] = $liztado;
		$this->view->show1("inspeccionbasico.html", $data);
 	}
 	
//---------------------------------------------------------------------------------------
	
	public function altainspeccion()
	{
		$alta = new modeloinspeccion();
		// controlo que no exista una inspeccion anterior asociada al expediente
		$alta->putIdSolicitud($_POST['idsolicitud']);
		if (!$alta->traerInspeccionAsociada())
		{
			$this->cargavariables($alta, ALTA);
			$altaok = $alta->altainspeccion();
			// en caso de error el mensaje es este
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde.");
		} else { 
			$altaok = false;
			// en caso de informe duplicado el mensaje es este
			$mensaje = htmlentities("El expediente ya tiene un informe de inspecci�n cargado.");
		}

		if (!$altaok)
		{
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$data['controlador'] = 'inspeccionbasico';
		$data['accion'] = 'verinspeccion&&idrural='.$_POST['idsolicitud']."&&operacion=".'4';
		$this->view->show1("bridgecustom.html",$data);	
	}
	
//---------------------------------------------------------------------------------------
	
	public function modificarinspeccion()
	{
        $modifica = new modeloinspeccion();
		$this->cargavariables($modifica, MODIFICAR);
        $modificado = $modifica->modificarinspeccion();
		if (!$modificado)
		{
			$mensaje= htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde.");
			$data['mensaje']=$mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->mostrarinspeccion();	
	}
		
//---------------------------------------------------------------------------------------
	
	public function borrarinspeccion()
	{
        $solicitud = new ModeloSolicitud();
        $solicitud->putIdSolicitud($_POST['idsolicitud']);
        if($solicitud->tieneCuentaAsociada())
        {
			$mensaje = htmlentities("Esta solicitud ya tiene un cuenta creada. Elimine primero la cuenta para modificar el informe.");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
        }
        $borra = new modeloinspeccion();
		$borra->putIdInspeccion($_POST['id']);
		$borrado = $borra->borrarinspeccion();
		if (!$borrado)
		{
			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde.");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->mostrarinspeccion();
	}

//-----------------------------------------------------------------------------------

	public function buscarInspeccion()
	// presenta la pantalla de busqueda de informe de inspeccion a traves de una solicitud
	{
		$this->view->show("inspeccionbasico.html", "");
	}

//----------------------------------------------------------------------------------

	public function traerInspeccionAsociada()
	// busca un informe de inspeccion asociado a la solicitud rural, si no tiene devuelve falso.
	{
		if (isset($_GET['idrural']))
		{
			$s_rural = new ModeloSolicitudRural();
			$s_rural->putIdSolicitudRural($_GET['idrural']);
			$s_rural->traersolicitudrural();
			$inspeccion = new ModeloInspeccion();
			$inspeccion->putIdSolicitud($_GET['idrural']);
			if ($inspeccion->traerInspeccionAsociada())
			{
				return true;
			}
			return false;
		}
		return false;
	}

//----------------------------------------------------------------------------------

	public function verInspeccion()
	// busca un informe de inspeccion asociado a un id.
	{
		$inspeccion = new ModeloInspeccion();
		$s_rural = new ModeloSolicitudRural();
		if (isset($_GET['idrural']))
		{
				// traigo la solicitud rural para obtener la generica 
				$s_rural->putIdSolicitudRural($_GET['idrural']);
				$s1_ok = $s_rural->traersolicitudrural();
				// traigo el infomre inspeccion segun la solicitud rural asociada
				$inspeccion->putIdSolicitud($s_rural->getIdSolicitudRural());
				$i_ok = $inspeccion->traerInspeccionAsociada();
				$inspeccion->putIdSolicitud($s_rural->getIdSolicitudRural()); /* por si trajo todos NULLs vuelvo a asignar el ID */
				// traigo la solicitud generica
				$solicitud = new ModeloSolicitud();
				$solicitud->putIdSolicitud($s_rural->getIdSolicitud());
				$s2_ok = $solicitud->traersolicitud();
				if (!$i_ok || !$s1_ok || !$s2_ok)
				{
					$mensaje = htmlentities("En este momento no se puede consultar la inspecci�n, int�ntelo m�s tarde.");
					$data['mensaje'] = $mensaje;
					$this->view->show1("mostrarerror.html", $data);
					return;
				}
		}
		$embarque = new modelopuntoembarque;
		$zonaagro = new modelozonasagro;
		$data = $this->cargarPlantillaModificar($inspeccion, $embarque, $zonaagro, $solicitud);
		$this->view->show("abminspeccionbasico.html", $data);
	}
	

//-----------------------------------------------------------------------------------

	public function verInspeccionAsociada()
	// levanta el informe de inspeccion asociado a una solicitud rural, sino existe el ID retorn campos en blanco
	{
		$inspeccion = new ModeloInspeccion();
		$solicitud = new ModeloSolicitud();
		
		if (isset($_GET['operacion']) && $_GET['operacion'] != '1')
		{
			// traigo la/s solicitud/es por nro de expediente o por apellido o por id
			if(isset($_GET['id']) && $_GET['id'] > 0)
			{
				$solicitud->putIdSolicitud($_GET['id']);
			}else{
				if (isset($_GET['expediente']))
					$solicitud->putNroExpediente($_GET['expediente']);

				if (isset($_GET['apellido']))
					$solicitud->putApellido($_GET['apellido']);
			}
			
			$arrRurales = $solicitud->listadoRuralesExpedienteConInspeccion();
			
			if(sizeof($arrRurales) > 1)
			{
				$data['liztado'] = $arrRurales;
				$this->view->show1("elegirsolicitudrural.html", $data);
				return;
			}
			elseif (sizeof($arrRurales) == 0)
			{
				$data = htmlentities("No se encontr� ninguna solicitud con esos datos");
				$this->view->show1("elegirsolicitudrural.html", $data);
				return;
			} else {
				// obtengo el unico elemento del arreglo, este es una solicitud generica 
				$obj = array_pop($arrRurales);
				// traigo la solicitud generica
				$solicitud->putIdSolicitud($obj['id']);
				$s2_ok = $solicitud->traersolicitud();
				// traigo la solicitud rural para vincular el ID con la inspeccion a partir del ID de la solicitud generica 
		    	$s_rural = new ModeloSolicitudRural();
				$s_rural->putIdSolicitud($obj['id']);
				$s1_ok = $s_rural->traersolicitudruralasociada();
				// traigo el infomre inspeccion segun la solicitud rural asociada
				$inspeccion->putIdSolicitud($s_rural->getIdSolicitudRural());
				$i_ok = $inspeccion->traerInspeccionAsociada();
				$inspeccion->putIdSolicitud($s_rural->getIdSolicitudRural()); /* por si trajo todos NULLs vuelvo a asignar el ID */ 
	
				if (!$i_ok || !$s1_ok || !$s2_ok)
				{
					$mensaje = htmlentities("En este momento no se puede consultar la inspecci�n, int�ntelo m�s tarde.");
					$data['mensaje'] = $mensaje;
					$this->view->show1("mostrarerror.html", $data);
					return;
				}
			}			
		}
		$embarque = new ModeloPuntoEmbarque();
		$zonaagro = new ModeloZonasagro();
		$data = $this->cargarPlantillaModificar($inspeccion, $embarque, $zonaagro, $solicitud);
		$this->view->show("abminspeccionbasico.html", $data);
	}

//-----------------------------------------------------------------------------------

	public function cargarPlantillaModificar($parInspeccion, $parEmbarque, $parZonaAgro, $parSolicitud)
	// carga las variables que se muestran en la vista con los valores existentes en el modelo de cada objeto 
	{  
		$ve = $parEmbarque->traerTodos();
		$ve['selected'] = $parInspeccion->getIdEmbarque();
		$vz = $parZonaAgro->traerTodos();
		$vz['selected'] = $parInspeccion->getIdZonaAgro();

		if ($parInspeccion->getIdInspeccion() == null){
			$quehacer = ALTA;
		} else { 
			if(isset($_GET['operacion']))
				$quehacer = $_GET['operacion'];
			else
				$quehacer = ALTA;
		}
		
		switch($quehacer)
		{
			case ALTA:
				$parInspeccion->putIdInspeccion("");
				$nombreboton="Guardar";
		    	$nombreaccion="altainspeccion";
				break;	 
			case MODIFICAR:
				$nombreboton="Guardar";
				$nombreaccion="modificarinspeccion";
				break;
			case BAJA:
				$nombreboton="Eliminar";
				$nombreaccion="borrarinspeccion";  
				break;
			default:  
				$nombreboton="";
				$nombreaccion="";  
		}
		
        switch ($quehacer)
		{
			case MODIFICAR:
			$parametros = array(
				"TITULO" =>  "Modificar",
				"ID" => $parInspeccion->getIdInspeccion(),
				"IDSOLICITUD" => $parInspeccion->getIdSolicitud(), // solicitud rural asociada a la inspeccion
				"EXPEDIENTE" => $parSolicitud->getNroExpediente(),
				"FECHASOLICITUD" => fechaACadena($parSolicitud->getFechaSolicitud()),
				"SOLICITANTE" => $parSolicitud->getApellido().", ".$parSolicitud->getNombres(),
				"FECHAINSPECCION" => fechaACadena($parInspeccion->getFechaInspeccion()),
				"SUPERFICIE" => $parInspeccion->getSuperficie(),
				"SUPINVERNADA" => $parInspeccion->getSupInvernada(),
				"SUPVERANADA" => $parInspeccion->getSupVeranada(),
				"CAPACIDADGANADERA" => $parInspeccion->getCapacidadGanadera(),
				"DISTANCIAEMBARQUE" => $parInspeccion->getDistanciaEmbarque(),
				"VALORHECTAREA" => $parInspeccion->getValorHectarea(),
				"LISTAZONAAGRO" => $vz,
				"IDZONAAGRO" => $parInspeccion->getIdZonaAgro(),
				"LISTAEMBARQUE" => $ve,
				"IDEMBARQUE" => $parInspeccion->getIdEmbarque(),
				"OBSERVACION" => $parInspeccion->getObservacion(),
				"ESPEJOAGUA" => $parInspeccion->getEspejoAgua(),
				"RIOARROYO" => $parInspeccion->getRioArroyo(),
				"ALTASCUMBRES" => $parInspeccion->getAltasCumbres(),
				"CENTROURBANO" => $parInspeccion->getCentroUrbano(),
				"ACCESOS" => $parInspeccion->getAccesos(),
				"CONFIGURACION"=>"",
				"OCULTO"=>"style='visibility:hidden;'",
				"ENAB_DISA"=>"disabled='disabled'",
				"DISA_MODI"=>"",
				"nombreboton"=>$nombreboton,
				"nombreaccion"=>$nombreaccion,
				);
			break;

			case BAJA:
			$parametros = array(
				"TITULO" =>  "Eliminar",
				"ID" => $parInspeccion->getIdInspeccion(),
				"IDSOLICITUD" =>$parInspeccion->getIdSolicitud(), // solicitud rural asociada a la inspeccion
				"EXPEDIENTE" => $parSolicitud->getNroExpediente(),
				"FECHASOLICITUD" => fechaACadena($parSolicitud->getFechaSolicitud()),
				"SOLICITANTE" => $parSolicitud->getApellido().", ".$parSolicitud->getNombres(),
				"FECHAINSPECCION" => fechaACadena($parInspeccion->getFechaInspeccion()),
				"SUPERFICIE" => $parInspeccion->getSuperficie(),
				"SUPINVERNADA" => $parInspeccion->getSupInvernada(),
				"SUPVERANADA" => $parInspeccion->getSupVeranada(),
				"CAPACIDADGANADERA" => $parInspeccion->getCapacidadGanadera(),
				"DISTANCIAEMBARQUE" => $parInspeccion->getDistanciaEmbarque(),
				"VALORHECTAREA" => $parInspeccion->getValorHectarea(),
				"LISTAZONAAGRO" => $vz,
				"IDZONAAGRO" => $parInspeccion->getIdZonaAgro(),
				"LISTAEMBARQUE" => $ve,
				"IDEMBARQUE" => $parInspeccion->getIdEmbarque(),
				"OBSERVACION" => $parInspeccion->getObservacion(),
				"ESPEJOAGUA" => $parInspeccion->getEspejoAgua(),
				"RIOARROYO" => $parInspeccion->getRioArroyo(),
				"ALTASCUMBRES" => $parInspeccion->getAltasCumbres(),
				"CENTROURBANO" => $parInspeccion->getCentroUrbano(),
				"ACCESOS" => $parInspeccion->getAccesos(),
				"CONFIGURACION"=>"",
				"DISA_MODI"=>"disabled='disabled'",
				"OCULTO"=>"style='visibility:hidden;'",
				"ENAB_DISA"=>"disabled='disabled'",
				"nombreaccion"=>$nombreaccion,
				"nombreboton"=>$nombreboton,
				);
			break;

			case ALTA:
			$parametros = array(
                "TITULO" =>  "Nueva",
				"ID" => 0,
				"IDSOLICITUD" => $parInspeccion->getIdSolicitud(), // solicitud rural asociada a la inspeccion
				"EXPEDIENTE" => $parSolicitud->getNroExpediente(),
				"FECHASOLICITUD" => fechaACadena($parSolicitud->getFechaSolicitud()),
				"SOLICITANTE" => $parSolicitud->getApellido(),
				"FECHAINSPECCION" => "",
				"SUPERFICIE" => 0,
				"SUPINVERNADA" => 0,
				"SUPVERANADA" => 0,
				"CAPACIDADGANADERA" => "",
				"DISTANCIAEMBARQUE" => "",
				"VALORHECTAREA" => 0,
				"LISTAZONAAGRO" => $vz,
				"IDZONAAGRO" => 0,
				"LISTAEMBARQUE" => $ve,
				"IDEMBARQUE" => 0,
				"OBSERVACION" => "",
				"ESPEJOAGUA" => 0,
				"RIOARROYO" => 0,
				"ALTASCUMBRES" => 0,
				"CENTROURBANO" => 0,
				"ACCESOS" => 0,
				"CONFIGURACION"=>"",
			    "OCULTO"=>"",
				"ENAB_DISA"=>"",					
				"DISA_MODI"=>"",
				"nombreaccion"=>$nombreaccion,
				"nombreboton"=>$nombreboton,
				);
			break;

			default:
			$parametros = array(
				"TITULO" =>  "Consulta",
				"ID" => $parInspeccion->getIdInspeccion(),
				"IDSOLICITUD" => $parInspeccion->getIdSolicitud(), // solicitud rural asociada a la inspeccion
				"EXPEDIENTE" => $parSolicitud->getNroExpediente(),
				"FECHASOLICITUD" => fechaACadena($parSolicitud->getFechaSolicitud()),
				"SOLICITANTE" => $parSolicitud->getApellido().", ".$parSolicitud->getNombres(),
				"FECHAINSPECCION" => fechaACadena($parInspeccion->getFechaInspeccion()),
				"SUPERFICIE" => $parInspeccion->getSuperficie(),
				"SUPINVERNADA" => $parInspeccion->getSupInvernada(),
				"SUPVERANADA" => $parInspeccion->getSupVeranada(),
				"CAPACIDADGANADERA" => $parInspeccion->getCapacidadGanadera(),
				"DISTANCIAEMBARQUE" => $parInspeccion->getDistanciaEmbarque(),
				"VALORHECTAREA" => $parInspeccion->getValorHectarea(),
				"LISTAZONAAGRO" => $vz,
				"IDZONAAGRO" => $parInspeccion->getIdZonaAgro(),
				"LISTAEMBARQUE" => $ve,
				"IDEMBARQUE" => $parInspeccion->getIdEmbarque(),
				"OBSERVACION" => $parInspeccion->getObservacion(),
				"ESPEJOAGUA" => $parInspeccion->getEspejoAgua(),
				"RIOARROYO" => $parInspeccion->getRioArroyo(),
				"ALTASCUMBRES" => $parInspeccion->getAltasCumbres(),
				"CENTROURBANO" => $parInspeccion->getCentroUrbano(),
				"ACCESOS" => $parInspeccion->getAccesos(),
				"nombreaccion"=>$nombreaccion,
				"CONFIGURACION"=>"style='visibility:hidden;'",
				"OCULTO"=>"style='visibility:hidden;'",
				"ENAB_DISA"=>"disabled='disabled'",
				"DISA_MODI"=>"disabled='disabled'",
				"nombreboton"=>$nombreboton,
				);
		}	
        return $parametros;
	}

//----------------------------------------------------------------------------------

	public function cargavariables($clasecarga, $oper)
	//trae las variables desde el html para volcarlas en la tabla a traves del modelo
	{
		if ($oper == MODIFICAR)
			$clasecarga->putIdInspeccion($_POST["id"]);

		$clasecarga->putIdSolicitud($_POST['idsolicitud']);
		$clasecarga->putFechaInspeccion(cadenaAFecha($_POST["fechainspeccion"]));
		$clasecarga->putSuperficie($_POST['superficie']);
		if($_POST["idunidad01"] == 0)
			$clasecarga->putIdUnidadSup("NULL");       
		else
			$clasecarga->putIdUnidadSup($_POST["idunidad01"]);
		
		$clasecarga->putSupInvernada($_POST['supinvernada']);
		if($_POST["idunidad02"] == 0)
			$clasecarga->putIdUnidadSupInv("NULL");       
		else
			$clasecarga->putIdUnidadSupInv($_POST["idunidad02"]);

		$clasecarga->putSupVeranada($_POST['supveranada']);
		if($_POST["idunidad03"] == 0)
			$clasecarga->putIdUnidadSupVer("NULL");       
		else
			$clasecarga->putIdUnidadSupVer($_POST["idunidad03"]);
		
		$clasecarga->putCapacidadGanadera($_POST['capacidadganadera']);
		$clasecarga->putDistanciaEmbarque($_POST['distanciaembarque']);
		$clasecarga->putValorHectarea($_POST['valorhectarea']);
			
	    if($_POST["idembarque"] == 0)
		   $clasecarga->putIdEmbarque('NULL');       
		else
		   $clasecarga->putIdEmbarque($_POST["idembarque"]);
		   
		if ($_POST['idagro'] == 0)
			$clasecarga->putIdZonaAgro("NULL");
		else
			$clasecarga->putIdZonaAgro($_POST['idagro']);
			
		$clasecarga->putEspejoAgua($_POST['espejoagua']);
		$clasecarga->putRioArroyo($_POST['rioarroyo']);
		$clasecarga->putAltasCumbres($_POST['altascumbres']);
		$clasecarga->putCentroUrbano($_POST['centrourbano']);
		$clasecarga->putAccesos($_POST['accesos']);

		$obs = $_POST["observacion"];
		if(!empty($obs))
			$nuevaobservacion = $_POST["observacionant"]."\n".$_POST["observacion"]." (".$_SESSION['s_username'].")";
		else
			$nuevaobservacion = $_POST["observacionant"];

		$clasecarga->putObservacion($nuevaobservacion);
   }	

}
?>