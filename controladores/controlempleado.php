<?php
require_once 'modelos/modeloempleado.php';
require_once 'modelos/modelolog.php';

class ControlEmpleado
{
 
//============================================================================
 
 	function __construct()
	{

	    $this->view = new View();
	}
 
//============================================================================
	 
	public function mostrarempleado()
	{
		$empleado = new modeloempleado();
		$liztado = $empleado->listadoTotal();
		$data['liztado'] = $liztado;
		$this->view->show1("empleado.html", $data);
	}
	
/*-------------------------------------------------------------------------------------*/

	public function verempleado()
	{
		$empleados = new modeloempleado();
		if (isset($_GET['idemp'])) {
       $empleados->putId($_GET['idemp']);
	 
	   $locent=$empleados->traerempleado();
       if (!$locent){
	       $mensaje="En este momento no se puede realizar la operacion para ver empleado, intentelo mas tarde";
  	    $data['mensaje']=$mensaje;
    	$this->view->show1("mostrarerror.html", $data);
        return;
       }
	   
	}   
	       $data=$this->cargarPlantillaModificar($empleados);
	       $this->view->show("abmempleado.html", $data);
}

/*-------------------------------------------------------------------------------------*/

    public function altaempleado()
    {
       //creo un nuevo objeto de LOG
       $log = new ModeloLog();       
        
       $alta= new modeloempleado();
       $this->cargavariables($alta,ALTA);

       $altaok=$alta->altaempleados();
       if (!$altaok){
            $mensaje= "En este momento no se puede realizar la operacion, intentelo mas tarde";
            $data['mensaje']=$mensaje;
            $this->view->show1("mostrarerror.html", $data);
       }else{
            //guardo el LOG creacion de empleado
            $log->altaLog("Se crea el empleado ".$alta->getNombreCompleto());
            $this->mostrarempleado();
       }	 
    }
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

    public function modificarempleado()
    {
        //creo un nuevo objeto de LOG
        $log = new ModeloLog(); 
        
        $modifica= new modeloempleado();
        $this->cargavariables($modifica,MODIFICAR);
        $modificado=$modifica->modificarempleados();
        if (!$modificado){
            $mensaje= "En este momento no se puede realizar la operacion, intentelo mas tarde";
            $data['mensaje']=$mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return;
        }
        //guardo el LOG de modificacion de empleado
        $log->altaLog("Se modifica el empleado ".$modifica->getNombreCompleto());
        
        $this->mostrarempleado();
    }
	
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/
	
    public function borrarempleado()
    {
        //creo un nuevo objeto de LOG
        $log = new ModeloLog();
        
        $borra= new modeloempleado();
        $borra->putId($_POST['id']);
        $borrado=$borra->borrarempleados();
        if (!$borrado){
            $mensaje= "En este momento no se puede realizar la operacion, intentelo mas tarde";
            $data['mensaje']=$mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return;
        }
        //guardo el LOG de eliminacion de empleado
        $log->altaLog("Se elimina el empleado ".$borra->getNombreCompleto());
        
        $this->mostrarempleado();
    }

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

    //*Esta funcion carga los valores en la vista*/
    public function cargarPlantillaModificar($parempleado) 
    {  
    /*En esta instancia se cargan toods los valores que son generales para todo  tipo de accion*/
	
	   if(isset($_GET['operacion'])){
	    $quehacer=$_GET['operacion'];
	}else{
		$quehacer=ALTA;
	}
    
	switch($quehacer)
	{
      case ALTA:
      
        $nombreboton="Guardar";
	    $nombreaccion="altaempleado";
	 
      break;	 
      case MODIFICAR:
        $nombreboton="Guardar";
	    $nombreaccion="modificarempleado";
	  break;
	  case BAJA:
         $nombreboton="Eliminar";
         $nombreaccion="borrarempleado";  
      break;
      default:  
		     $nombreboton="";
             $nombreaccion="";  
		  
   }
		  
  
	  switch ($quehacer)
       {

       	case MODIFICAR:

	      $parametros = array(
                    "TITULO"=>"Modificar empleado",
                    "ID"=>$parempleado->getId(),
					"NOMBRECOMPLETO"=>$parempleado->getNombreCompleto(),
					"DISA_MODI"=>"readonly='readonly'",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
                    );
					
	    break;
		case BAJA:
		  $parametros = array(
                    "TITULO"=>"Eliminar empleado",
                    "ID"=>$parempleado->getId(),
					"NOMBRECOMPLETO"=>$parempleado->getNombreCompleto(),
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
                    );
	    break;
		case ALTA:
	     $parametros = array(
					"TITULO" =>  "Alta de empleado",
                    "ID"=>0,
					"NOMBRECOMPLETO"=>"",
                 	"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
                    );
	    break;
		default :

		 $parametros = array(
					"TITULO"=>"Ver empleado",
                    "ID"=>$parempleado->getId(),
					"NOMBRECOMPLETO"=>$parempleado->getNombreCompleto(),
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"style='visibility:hidden'",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'"
                    );
		}
		return $parametros;
	}
 

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/


   public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putId($_POST["id"]);
		}
        $clasecarga->putNombreCompleto($_POST["nombrecompleto"]);

   }

}

?>