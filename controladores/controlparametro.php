<?php

require_once 'modelos/modeloparametro.php';
require_once 'modelos/modelolog.php';

class ControlParametro
{


 	function __construct()
	{
        $this->view = new View();
	}


/*-------------------------------------------------------------------------------------*/

    /*public function verInteresMora()
	{
        $convenio = new ModeloParametro();
        $ok = $convenio->traerInteresMora();
        if (!$ok)
        {
            $mensaje = htmlentities("No se encontr� el par�metro");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return false;
        }
        $data = $this->cargarPlantillaInteres($convenio);
        $this->view->show("modificarinteresmora.html", $data);
    }*/
        
    public function verIntereses()
	{
        $convenio = new ModeloParametro();
        $ok = $convenio->traerIntereses();
        if (!$ok)
        {
            $mensaje = htmlentities("No se encontr� el par�metro");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return false;
        }       
        $data = $this->cargarPlantillaInteres($convenio);        
        $this->view->show("modificarinteresmora.html", $data);
    }

/*-------------------------------------------------------------------------------------*/

    /*public function modificarInteresMora()
	{
        $modifica = new ModeloParametro();
        $modifica->putInteresMora($_POST['valor']);
        $modificado = $modifica->modificarInteresMora();
        if (!$modificado)
        {
            $mensaje = htmlentities("No se pudo modificar el valor del inter�s");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return false;
        }
        $this->verInteresMora();
    }*/
    
    public function modificarIntereses()
    {
        $log = new ModeloLog();
        $interes = new ModeloParametro();
        $ok = $interes->traerIntereses();
        $data = $this->cargarPlantillaInteres($interes);
                
        if ($_POST['cambioValor']==1){            
            $interes->putInteresMora($_POST['valor']);            
            $log->altaLog("Cambia el valor del Interes por Mora. El nuevo valor es: ".$_POST['valor']);            
        }
        if ($_POST['cambioMora']==1){
            $interes->putInteresCuotas($_POST['mora']);            
            $log->altaLog("Cambia el valor del Interes de las cuotas. El nuevo valor es: ".$_POST['mora']);
        }
        if ($_POST['cambioMensura']==1){            
            $interes->putRecuperoMensura($_POST['mensura']);            
            $log->altaLog("Cambia el valor del % de mensura. El nuevo valor es: ".$_POST['mensura']);
        }
        $modificado = $interes->modificarIntereses();
        if (!$modificado)
        {
            $mensaje = htmlentities("No se pudo modificar el valor del inter�s");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return false;
        }        
        $this->verIntereses();
    }

/*-------------------------------------------------------------------------------------*/

	public function verConvenioCuenta()
	{
        $convenio = new ModeloParametro();
        $ok = $convenio->traerConvenioCuenta();
        if (!$ok)
        {
            $mensaje = htmlentities("No se encontr� el par�metro");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return false;
        }
        $data = $this->cargarPlantillaInteres($convenio);
        $this->view->show("modificarconveniocuenta.html", $data);
    }

/*-------------------------------------------------------------------------------------*/

    public function modificarConvenioCuenta()
	{
        $log = new ModeloLog();
        $modifica = new ModeloParametro();
        $modifica->putCuentaCorriente($_POST['cuentacorriente']);
        $modifica->putConvenio($_POST['convenio']);
        $modificado = $modifica->modificarConvenioCuenta();
        $log->altaLog("Cambia la cuenta corriente del Convenio. El nuevo valor es: ".$_POST['cuentacorriente']);
        $log->altaLog("Cambia el valor del Convenio. El nuevo valor es: ".$_POST['convenio']);
        if (!$modificado)
        {
            $mensaje = htmlentities("No se pudo modificar el valor del inter�s");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return false;
        }
        $this->verConvenioCuenta();
    }

/*-------------------------------------------------------------------------------------*/

    public function cargarPlantillaInteres($parametro)
    {        
        $parametros = array(
            "TITULO"=>"Modificar",
            "VALOR"=>number_format($parametro->getInteresMora(),2,',','.'),
            "CONVENIO"=>$parametro->getConvenio(),
            "CUENTACORRIENTE"=>$parametro->getCuentaCorriente(),
            "MORA"=>number_format($parametro->getInteresCuotas(),2,',','.'),
            "MENSURA"=>number_format($parametro->getRecuperoMensura(),2,',','.'),
        );
        return $parametros;
    }

/*-------------------------------------------------------------------------------------*/

	public function verInteresCuotas()
	{
        $convenio = new ModeloParametro();
        $ok = $convenio->traerInteresCuotas();
        if (!$ok)
        {
            $mensaje = htmlentities("No se encontr� el par�metro");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return false;
        }
        $data = $this->cargarPlantillaInteres($convenio);
        $this->view->show("modificarinteresmora.html", $data);
    }

/*-------------------------------------------------------------------------------------*/

    public function modificarInteresCuotas()
    {
        $modifica = new ModeloParametro();
        $modifica->putInteresCuotas($_POST['mora']);
        $modificado = $modifica->modificarInteresCuotas();
        if (!$modificado)
        {
            $mensaje = htmlentities("No se pudo modificar el valor del inter�s");
            $data['mensaje'] = $mensaje;
            $this->view->show1("mostrarerror.html", $data);
            return false;
        }
        //$this->verInteresMora();
        $this->verIntereses();
    }

}

?>