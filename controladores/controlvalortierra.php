<?php

require_once 'modelos/modelovalortierra.php';




class controlvalortierra
{
 
 
 	function __construct()
	{

	    $this->view = new View();
	}
 
	/*Muestra un listado con las valortierraes exitentes en la base de datos*/ 
  
  
/*-------------------------------------------------------------------------------------*/
  
    public function mostrarvalortierra()
    {
        $valortierra = new modelovalortierra();
        $liztado = $valortierra->listadoTotal();
        $data['liztado'] = $liztado;
        $this->view->show1("valortierra.html", $data);
		
 	}

/*-------------------------------------------------------------------------------------*/
  
    public function popupValorTierra()
    {
        $valortierra = new modelovalortierra();
        $liztado = $valortierra->listadoTotal();
        $data['liztado'] = $liztado;
        $this->view->show1("valortierrapopup.html", $data);
		
 	}

/*-------------------------------------------------------------------------------------*/

	
	public function vervalortierra()
	{

    $valortierra= new modelovalortierra();
   
    if (isset($_GET['id'])) { 
       $valortierra->putIdValorTierra($_GET['id']);
	 
	   $locent=$valortierra->traervalortierra();
       if (!$locent){
	          $mensaje= "No se encontro el valor de la tierra";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
       }
	}   
	$data=$this->cargarPlantillaModificar($valortierra);
	$this->view->show("abmvalortierra.html", $data);
	}

/*-------------------------------------------------------------------------------------*/

	public function altavalortierra()
	{
	   $alta= new modelovalortierra();
	   
	    
       $this->cargavariables($alta,ALTA);
	   
	   $altaok=$alta->altavalortierra();
	   if (!$altaok){
	          $mensaje= "No se pudo dar de alta el valor de la tierra ";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
	  
	   else{
		 $this->view->show1("bridge.html","");
	   }	 
	  
	  
	  
		 
	}
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

	public function modificarvalortierra()
	{
		   
       $modifica= new modelovalortierra();
	   
	   $this->cargavariables($modifica,MODIFICAR);
		
	    $modificado=$modifica->modificarvalortierra();
        
	   if (!$modificado){
	          $mensaje= "No se pudo modificar el valor de la tierra";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
		else{
	      $this->mostrarvalortierra();
		 } 
			
	}
	
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

	
	public function borrarvalortierra()
	{
	 
       $borra= new modelovalortierra();
	   $borra->putIdValorTierra($_POST['id']);
	   $borrado=$borra->borrarvalortierra();
       if (!$borrado){
	          $mensaje= "No se puede borrar el valor de la tierra";
	          $data['mensaje']=$mensaje;
    	      $this->view->show1("mostrarerror.html", $data);
		      return;
        }
	    $this->mostrarvalortierra();
		 
	}

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/

    //*Esta funcion carga los valores en la vista*/
    public function cargarPlantillaModificar($parvalortierra) 
    {  
    /*En esta instancia se cargan toods los valores que son generales para todo  tipo de accion*/
	
       if(isset($_GET['operacion'])){
	    $quehacer=$_GET['operacion'];
	}else{
		$quehacer=ALTA;
	}
    
	switch($quehacer)
	{
      case ALTA:
      
        $nombreboton="Guardar";
	    $nombreaccion="altavalortierra";
	 
      break;	 
      case MODIFICAR:
        $nombreboton="Guardar";
	    $nombreaccion="modificarvalortierra";
	  break;
	  case BAJA:
         $nombreboton="Eliminar";
         $nombreaccion="borrarvalortierra";  
      break;
      default:  
		     $nombreboton="";
             $nombreaccion="";  
		  
   }
		  
  
	  switch ($quehacer)
       {

       	case MODIFICAR:

	      $parametros = array(
                    "TITULO" =>  "Editando Valor de la tierra",
                    "ID" => $parvalortierra->getIdValorTierra(),
					"RECEPTIVIDAD" => $parvalortierra->getReceptividad(),
					"DISTANCIA50" => $parvalortierra->getDistancia50(),
					"DISTANCIA100" => $parvalortierra->getDistancia100(),
					"DISTANCIA150" => $parvalortierra->getDistancia150(),
					"DISTANCIA200" => $parvalortierra->getDistancia200(),
					"DISTANCIA250" => $parvalortierra->getDistancia250(),
					"DISTANCIA300" => $parvalortierra->getDistancia300(),
					"DISTANCIA350" => $parvalortierra->getDistancia350(),
					"DISTANCIAMAS350" => $parvalortierra->getDistanciamas350(),
					"DISA_MODI" =>"readonly='readonly'",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
                    );
					
	    break;
		case BAJA:
		  $parametros = array(
                    "TITULO" =>  "Eliminando Valor de la tierra",

                    "ID" => $parvalortierra->getIdValorTierra(),
					"RECEPTIVIDAD" => $parvalortierra->getReceptividad(),
					"DISTANCIA50" => $parvalortierra->getDistancia50(),
					"DISTANCIA100" => $parvalortierra->getDistancia100(),
					"DISTANCIA150" => $parvalortierra->getDistancia150(),
					"DISTANCIA200" => $parvalortierra->getDistancia200(),
					"DISTANCIA250" => $parvalortierra->getDistancia250(),
					"DISTANCIA300" => $parvalortierra->getDistancia300(),
					"DISTANCIA350" => $parvalortierra->getDistancia350(),
					"DISTANCIAMAS350" => $parvalortierra->getDistanciamas350(),
					
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
                    );
	    break;
		case ALTA:
	     $parametros = array(
		 
					"TITULO" =>  "Alta de Valor de la tierra",
                    "ID" =>0,
					"RECEPTIVIDAD" => "",
					"DISTANCIA50" => 0,
					"DISTANCIA100" => 0,
					"DISTANCIA150" => 0,
					"DISTANCIA200" => 0,
					"DISTANCIA250" => 0,
					"DISTANCIA300" => 0,
					"DISTANCIA350" => 0,
					"DISTANCIAMAS350" => 0,
					
                 	"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
					
                    );
	    break;
		default :

		 $parametros = array(
  
                    "ID" => $parvalortierra->getIdValorTierra(),
					"RECEPTIVIDAD" => $parvalortierra->getReceptividad(),
					"DISTANCIA50" => $parvalortierra->getDistancia50(),
					"DISTANCIA100" => $parvalortierra->getDistancia100(),
					"DISTANCIA150" => $parvalortierra->getDistancia150(),
					"DISTANCIA200" => $parvalortierra->getDistancia200(),
					"DISTANCIA250" => $parvalortierra->getDistancia250(),
					"DISTANCIA300" => $parvalortierra->getDistancia300(),
					"DISTANCIA350" => $parvalortierra->getDistancia350(),
					"DISTANCIAMAS350" => $parvalortierra->getDistanciamas350(),
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton,
					"CONFIGURACION"=>"style='visibility:hidden'",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'"
                    );
	  }				

        return $parametros;
  }
 

/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/


   public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putIdValorTierra($_POST["id"]);
		}
        $clasecarga->putReceptividad($_POST["receptividad"]);
        $clasecarga->putDistancia50($_POST["distancia50"]);
        $clasecarga->putDistancia100($_POST["distancia100"]);
        $clasecarga->putDistancia150($_POST["distancia150"]);
        $clasecarga->putDistancia200($_POST["distancia200"]);
        $clasecarga->putDistancia250($_POST["distancia250"]);
        $clasecarga->putDistancia300($_POST["distancia300"]);
        $clasecarga->putDistancia350($_POST["distancia350"]);
        $clasecarga->putDistanciamas350($_POST["distanciamas350"]);
   }

}

?>