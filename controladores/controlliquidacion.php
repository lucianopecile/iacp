<?php
require_once 'modelos/modelosolicitud.php';
require_once 'modelos/modelosolicitudurbana.php';
require_once 'modelos/modelosolicitudrural.php';
require_once 'modelos/modelolocalidad.php';
require_once 'modelos/modeloconceptoliquidacion.php';
require_once 'modelos/modelounidad.php';
require_once 'modelos/modelogradotenencia.php';
require_once 'modelos/modelodestinotierra.php';
require_once 'modelos/modeloconceptoliquidacion.php';
require_once 'modelos/modelosolicitudconcepto.php';
require_once 'modelos/modeloinspeccion.php';
require_once 'modelos/modelozonasagro.php';
require_once 'modelos/modelopuntoembarque.php';


class ControlLiquidacion
{
 
 	function __construct()
	{
	    $this->view = new View();
	}
 
		
//============================================================================	

	public function modificarliquidacionurbana()
	{
		$modifica = new ModeloSolicitudConcepto();
		$this->cargavariables($modifica, MODIFICAR);
		$modificado = $modifica->modificarsolicitudconcepto();
		if(!$modificado)
		{
		    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
		    $data['mensaje'] = $mensaje;
	    	$this->view->show1("mostrarerror.html", $data);
		}
		$_POST['operacion']=4;
		$this->verliquidacionurbana();
	}

//============================================================================	

	public function modificarliquidacionrural()
	{
		$modifica = new ModeloSolicitudConcepto();
		$this->cargavariables($modifica, MODIFICAR);
		$modificado = $modifica->modificarsolicitudconcepto();
		if(!$modificado)
		{
		    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
		    $data['mensaje'] = $mensaje;
	    	$this->view->show1("mostrarerror.html", $data);
		}
		$_POST['operacion']=4;
		$this->verliquidacionrural();
	}

//-----------------------------------------------------------------------------------
public function verliquidacionurbana()
	{
		
	$solicitudes = new modelosolicitud();
    if (isset($_GET['idsol']) || isset($_POST['idsolicitud'])) { 
	  if (isset($_GET['idsol']))
        $solicitudes->putIdSolicitud($_GET['idsol']);
	  else	
        $solicitudes->putIdSolicitud($_POST['idsolicitud']);
       	$solicitud = $solicitudes->traersolicitudporid();
	
     	if (!$solicitud){
 	       $mensaje= "En este momento no se puede realizar la operaci�n para ver solicitud, int�ntelo m�s tarde";
	       $data['mensaje']=$mensaje;
           $this->view->show1("mostrarerror.html", $data);
		   return;
         }
	
    } 
	$conceptoliquidacion=new modeloconceptoliquidacion;
	$solicitudconcepto=new modelosolicitudconcepto;
	$solicitudurbana=new modelosolicitudurbana;	
	$destino=new modelodestinotierra;
	$localidad=new modelolocalidad;
	$gradotenencia=new modelogradotenencia;
	$unidad=new modelounidad;
	$data=$this->cargarPlantillaUrbana($solicitudes,$solicitudurbana,$localidad,$gradotenencia,$unidad,$destino,$conceptoliquidacion,$solicitudconcepto);
	  $this->view->show("liquidacionurbana.html", $data);
	
	}	

//-----------------------------------------------------------------------------------
	public function verliquidacionrural()
	{
		$solicitudes = new ModeloSolicitud();
		if (isset($_GET['idsol']) || isset($_POST['idsolicitud']))
		{ 
			if (isset($_GET['idsol']))
				$solicitudes->putIdSolicitud($_GET['idsol']);
			else
				$solicitudes->putIdSolicitud($_POST['idsolicitud']);
				$solicitud = $solicitudes->traersolicitudporid();
			if (!$solicitud)
			{
	 			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n para ver solicitud, int�ntelo m�s tarde");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
		} 
		$conceptoliquidacion = new modeloconceptoliquidacion;
		$solicitudconcepto = new modelosolicitudconcepto;
		$solicitudrural = new modelosolicitudrural;	
		$gradotenencia = new modelogradotenencia;
		$inspeccion = new ModeloInspeccion();
		$zonas = new ModeloZonasagro();
		$embarque = new ModeloPuntoEmbarque();
		$data = $this->cargarPlantillaRural($solicitudes,$solicitudrural,$gradotenencia,$inspeccion,$zonas,$embarque,$conceptoliquidacion,$solicitudconcepto);
		$this->view->show("liquidacionrural.html", $data);
	}	
	
//-----------------------------------------------------------------------------------

public function cargarPlantillaUrbana($parSolicitud,$parSolicitudUrbana,$parLocalidad,$parGradoTenencia,$parUnidad,$parDestino,$parConceptoLiquidacion,$parSolicitudConcepto) 
{  
	$vl= $parLocalidad->TraerTodosL();
	$vl['selected']=  $parSolicitud->getIdLocalidad();
	$listaunidad= $parUnidad->TraerTodos();
	$listaunidad['selected']=  $parSolicitud->getIdUnidad();
	$vgt= $parGradoTenencia->TraerTodos();
	$parSolicitudUrbana->putIdSolicitud($parSolicitud->getIdSolicitud());
	$parSolicitudUrbana->traersolicitudurbanaasociada();
    $parDestino->putId($parSolicitudUrbana->getIdDestinoTierra());
	$parDestino->traerdestinotierra();
	$vgt['selected']=  $parSolicitud->getIdGradoTenencia();
	$lista_ts = array(
		array('1','RURAL'), 
		array('2','URBANA'), 
		'selected' => $parSolicitud->getTipoSolicitud()
	);
	$idsolicitud = $parSolicitud->getIdSolicitud();
		
		
		$quehacer = "";
	  if(!isset($_POST['operacion'])){	
		if ($idsolicitud == 0)
			$quehacer = ALTA;
		else
			 $quehacer = MODIFICAR;
	  }
	  else
	    $quehacer=$_POST['operacion'];
	  
	switch($quehacer)
	{
      case ALTA:
		
		$parSolicitud->putIdSolicitud("");
		
        $nombreboton="Guardar";
	    $nombreaccion="altaliquidacion";
	 
      break;	 
      case MODIFICAR:
	     
        $nombreboton="Guardar";
	    $nombreaccion="modificarliquidacionurbana";
	  break;
	  case BAJA:
	     
         $nombreboton="Eliminar";
         $nombreaccion="borrarliquidacion";  
      break;
      default:  
		  $nombreboton="";
	      $nombreaccion="verliquidacionurbana";
		  
   }
	
		  
        switch ($quehacer)
       {

       	case MODIFICAR:
		$idsoli=$parSolicitud->getIdSolicitud();
		$parametros = array(
                    "ID" => $parSolicitud->getIdSolicitud(),
					"IDSOLICITUD" => $parSolicitud->getIdSolicitud(),
    				"USR_MOD"=>$parSolicitud->getUsrMod(),
					"LISTALOCALIDAD"=>$vl,
					"LISTASOLICITUD"=>$lista_ts,
					"TIPOSOLICITUD"=>$parSolicitud->getTipoSolicitud(),
					"PRECIO"=>$parDestino->getPrecio(),
					"DESTINOTIERRA"=>$parDestino->getDescripcion(),
					"IDLOCALIDAD"=>$parSolicitud->getIdLocalidad(),
					"IDESTADOSOLICITUD"=>  $parSolicitud->getIdEstadoSolicitud(), 
					"SUPERFICIE"=>$parSolicitud->getSuperficie(),
					"IDUNIDADSOL"=>$parSolicitud->getIdUnidad(),
					"LISTAUNIDADSOL"=>$listaunidad,
					"LISTAGT"=>$vgt,
					"NOVER"=>"style='visibility:hidden'",
                    "TABLACONCEPTOSDISPONIBLES" => $this->cargarconceptos($idsoli,$parConceptoLiquidacion),
					"TABLACONCEPTOSELEGIDOS" => $this->cargarconceptosElegidos($idsoli,$parSolicitudConcepto), 
					"nombreaccion"=>$nombreaccion,
					"CONFIGURACION"=>"",
					"VERENVIAR"=>"style='visibility:hidden'",
					"SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"DISA_MODI"=>"disabled='disabled'",
					"nombreboton"=>$nombreboton
					);
        break;
		case BAJA:
        $idsoli=$parSolicitud->getIdSolicitud();
	    $parametros = array(
                    "ID" => $parSolicitud->getIdSolicitud(),
					"IDSOLICITUD" => $parSolicitud->getIdSolicitud(),
    				"USR_MOD"=>$parSolicitud->getUsrMod(),
					"LISTALOCALIDAD"=>$vl,
					"LISTASOLICITUD"=>$lista_ts,
					"PRECIO"=>$parDestino->getPrecio(),
					"DESTINOTIERRA"=>$parDestino->getDescripcion(),
					"TIPOSOLICITUD"=>$parSolicitud->getTipoSolicitud(),
					"IDLOCALIDAD"=>$parSolicitud->getIdLocalidad(),
					"IDESTADOSOLICITUD"=>  $parSolicitud->getIdEstadoSolicitud(), 
					"SUPERFICIE"=>$parSolicitud->getSuperficie(),
					"IDUNIDADSOL"=>$parSolicitud->getIdUnidad(),
					"LISTAUNIDADSOL"=>$listaunidad,
					"LISTAGT"=>$vgt,
                    "TABLACONCEPTOSDISPONIBLES" => $this->cargarconceptos($idsoli,$parConceptoLiquidacion),
					"TABLACONCEPTOSELEGIDOS" => $this->cargarconceptosElegidos($idsoli,$parSolicitudConcepto), 
					"nombreaccion"=>$nombreaccion,
					"NOVER"=>"style='visibility:hidden'",
					"VERENVIAR"=>"style='visibility:hidden'",
					"DISA_MODI"=>"",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
					"nombreboton"=>$nombreboton
				
                    );
	     break;
		 case ALTA:
             $parametros = array(
                    "ID" => $parSolicitud->getIdSolicitud(),
					"IDSOLICITUD" => $parSolicitud->getIdSolicitud(),
    				"USR_MOD"=>$parSolicitud->getUsrMod(),
					"LISTALOCALIDAD"=>$vl,
					"LISTASOLICITUD"=>$lista_ts,
					"TIPOSOLICITUD"=>$parSolicitud->getTipoSolicitud(),
					"IDLOCALIDAD"=>$parSolicitud->getIdLocalidad(),
					"PRECIO"=>$parDestino->getPrecio(),
					"DESTINOTIERRA"=>$parDestino->getDescripcion(),
					"LISTAGT"=>$vgt,
					"IDESTADOSOLICITUD"=>  $parSolicitud->getIdEstadoSolicitud(), 
					"SUPERFICIE"=>$parSolicitud->getSuperficie(),
					"IDUNIDADSOL"=>$parSolicitud->getIdUnidad(),
					"LISTAUNIDADSOL"=>$listaunidad,
					"CONFIGURACION"=>"",
				    "SOLOLECTURA"=>"",
					"ENAB_DISA"=>"",
					"VERENVIAR"=>"style='visibility:hidden'",
					"TABLACONCEPTOSDISPONIBLES" => $this->cargarconceptos(0,$parConceptoLiquidacion),
					"TABLACONCEPTOSELEGIDOS" => $this->cargarconceptosElegidos(0,$parSolicitudConcepto), 
					"DISA_MODI"=>"",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
					   );
	
	     break;
		 default :
		 $idsoli=$parSolicitud->getIdSolicitud();
		$parametros = array(
                    "ID" => $parSolicitud->getIdSolicitud(),
					"IDSOLICITUD" => $parSolicitud->getIdSolicitud(),
    				"USR_MOD"=>$parSolicitud->getUsrMod(),
					"LISTALOCALIDAD"=>$vl,
					"LISTASOLICITUD"=>$lista_ts,
					"TIPOSOLICITUD"=>$parSolicitud->getTipoSolicitud(),
					"PRECIO"=>$parDestino->getPrecio(),
					"DESTINOTIERRA"=>$parDestino->getDescripcion(),
					"IDLOCALIDAD"=>$parSolicitud->getIdLocalidad(),
					"LISTAGT"=>$vgt,
					"IDESTADOSOLICITUD"=>  $parSolicitud->getIdEstadoSolicitud(), 
					"SUPERFICIE"=>$parSolicitud->getSuperficie(),
					"IDUNIDADSOL"=>$parSolicitud->getIdUnidad(),
					"LISTAUNIDADSOL"=>$listaunidad,
			    	"nombreaccion"=>$nombreaccion,
                    "TABLACONCEPTOSDISPONIBLES" => $this->cargarconceptos($idsoli,$parConceptoLiquidacion),
					"TABLACONCEPTOSELEGIDOS" => $this->cargarconceptosElegidos($idsoli,$parSolicitudConcepto), 
					"NOVER"=>"style='visibility:hidden'",
					"CONFIGURACION"=>"style='visibility:hidden'",
					"SOLOLECTURA"=>"readonly='readonly'",
					"ENAB_DISA"=>"disabled='disabled'",
	                "DISA_MODI"=>"disabled='disabled'",
					"VERENVIAR"=>"",
					"nombreaccion"=>$nombreaccion,
					"nombreboton"=>$nombreboton
                    );
		}
        return $parametros;
  }
  
//-----------------------------------------------------------------------------------
	public function cargarPlantillaRural($parSolicitud,$parSolicitudRural,$parGradoTenencia,$parInspeccion,$parZonas,$parEmbarque,$parConceptoLiquidacion,$parSolicitudConcepto) 
	{  
		$parSolicitudRural->putIdSolicitud($parSolicitud->getIdSolicitud());
		$parSolicitudRural->traersolicitudruralasociada();
		$vgt = $parGradoTenencia->TraerTodos();
		$vgt['selected']=  $parSolicitud->getIdGradoTenencia();
		$idsolicitud = $parSolicitud->getIdSolicitud();
		$parInspeccion->putIdSolicitud($parSolicitudRural->getIdSolicitudRural());
		$parInspeccion->traerInspeccionAsociada();
		$lz = $parZonas->traerTodos();
		$lz['selected'] = $parInspeccion->getIdZonaAgro();
		$le = $parEmbarque->traerTodos();
		$le['selected'] = $parInspeccion->getIdEmbarque();
				
		$quehacer = "";
		if(!isset($_POST['operacion'])){	
			if ($idsolicitud == 0)
				$quehacer = ALTA;
			else
				$quehacer = MODIFICAR;
		}else{
			$quehacer=$_POST['operacion'];
		}
	  
		switch($quehacer)
		{
	    	case ALTA:
				$parSolicitud->putIdSolicitud("");
				$nombreboton="Guardar";
		    	$nombreaccion="altaliquidacion";
		 		break;	 
	      case MODIFICAR:
				$nombreboton="Guardar";
				$nombreaccion="modificarliquidacionrural";
				break;
		  case BAJA:
		     	$nombreboton="Eliminar";
	         	$nombreaccion="borrarliquidacion";  
	      		break;
	      default:  
			  	$nombreboton="";
		      	$nombreaccion="verliquidacionrural";
		}
		
	    switch ($quehacer)
	    {
			case MODIFICAR:
				$idsoli=$parSolicitud->getIdSolicitud();
				$parametros = array(
						"ID"=>$parSolicitud->getIdSolicitud(),
						"IDSOLICITUD"=>$parSolicitud->getIdSolicitud(),
						"USR_MOD"=>$parSolicitud->getUsrMod(),
						"IDESTADOSOLICITUD"=>$parSolicitud->getIdEstadoSolicitud(), 
						"SUPERFICIE"=>$parInspeccion->getSuperficie(),
						"VERANADA"=>$parInspeccion->getSupVeranada(),
						"INVERNADA"=>$parInspeccion->getSupInvernada(),
						"PRECIO"=>$parInspeccion->getValorHectarea(),
						"IDZONAAGRO"=>$parInspeccion->getIdZonaAgro(),
						"LISTAZONAS"=>$lz,
						"LISTAEMBARQUE"=>$le,
						"IDEMBARQUE"=>$parInspeccion->getIdEmbarque(),
						"ESPEJOAGUA"=>$parInspeccion->getEspejoAgua(),
						"RIOARROYO"=>$parInspeccion->getRioArroyo(),
						"ALTASCUMBRES"=>$parInspeccion->getAltasCumbres(),
						"CENTROURBANO"=>$parInspeccion->getCentroUrbano(),
						"ACCESOS"=>$parInspeccion->getAccesos(),
						"LISTAGT"=>$vgt,
						"TABLACONCEPTOSDISPONIBLES"=>$this->cargarconceptos($idsoli,$parConceptoLiquidacion),
						"TABLACONCEPTOSELEGIDOS"=>$this->cargarconceptosElegidos($idsoli,$parSolicitudConcepto), 
						"nombreaccion"=>$nombreaccion,
						"NOVER"=>"style='visibility:hidden'",
						"CONFIGURACION"=>"",
						"VERENVIAR"=>"style='visibility:hidden'",
						"SOLOLECTURA"=>"",
						"ENAB_DISA"=>"",
						"DISA_MODI"=>"disabled='disabled'",
						"nombreboton"=>$nombreboton
						);
				break;

			case BAJA:
				$idsoli = $parSolicitud->getIdSolicitud();
				$parametros = array(
						"ID"=>$parSolicitud->getIdSolicitud(),
						"IDSOLICITUD"=>$parSolicitud->getIdSolicitud(),
						"USR_MOD"=>$parSolicitud->getUsrMod(),
						"IDESTADOSOLICITUD"=>$parSolicitud->getIdEstadoSolicitud(), 
						"SUPERFICIE"=>$parInspeccion->getSuperficie(),
						"VERANADA"=>$parInspeccion->getSupVeranada(),
						"INVERNADA"=>$parInspeccion->getSupInvernada(),
						"PRECIO"=>$parInspeccion->getValorHectarea(),
						"IDZONAAGRO"=>$parInspeccion->getIdZonaAgro(),
						"LISTAZONAS"=>$lz,
						"LISTAEMBARQUE"=>$le,
						"IDEMBARQUE"=>$parInspeccion->getIdEmbarque(),
						"ESPEJOAGUA"=>$parInspeccion->getEspejoAgua(),
						"RIOARROYO"=>$parInspeccion->getRioArroyo(),
						"ALTASCUMBRES"=>$parInspeccion->getAltasCumbres(),
						"CENTROURBANO"=>$parInspeccion->getCentroUrbano(),
						"ACCESOS"=>$parInspeccion->getAccesos(),
						"LISTAGT"=>$vgt,
						"TABLACONCEPTOSDISPONIBLES" => $this->cargarconceptos($idsoli,$parConceptoLiquidacion),
						"TABLACONCEPTOSELEGIDOS" => $this->cargarconceptosElegidos($idsoli,$parSolicitudConcepto), 
						"nombreaccion"=>$nombreaccion,
						"NOVER"=>"style='visibility:hidden'",
						"VERENVIAR"=>"style='visibility:hidden'",
						"DISA_MODI"=>"",
						"SOLOLECTURA"=>"readonly='readonly'",
						"ENAB_DISA"=>"disabled='disabled'",
						"nombreboton"=>$nombreboton
	                    );
					break;
					
			 case ALTA:
			 	$parametros = array(
						"ID"=>$parSolicitud->getIdSolicitud(),
						"IDSOLICITUD"=>$parSolicitud->getIdSolicitud(),
						"USR_MOD"=>$parSolicitud->getUsrMod(),
						"IDESTADOSOLICITUD"=>$parSolicitud->getIdEstadoSolicitud(), 
						"SUPERFICIE"=>$parInspeccion->getSuperficie(),
						"VERANADA"=>$parInspeccion->getSupVeranada(),
						"INVERNADA"=>$parInspeccion->getSupInvernada(),
						"PRECIO"=>$parInspeccion->getValorHectarea(),
						"IDZONAAGRO"=>$parInspeccion->getIdZonaAgro(),
						"LISTAZONAS"=>$lz,
						"LISTAEMBARQUE"=>$le,
						"IDEMBARQUE"=>$parInspeccion->getIdEmbarque(),
						"ESPEJOAGUA"=>$parInspeccion->getEspejoAgua(),
						"RIOARROYO"=>$parInspeccion->getRioArroyo(),
						"ALTASCUMBRES"=>$parInspeccion->getAltasCumbres(),
						"CENTROURBANO"=>$parInspeccion->getCentroUrbano(),
						"ACCESOS"=>$parInspeccion->getAccesos(),
						"LISTAGT"=>$vgt,
						"TABLACONCEPTOSDISPONIBLES" => $this->cargarconceptos(0,$parConceptoLiquidacion),
						"TABLACONCEPTOSELEGIDOS" => $this->cargarconceptosElegidos(0,$parSolicitudConcepto), 
			 			"CONFIGURACION"=>"",
					    "SOLOLECTURA"=>"",
						"ENAB_DISA"=>"",
						"VERENVIAR"=>"style='visibility:hidden'",
						"DISA_MODI"=>"",
						"nombreaccion"=>$nombreaccion,
						"nombreboton"=>$nombreboton
						   );
				break;

			default :
				$idsoli = $parSolicitud->getIdSolicitud();
				$parametros = array(
						"ID"=>$parSolicitud->getIdSolicitud(),
						"IDSOLICITUD"=>$parSolicitud->getIdSolicitud(),
						"USR_MOD"=>$parSolicitud->getUsrMod(),
						"IDESTADOSOLICITUD"=>$parSolicitud->getIdEstadoSolicitud(), 
						"SUPERFICIE"=>$parInspeccion->getSuperficie(),
						"VERANADA"=>$parInspeccion->getSupVeranada(),
						"INVERNADA"=>$parInspeccion->getSupInvernada(),
						"PRECIO"=>$parInspeccion->getValorHectarea(),
						"IDZONAAGRO"=>$parInspeccion->getIdZonaAgro(),
						"LISTAZONAS"=>$lz,
						"LISTAEMBARQUE"=>$le,
						"IDEMBARQUE"=>$parInspeccion->getIdEmbarque(),
						"ESPEJOAGUA"=>$parInspeccion->getEspejoAgua(),
						"RIOARROYO"=>$parInspeccion->getRioArroyo(),
						"ALTASCUMBRES"=>$parInspeccion->getAltasCumbres(),
						"CENTROURBANO"=>$parInspeccion->getCentroUrbano(),
						"ACCESOS"=>$parInspeccion->getAccesos(),
						"LISTAGT"=>$vgt,
						"TABLACONCEPTOSDISPONIBLES"=>$this->cargarconceptos($idsoli,$parConceptoLiquidacion),
						"TABLACONCEPTOSELEGIDOS"=>$this->cargarconceptosElegidos($idsoli,$parSolicitudConcepto),
						"NOVER"=>"style='visibility:hidden'",
						"CONFIGURACION"=>"style='visibility:hidden'",
						"SOLOLECTURA"=>"readonly='readonly'",
						"ENAB_DISA"=>"disabled='disabled'",
		                "DISA_MODI"=>"disabled='disabled'",
						"VERENVIAR"=>"",
						"nombreaccion"=>$nombreaccion,
						"nombreboton"=>$nombreboton
	                    );
			} 				
		return $parametros;
	}
//----------------------------------------------------------------------------------
//Carga las variables del html para volcarlas en la tabla


public function cargavariables($clasecarga,$oper){
       
	 ///carga las variables de la clase 
	   
	   if ($oper==MODIFICAR){  
	    
        $clasecarga->putIdSolicitud($_POST["idsolicitud"]);
		}
       
		$clasecarga->putSolicitudesConceptos($_POST["conceptoselegidos"]);
   }	

//----------------------------------------------------------------------------------
//Carga la tabla de conceptos como variable fija

public function cargarconceptos($idsoli,$parConceptoLiquidacion){

$arrConceptos=array();
if($idsoli==0)
$result_all=$parConceptoLiquidacion->TraerTodosSolicitud("");
else
$result_all=$parConceptoLiquidacion->TraerTodosSolicitud("id not in(select idconceptoliquidacion from solicitudesconceptos where idsolicitud='$idsoli')");
 if(isset($_POST['operacion']) && ($_POST['operacion']==4))
    $disable="disabled=\"disabled\"";
 else
    $disable="";
 while ($varlab = mysql_fetch_object($result_all))
		{
		
			//llenar el array 
			$arrConceptos[] = array("id"=>$varlab->id,
			                    
  		    					"descripcion"=>"<input disabled=\"disabled\" id=\"descripcion".$varlab->id."\" name=\"descripcion".$varlab->id."\" value=\"".trim($varlab->descripcion)."\" >",
								"valor"=>"<input ".$disable." id=\"valor".$varlab->id."\" name=\"valor".$varlab->id."\" onkeypress=\"return solonumerocoma(event)\" value=".$varlab->valor.">",
								"celda"=>"<input ".$disable." name=\"elegir\" id=\"elegir\" type=\"checkbox\" value=".$varlab->id." onclick=\"this.form.iditem.value=".$varlab->id."; elegiritem(this.form.iditem.value,this); \">"
								
								);
		} 	
return($arrConceptos);
}

//----------------------------------------------------------------------------------
//Carga la tabla de conceptos como variable fija

public function cargarconceptosElegidos($idsoli,$parSolicitudConcepto){

$arrConceptos=array();
$parSolicitudConcepto->putIdSolicitud($idsoli);
$result_all=$parSolicitudConcepto->TraerDetalle();
 if(isset($_POST['operacion']) && ($_POST['operacion']==4))
    $disable="disabled=\"disabled\"";
 else
    $disable="";

 while ($varlab = mysql_fetch_object($result_all))
		{
			//llenar el array 
			$arrConceptos[] = array("id"=>$varlab->idconceptoliquidacion,
  		    					"descripcion"=>"<input disabled=\"disabled\" id=\"descripcion".$varlab->idconceptoliquidacion."\" name=\"descripcion".$varlab->idconceptoliquidacion."\" value=\"".$varlab->descripcion."\" >",
								"valor"=>"<input  ".$disable." id=\"valor".$varlab->idconceptoliquidacion."\" name=\"valor".$varlab->idconceptoliquidacion."\" onkeypress=\"return solonumerocoma(event)\" value=".$varlab->valor.">",
								"celda"=>"<input  ".$disable." name=\"elegir\" id=\"elegir\" type=\"checkbox\"  checked=\"checked\" value=".$varlab->idconceptoliquidacion." onclick=\"this.form.iditem.value=".$varlab->idconceptoliquidacion."; elegiritem(this.form.iditem.value,this); \">"
								
								);
		} 	
return($arrConceptos);
}

//-----------------------------------------------------------------------------------
	public function conceptoscuenta()
	{
		$solicitudes = new ModeloSolicitud();
		if (isset($_GET['idsolicitud']))
		{ 
				$solicitudes->putIdSolicitud($_GET['idsolicitud']);
				$solicitud = $solicitudes->traersolicitudporid();
			if (!$solicitud)
			{
	 			$mensaje = htmlentities("En este momento no se puede realizar la operaci�n para ver solicitud, int�ntelo m�s tarde");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
		} 
		$conceptoliquidacion = new modeloconceptoliquidacion();
		$solicitudconcepto = new modelosolicitudconcepto();
		$data = $this->cargarPlantillaConceptos($solicitudes,$conceptoliquidacion,$solicitudconcepto);
		$this->view->show("conceptoscuenta.html", $data);
	}	
	
  
//-----------------------------------------------------------------------------------
	public function cargarPlantillaConceptos($parSolicitud,$parConceptoLiquidacion,$parSolicitudConcepto) 
	{  

				$nombreboton="Guardar";
				$nombreaccion="modificarconceptoscuenta";
				$idsoli=$parSolicitud->getIdSolicitud();
				$parametros = array(
						"ID"=>$parSolicitud->getIdSolicitud(),
						"IDSOLICITUD"=>$parSolicitud->getIdSolicitud(),
						"USR_MOD"=>$parSolicitud->getUsrMod(),
						"TABLACONCEPTOSDISPONIBLES"=>$this->cargarconceptos($idsoli,$parConceptoLiquidacion),
						"TABLACONCEPTOSELEGIDOS"=>$this->cargarconceptosElegidos($idsoli,$parSolicitudConcepto), 
						"nombreaccion"=>$nombreaccion,
						"NOVER"=>"style='visibility:hidden'",
						"CONFIGURACION"=>"",
						"SOLOLECTURA"=>"",
						"ENAB_DISA"=>"",
						"DISA_MODI"=>"disabled='disabled'",
						"nombreboton"=>$nombreboton
						);

		return $parametros;
	}

//============================================================================	

	public function modificarconceptoscuenta()
	{
		$modifica = new ModeloSolicitudConcepto();
		$this->cargavariables($modifica, MODIFICAR);
		$modificado = $modifica->modificarsolicitudconcepto();
		if(!$modificado)
		{
		    $mensaje = htmlentities("En este momento no se puede realizar la operaci�n, int�ntelo m�s tarde");
		    $data['mensaje'] = $mensaje;
	    	$this->view->show1("mostrarerror.html", $data);
		}
	}






}

?>