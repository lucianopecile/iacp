<?php

require_once 'modelos/modelomovimiento.php';
require_once 'modelos/modelocuenta.php';
require_once 'modelos/modelocuota.php';




class controlmovimiento
{
 
 
 	function __construct()
	{

	    $this->view = new View();
	}
 
	/*Muestra un listado con los movimientos existentes en la base de datos*/ 
  
  
/*-------------------------------------------------------------------------------------*/
  
    public function mostrarmovimiento()
    {
        $mov = new modelomovimiento();
        $liztado = $mov->listadoTotal();
        $data['liztado'] = $liztado;
        $this->view->show1("movimiento.html", $data);
		
 	}

/*-------------------------------------------------------------------------------------*/

	public function vermovimiento()
	{
		$mov = new modelomovimiento();
		if(isset($_GET['idcuota']))
		{
		   $mov->putIdCuota($_GET['idcuota']);
		   $liztado = $mov->traermovimiento();
		}
		$data['liztado'] = $liztado;
		if(isset($_GET['nrocuota']))
			$data['nrocuota'] = $_GET['nrocuota'];

		$this->view->show1("movimiento.html", $data);
	}

/*-------------------------------------------------------------------------------------*/

	public function cargavariables($clasecarga, $oper)
	{
		//carga las variables de la clase
		if($oper == MODIFICAR)
		{
			$clasecarga->putId($_POST["id"]);
		}
		$clasecarga->putMonto($_POST["monto"]);
		$clasecarga->putInteres($_POST["interes"]);
		$clasecarga->putFecha($_POST["fecha"]);
		$clasecarga->putIdCuota($_POST["idcuota"]);
		$clasecarga->putIdTipomov($_POST["idtipomov"]);
		$clasecarga->putIdRegmov($_POST["idregmov"]);
		$clasecarga->putTransaccion($_POST["transaccion"]);
	}

//---------------------------------------------------------------------------------

	public function mostrarCuentaDeshacerMovimiento()
	// muestra todos los cuotas en un html con una tabla
	{
		$cuentas = new ModeloCuenta();
		$liztado = $cuentas->listadoTotal();
		$data['liztado'] = $liztado;
        $this->view->show1("listadeshacermovimiento.html", $data);
	}

//-------------------------------------------------------------------------------

	public function deshacerMovimiento()
	{
		$mov = new modelomovimiento();
		$cuota = new ModeloCuota();
		if(isset($_GET['idcuota']))
		{
			$cuota->putIdCuota($_GET['idcuota']);
			$cuota->traerCuota();
			$mov->putIdCuota($_GET['idcuota']);
			$liztado = $mov->traermovimiento();
			if(count($liztado) > 0)
			{
				//ordeno el listado por ID y no por fecha
				$liztado = orderMultiDimensionalArray($liztado, "id");
			}
		}
		$data['liztado'] = $liztado;
		$data['nrocuota'] = $cuota->getNroCuota();
		$data['idcuenta'] = $cuota->getIdCuenta();
		$data['fechamora'] = fechaACadena($cuota->getFechaCalculoMora());
		$data['fechavenc'] = fechaACadena($cuota->getFechaVenc());
		$data['interesmora'] = $cuota->getInteresMora();
		$this->view->show1("deshacermovimiento.html", $data);
	}

//-----------------------------------------------------------------------------------

	public function cuentaDeshacerMovimiento()
	//muestra la pantalla para realizar un pago manual de las cuotas
	{
		$cuenta = new ModeloCuenta();
		$cuotas = new ModeloCuota();
		if(isset($_GET['idcuenta']) && $_GET['idcuenta'] > 0)
		{
			$cuenta->putIdCuenta($_GET['idcuenta']);
			$cuenta->traerCuenta();
			$cuotas->putIdCuenta($_GET['idcuenta']);
			$liztado = $cuotas->listadoTotal();
			if(!$liztado)
			{
				$mensaje = htmlentities("En este momento no se puede realizar la operaci�n para ver cuotas, int�ntelo m�s tarde");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
			$idcuenta = $_GET['idcuenta'];
			$deuda_vencida = $cuotas->calcularDeudaVencida($idcuenta);
			$deuda_no_vencida = $cuotas->calcularSaldoNoVencido($idcuenta);
			$saldo = $deuda_vencida + $deuda_no_vencida;
			$data['liztado'] = $liztado;
			$data['idcuenta'] = $idcuenta;
			$data['solicitante'] = $cuotas->getSolicitante();
			$data['anioexpediente'] = $cuotas->getAnioExpediente();
			$data['letraexpediente'] = $cuotas->getLetraExpediente();
			$data['nroexpediente'] = $cuotas->getNroExpediente();
			$data['nrocuenta'] = $cuotas->getNroCuenta();
			$data['valorliquidacion'] = number_format($cuotas->getValorLiquidacion(),2,',','.');
			$data['cuentacorriente'] = $cuotas->getCuentaCorriente();
			$data['cobradototal'] = number_format($cuotas->calcularCobradoTotal($idcuenta),2,',','.');
			$data['deudavencida'] = number_format($deuda_vencida,2,',','.');
			$data['deudanovencida'] = number_format($deuda_no_vencida,2,',','.');
			$data['saldo'] = number_format($saldo,2,',','.');
			$data['interestotal'] = number_format($cuotas->calcularInteresTotal($idcuenta),2,',','.');
			$data['totalfinanciacion'] = number_format($cuotas->calcularTotalFinanciacion($idcuenta),2,',','.');
			$data['observacionant'] = $cuenta->getObservacion();
		}else{
			$mensaje = htmlentities("No se seleccion� correctamente la cuenta, int�ntelo nuevamente");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
		$this->view->show1("cuentadeshacermovimiento.html", $data);
	}

//-----------------------------------------------------------------------------------

	public function eliminarUltimoMovimiento()
	//elimina el ultimo movimiento de una cuota de la DB y devulve los montos de la cuota al estado anterior
	{
		$mov = new ModeloMovimiento();
		$cuota = new ModeloCuota();
		if(isset($_GET['idmov']))
		{
			//traigo el movimiento de la DB
			$mov->putId($_GET['idmov']);
			$ok = $mov->traerMovimientoId();
			if(!$ok)
			{
				$mensaje = htmlentities("No se pudo recuperar el movimiento, int�ntelo nuevamente");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
			//traigo la cuota asociada al movimiento
			$idcuota = $mov->getIdCuota();
			$cuota->putIdCuota($idcuota);
			$ok = $cuota->traerCuota();
			if(!$ok)
			{
				$mensaje = htmlentities("No se pudo recuperar la cuota, int�ntelo nuevamente");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
			//calculo y genero los nuevos valores
			$nuevo_cobrado = $cuota->getCobrado() - ($mov->getCobrado() - $mov->getMontoGastos());
			$nuevo_saldo = $cuota->getSaldo() + $mov->getMontoSaldo();
			$nueva_mora = $cuota->getInteresMora() + $mov->getMontoMora();
			$nueva_fecha_pago = $mov->traerFechaMovAnterior();
			$nueva_observacion = $cuota->getObservacion()."\n".date('d/m/Y').": el usuario [".$_SESSION['s_username']."] elimin� el �ltimo movimiento.";
			$cuota->putCobrado($nuevo_cobrado);
			$cuota->putSaldo($nuevo_saldo);
			$cuota->putInteresMora($nueva_mora);
			$cuota->putFechaPago(cadenaAFecha($nueva_fecha_pago));
			$cuota->putIdUsrMod($_SESSION['s_idusr']);
			$cuota->putObservacion($nueva_observacion);
			//registro las modificaciones en la DB
			$ok = $cuota->guardarCobro();
			if(!$ok)
			{
				$mensaje = htmlentities("No se pudo modificar el estado de la cuota, int�ntelo nuevamente");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
			//elimino el movimiento de la DB
			$ok = $mov->borrarMovimiento();
			if(!$ok)
			{
				$mensaje = htmlentities("No se pudo eliminar el movimiento de la base de datos. Puede haber inconsistencias, verifique los datos");
				$data['mensaje'] = $mensaje;
				$this->view->show1("mostrarerror.html", $data);
				return;
			}
			//llamo a la nueva vista
/*			$data['controlador'] = "movimiento";
			$data['accion'] = "cuentadeshacermovimiento&&idcuenta=".$cuota->getIdCuenta();
			$this->view->show1("bridgecustom.html",$data);*/
		}else{
			$mensaje = htmlentities("Error interno en el identificador del movimiento. Cierre la sesi�n e int�ntelo nuevamente");
			$data['mensaje'] = $mensaje;
			$this->view->show1("mostrarerror.html", $data);
			return;
		}
	}

}

?>